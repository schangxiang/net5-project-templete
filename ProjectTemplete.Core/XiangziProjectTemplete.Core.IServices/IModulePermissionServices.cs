﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;

namespace XiangziProjectTemplete.Core.IServices
{
    public partial interface IModulePermissionServices : IBaseServices<ModulePermission>
    {
    }
}