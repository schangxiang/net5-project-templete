﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.CommonModel;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// 生成任务号表服务接口 
    /// </summary>	 
    public interface IBase_GenerateTaskNoServices : IBaseServices<Base_GenerateTaskNo>
    {
        /// <summary>
        /// 生成新的任务号
        /// </summary>
        /// <param name="generateTaskType"></param>
        /// <returns></returns>
        Task<FunReturnResultModel<string>> GenerateTaskNo(int generateTaskType);

    }
}
