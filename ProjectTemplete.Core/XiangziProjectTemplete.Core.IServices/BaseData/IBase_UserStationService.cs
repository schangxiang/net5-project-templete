﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// 用户站点绑定表服务接口 
    /// </summary>	 
    public interface IBase_UserStationServices :IBaseServices<Base_UserStation> 
	{
        /// <summary>
        /// 根据用户获取他能看到的工序列表
        /// </summary>
        /// <param name="_user"></param>
        /// <returns></returns>
        Task<SeeUserStationViewModel> GetSeeStationListByUser(IUser _user);
    } 
} 
