﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 大类物料和站点绑定信息服务接口 
	/// </summary>	 
    public interface IBase_BigMaterialStationServices :IBaseServices<Base_BigMaterialStation> 
	{ 
    } 
} 
