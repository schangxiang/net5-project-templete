﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary>
    /// IBase_SalverServices
    /// </summary>	
    public interface IBase_SalverServices : IBaseServices<Base_Salver>
    {
        /// <summary>
        /// 根据编号查找托盘
        /// </summary>
        /// <param name="salverCode">托盘号</param>
        /// <param name="noFoundThrowError">如果没有发现托盘，是否需要报警</param>
        /// <returns></returns>
        Task<MessageModel<Base_Salver>> GetSalverByCode(string salverCode, bool noFoundThrowError);

    }
}