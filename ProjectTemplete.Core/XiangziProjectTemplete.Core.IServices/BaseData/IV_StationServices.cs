﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels.BasicData;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary>
    /// IBase_StationServices
    /// </summary>	
    public interface IV_StationServices : IBaseServices<V_Station>
    {
        /// <summary>
        /// 根据分类获取分类集合
        /// </summary>
        /// <param name="category">分类</param>
        /// <returns></returns>
        Task<List<V_Station>> GetStationByCategory(string category);

    }
}