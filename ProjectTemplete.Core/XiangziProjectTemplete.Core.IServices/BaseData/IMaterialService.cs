﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// 物料管理表服务接口 
    /// </summary>	 
    public interface IBase_MaterialServices : IBaseServices<Base_Material>
    {
        /// <summary>
        /// 根据物料名称查找物料
        /// </summary>
        /// <param name="materialName">物料名称</param>
        /// <param name="noFoundThrowError">如果没有发现物料，是否需要报警</param>
        /// <returns></returns>
        Task<MessageModel<Base_Material>> GetMaterialByName(string materialName, bool noFoundThrowError);

    }
}
