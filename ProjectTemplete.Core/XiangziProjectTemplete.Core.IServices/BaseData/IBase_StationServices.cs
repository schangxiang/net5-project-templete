﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IRepository.UnitOfWork;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary>
    /// IBase_StationServices
    /// </summary>	
    public interface IBase_StationServices : IBaseServices<Base_Station>
    {
        /// <summary>
        ///  验证站点编号和站点名称是否重复
        /// </summary>
        /// <param name="stationCode"></param>
        /// <param name="stationName"></param>
        /// <returns></returns>
        Task<MessageModel<bool>> ValidateIsRepate(string stationCode, string stationName, int id = 0);

        /// <summary>
        /// 验证站点是否可以被使用
        /// </summary>
        /// <param name="stationId"></param>
        /// <returns></returns>
        Task<MessageModel<bool>> ValidateStationIsAllowUse(int stationId);

        /// <summary>
        /// 获取库里的空库位
        /// </summary>
        /// <param name="storeArea">库存区域</param>
        /// <param name="stationPlaceTypeEnum">库位类型</param>
        /// <returns></returns>
        Task<MessageModel<Base_Station>> GetEmptyStation(string storeArea, StationPlaceTypeEnum stationPlaceTypeEnum);


        /// <summary>
        /// 获取库里的空库位列表
        /// </summary>
        /// <param name="storeArea">库存区域</param>
        /// <param name="stationPlaceTypeEnum">库位类型</param>
        /// <returns></returns>
        Task<MessageModel<List<Base_Station>>> GetEmptyStationList(string storeArea_CodeItemId, int stationPlaceType_CodeItemId);

        /// <summary>
        /// 获取库里的所有库位列表
        /// </summary>
        /// <param name="storeArea">库存区域</param>
        /// <param name="stationPlaceTypeEnum">库位类型</param>
        /// <returns></returns>
        Task<MessageModel<List<Base_Station>>> GetAllStationList(string storeArea_CodeItemId, int stationPlaceType_CodeItemId);

        /// <summary>
        /// 新增或编辑站点
        /// </summary>
        /// <param name="param"></param>
        /// <param name="station"></param>
        /// <param name="addEditDeleteEnum"></param>
        /// <param name="_unitOfWork"></param>
        /// <param name="_user"></param>
        /// <returns></returns>
        Task<MessageModel<string>> AddEditStation(AddEditStationPostParam param,
            Base_Station station, AddEditDeleteEnum addEditDeleteEnum,
           IUnitOfWork _unitOfWork, IUser _user);

        /// <summary>
        /// 根据工序获取工序下的站点列表（显示全部）
        /// </summary>
        /// <param name="procedureCodeItemId">工序的码表ID</param>
        /// <returns></returns>
        Task<MessageModel<List<Base_Station>>> GetStationsByProcedure(int procedureCodeItemId);

        /// <summary>
        /// 根据用户的工序权限，获取他下面的工序获取工序下的站点列表（显示全部）
        /// </summary>
        /// <returns></returns>
        Task<MessageModel<List<Base_Station>>> GetAllStationsByUserProcedure(IUser user, ILes_ProcedureRoleServices les_ProcedureRoleServices);


        /// <summary>
        /// 获取库里可用空闲库位列表
        /// </summary>
        /// <param name="_base_CodeItemsServices"></param>
        /// <param name="storeArea"></param>
        /// <param name="StationPlaceType"></param>
        /// <returns></returns>
        Task<MessageModel<List<Base_Station>>> GetEmptyStationList(IBase_CodeItemsServices _base_CodeItemsServices,
         StoreAreaEnum storeArea, StationPlaceTypeEnum StationPlaceType);

    }
}