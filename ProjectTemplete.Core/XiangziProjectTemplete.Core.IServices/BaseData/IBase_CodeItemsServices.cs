﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary>
    /// IBase_CodeItemsServices
    /// </summary>	
    public interface IBase_CodeItemsServices : IBaseServices<Base_CodeItems>
    {
        /// <summary>
        /// 同一个码表集，是否已经存在了该码表项
        /// </summary>
        /// <param name="setCode">码表集编码</param>
        /// <param name="code">码表项编码</param>
        /// <returns>true:已存在，false：不存在</returns>
        Task<bool> IsExistCodeItemBySetCode(int setCode, string code);

        /// <summary>
        /// 同一个码表集，是否已经存在了该码表项(排除ID)
        /// </summary>
        /// <param name="setCode">码表集编码</param>
        /// <param name="code">码表项编码</param>
        /// <param name="id">码表项ID</param>
        /// <returns>true:已存在，false：不存在</returns>
        Task<bool> IsExistCodeItemBySetCodeExcludeId(int setCode, string code, int id);

        /// <summary>
        /// 根据代码编码获取码表项
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<MessageModel<Base_CodeItems>> GetSingleCodeItemByCode(string code);


        /// <summary>
        /// 根据 代码集编码 获取码表项列表
        /// </summary>
        /// <param name="setCode">代码集编码</param>
        /// <returns></returns>
        Task<MessageModel<List<Base_CodeItems>>> GetCodeItemsBySetCode(string setCode);

    }
}