﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels.BasicData;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary>
    /// IBase_CodeItemsServices
    /// </summary>	
    public interface IV_CodeItemsServices : IBaseServices<V_CodeItems>
    {
       
    }
}