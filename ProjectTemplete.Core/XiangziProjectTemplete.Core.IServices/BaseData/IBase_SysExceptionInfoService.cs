﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 异常日志表服务接口 
	/// </summary>	 
    public interface IBase_SysExceptionInfoServices :IBaseServices<Base_SysExceptionInfo> 
	{
        /// <summary>
        /// 插入异常数据内容
        /// </summary>
        /// <param name="eie">异常对象</param>
        /// <param name="isWriteTextLog">是否需要写入文本日志</param>
        void InsertExceptionInfo(Base_SysExceptionInfo eie, bool isWriteTextLog = false);
    } 
} 
