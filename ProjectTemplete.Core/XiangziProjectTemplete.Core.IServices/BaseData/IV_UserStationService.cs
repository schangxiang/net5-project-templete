﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// 用户站点绑定服务接口 
    /// </summary>	 
    public interface IV_UserStationServices :IBaseServices<V_UserStation> 
	{ 
    } 
} 
