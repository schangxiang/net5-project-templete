﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;

namespace XiangziProjectTemplete.Core.IServices
{	
	/// <summary>
	/// IBase_CodeSetsServices
	/// </summary>	
    public interface IBase_CodeSetsServices :IBaseServices<Base_CodeSets>
	{
    }
}