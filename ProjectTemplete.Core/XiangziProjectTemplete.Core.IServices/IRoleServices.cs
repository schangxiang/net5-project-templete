using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{	
	/// <summary>
	/// RoleServices
	/// </summary>	
    public interface IRoleServices :IBaseServices<Role>
	{
        Task<Role> SaveRole(string roleName);
        Task<string> GetRoleNameByRid(int rid);

        /// <summary>
        /// 是否存在同名的角色
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> IsExistSameRoleName(string roleName, int id = 0);

    }
}
