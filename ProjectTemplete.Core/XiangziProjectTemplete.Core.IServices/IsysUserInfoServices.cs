    

using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{	
	/// <summary>
	/// sysUserInfoServices
	/// </summary>	
    public interface ISysUserInfoServices :IBaseServices<sysUserInfo>
	{
        Task<sysUserInfo> SaveUserInfo(string loginName, string loginPwd);
        Task<string> GetUserRoleNameStr(string loginName, string loginPwd);

        /// <summary>
        /// 是否存在同登录名或昵称的用户
        /// </summary>
        /// <param name="uLoginName">登录名</param>
        /// <param name="uRealName">昵称</param>
        /// <param name="uID">是否需要过滤用户ID</param>
        /// <returns></returns>
        Task<bool> IsExistSameUserName(string uLoginName, string uRealName, int uID = 0);
    }
}
