﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// Agv车辆状态历史记录服务接口 
	/// </summary>	 
    public interface ILes_AgvStatusHistoryServices :IBaseServices<Les_AgvStatusHistory> 
	{ 
    } 
} 
