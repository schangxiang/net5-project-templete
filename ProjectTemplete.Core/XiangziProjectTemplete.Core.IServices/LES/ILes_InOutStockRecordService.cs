﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete.LESPublicCommon;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 出入库记录服务接口 
	/// </summary>	 
    public interface ILes_InOutStockRecordServices :IBaseServices<Les_InOutStockRecord> 
	{
        /// <summary>
        /// 保存 出入库记录
        /// </summary>
        /// <param name="_user"></param>
        /// <param name="inOutStockTypeEnum"></param>
        /// <param name="handlerType"></param>
        /// <param name="manualHandlerRemark"></param>
        /// <param name="operationRemark"></param>
        /// <param name="base_Station"></param>
        /// <returns></returns>
        Task<MessageModel<bool>> SaveInOutStockRecord(IUser _user,
            InOutStockTypeEnum inOutStockTypeEnum, string operationRemark, Base_Station base_Station);
    } 
} 
