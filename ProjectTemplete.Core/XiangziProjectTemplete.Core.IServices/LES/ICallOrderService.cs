﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 叫料工单服务接口 
	/// </summary>	 
    public interface ILes_CallOrderServices :IBaseServices<Les_CallOrder> 
	{ 
    } 
} 
