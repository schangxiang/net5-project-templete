﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 入库记录服务接口 
	/// </summary>	 
    public interface ILes_InStockRecordServices :IBaseServices<Les_InStockRecord> 
	{ 
    } 
} 
