﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// Thingworx发送的呼叫任务服务接口 
	/// </summary>	 
    public interface IThingworx_CallOrderServices :IBaseServices<Thingworx_CallOrder> 
	{ 
    } 
} 
