﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.Views;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 分组查询立库缓存区的物料服务接口 
	/// </summary>	 
    public interface IV_BufferStockCollectGroupMaterialServices :IBaseServices<V_BufferStockCollectGroupMaterial> 
	{ 
    } 
} 
