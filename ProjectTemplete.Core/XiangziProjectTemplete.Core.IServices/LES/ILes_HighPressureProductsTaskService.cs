﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 高压绕线线圈任务服务接口 
	/// </summary>	 
    public interface ILes_HighPressureProductsTaskServices :IBaseServices<Les_HighPressureProductsTask> 
	{ 
    } 
} 
