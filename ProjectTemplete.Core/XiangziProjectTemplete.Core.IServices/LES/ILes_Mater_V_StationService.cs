﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 物料和站点绑定信息服务接口 
	/// </summary>	 
    public interface ILes_Mater_V_StationServices :IBaseServices<Les_Mater_V_Station> 
	{
        /// <summary>
        /// 根据站点id寻找绑定数据
        /// </summary>
        /// <param name="stationId"></param>
        /// <returns></returns>
        Task<MessageModel<Les_Mater_V_Station>> GetLes_Mater_V_StationByStationId(int stationId);
    } 
} 
