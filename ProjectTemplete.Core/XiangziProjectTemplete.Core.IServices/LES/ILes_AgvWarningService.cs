﻿using XiangziProjectTemplete.Core.IRepository.UnitOfWork;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.CommonModel;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// AGV报警服务接口 
    /// </summary>	 
    public interface ILes_AgvWarningServices : IBaseServices<Les_AgvWarning>
    {
        /// <summary>
        /// 处理AGV报警信息
        /// </summary>
        /// <param name="_unitOfWork"></param>
        /// <param name="requestList"></param>
        /// <returns></returns>
        Task<MessageModel<bool>> AddAgvWarning(IUnitOfWork _unitOfWork, List<AgvWarningPostParam> requestList);

        /// <summary>
        /// 更新报警内容为已读
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<FunReturnResultModel> UpdateWarningKnow(int id, string user);

        /// <summary>
        /// 更新全部报警为已处理
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<FunReturnResultModel> UpdateAllWarningDone(string user, string content);
    }
}
