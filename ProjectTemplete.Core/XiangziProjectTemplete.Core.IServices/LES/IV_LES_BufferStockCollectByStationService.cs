﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.Views;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 根据站点查询的库存查询视图服务接口 
	/// </summary>	 
    public interface IV_LES_BufferStockCollectByStationServices :IBaseServices<V_LES_BufferStockCollectByStation> 
	{ 
    } 
} 
