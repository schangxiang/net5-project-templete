﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 人工处理数据记录表服务接口 
	/// </summary>	 
    public interface ILes_PersonHandlerRecordServices :IBaseServices<Les_PersonHandlerRecord> 
	{
        /// <summary>
        ///  保存人工处理数据记录
        /// </summary>
        /// <param name="piece"></param>
        /// <param name="_user"></param>
        /// <param name="manualHandlerRemark"></param>
        /// <returns></returns>
        Task<MessageModel<bool>> SavePersonHandlerRecord(IUser _user, string handlerType, string manualHandlerRemark
             , string operationRemark,
            string key1, string key2, Base_Station _Station=null);
    } 
} 
