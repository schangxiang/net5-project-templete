﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 回库搬运服务接口 
	/// </summary>	 
    public interface ILes_BackToStoreServices :IBaseServices<Les_BackToStore> 
	{ 
    } 
} 
