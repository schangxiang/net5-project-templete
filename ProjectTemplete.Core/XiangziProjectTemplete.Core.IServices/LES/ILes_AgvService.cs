﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// Agv车辆服务接口 
	/// </summary>	 
    public interface ILes_AgvServices :IBaseServices<Les_Agv> 
	{ 
    } 
} 
