﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IRepository.UnitOfWork;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 表的中文注解服务接口 
	/// </summary>	 
    public interface ILes_StockInfoServices :IBaseServices<Les_StockInfo> 
	{
        /// <summary>
        /// 清空 某个区域的空托盘和余料工位
        /// </summary>
        /// <param name="_unitOfWork"></param>
        /// <param name="_user"></param>
        /// <returns></returns>
        Task<MessageModel<bool>> DeleteAllSalverStation(StoreAreaEnum storeAreaEnum,
            string remark, IUnitOfWork _unitOfWork, IUser _user, ILes_PersonHandlerRecordServices _les_PersonHandlerRecordServices, ILes_InOutStockRecordServices les_InOutStockRecordServices);


        /// <summary>
        /// 清空 某个区域的全部库存
        /// </summary>
        /// <param name="_unitOfWork"></param>
        /// <param name="_user"></param>
        /// <returns></returns>
        Task<MessageModel<bool>> ClearStockByArea(StoreAreaEnum storeAreaEnum,
             string remark,
             IUnitOfWork _unitOfWork, IUser _user
             , ILes_PersonHandlerRecordServices _les_PersonHandlerRecordServices, ILes_InOutStockRecordServices les_InOutStockRecordServices);

        /// <summary>
        /// 根据站点ID删除库存
        /// </summary>
        /// <param name="_les_PersonHandlerRecordServices"></param>
        /// <param name="staionId">站点ID</param>
        /// <param name="manualHandlerRemark"></param>
        /// <param name="_unitOfWork"></param>
        /// <param name="_user"></param>
        /// <returns></returns>
        Task<MessageModel<bool>> DeleteStockByStationId(ILes_AgvTaskServices _les_AgvTaskServices, ILes_PersonHandlerRecordServices _les_PersonHandlerRecordServices,
               int staionId, string manualHandlerRemark, IUnitOfWork _unitOfWork, IUser _user, ILes_InOutStockRecordServices les_InOutStockRecordServices);


    } 
} 
