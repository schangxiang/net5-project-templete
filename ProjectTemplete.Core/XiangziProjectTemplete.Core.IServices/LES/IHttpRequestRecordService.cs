﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 请求日志表服务接口 
	/// </summary>	 
    public interface IHttpRequestRecordServices :IBaseServices<HttpRequestRecord> 
	{ 
    } 
} 
