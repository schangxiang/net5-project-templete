﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// Thingworx 备料通知表服务接口 
	/// </summary>	 
    public interface IThingworx_StockPreparationNoticeServices :IBaseServices<Thingworx_StockPreparationNotice> 
	{ 
    } 
} 
