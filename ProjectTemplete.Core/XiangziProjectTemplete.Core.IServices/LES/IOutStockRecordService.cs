﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 出库记录服务接口 
	/// </summary>	 
    public interface ILes_OutStockRecordServices : IBaseServices<Les_OutStockRecord> 
	{ 
    } 
} 
