﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// 角色工序绑定服务接口 
    /// </summary>	 
    public interface ILes_ProcedureRoleServices : IBaseServices<Les_ProcedureRole>
    {
        /// <summary>
        /// 根据角色获取他能看到的工序列表
        /// </summary>
        /// <param name="_user"></param>
        /// <returns></returns>
        Task<SeeProcedureRoleViewModel> GetProcedureListByUser(IUser _user);

    }
}
