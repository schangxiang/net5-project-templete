﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IRepository.UnitOfWork;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// Agv任务服务接口 
    /// </summary>	 
    public interface ILes_AgvTaskServices : IBaseServices<Les_AgvTask>
    {
        /// <summary>
        /// 创建 分拣出库的任务
        /// </summary>
        /// <param name="param"></param>
        /// <param name="_user"></param>
        /// <param name="_unitOfWork"></param>
        /// <returns></returns>
        Task<bool> CreateSortOutStoreTask(LES_SortOutStoreTaskPostParam param, IUser _user, IUnitOfWork _unitOfWork);

        /// <summary>
        /// 创建 回库搬运的任务
        /// </summary>
        /// <param name="param"></param>
        /// <param name="_user"></param>
        /// <param name="_unitOfWork"></param>
        /// <returns></returns>
        Task<bool> CreateaBackStoreTask(LES_SortOutStoreTaskPostParam param, IUser _user, IUnitOfWork _unitOfWork);

        /// <summary>
        /// 根据任务号查询AGV任务
        /// </summary>
        /// <param name="taskNo"></param>
        /// <returns></returns>
        Task<Les_AgvTask> GetAgvTaskByTaskNo(string taskNo);


        /// <summary>
        /// 根据 XiangziProjectTemplete任务ID 查找 该任务的详细阶段列表
        /// </summary>
        /// <param name="agvTaskId">AGV任务ID</param>
        /// <returns></returns>
        Task<List<Les_AgvTaskPhase>> GetAgvTaskPhaseList(string agvTaskId);

        /// <summary>
        /// 根据 AGV任务号 查找 该任务的详细阶段列表
        /// </summary>
        /// <param name="agvTaskId">AGV任务ID</param>
        /// <returns></returns>
        Task<List<Les_AgvTaskPhase>> GetAgvTaskPhaseListByTaskNo(string agvTaskNo);

        /// <summary>
        /// 验证站点是否被AGV任务占用
        /// </summary>
        /// <param name="station"></param>
        /// <returns></returns>
        Task<MessageModel<bool>> ValidateIsUsingAgvTaskForStation(Base_Station station);

    }
}
