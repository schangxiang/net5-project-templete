﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// XiangziProjectTemplete任务阶段服务接口 
	/// </summary>	 
    public interface ILes_TaskPhaseServices :IBaseServices<Les_TaskPhase> 
	{ 
    } 
} 
