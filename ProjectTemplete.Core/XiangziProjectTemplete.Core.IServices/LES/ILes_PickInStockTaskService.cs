﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// 分拣入库任务服务接口 
    /// </summary>	 
    public interface ILes_PickInStockTaskServices : IBaseServices<Les_PickInStockTask>
    {

        /// <summary>
        /// 更新是否分拣完成状态
        /// </summary>
        /// <param name="updateIsPickFinish">要更新成的分拣状态</param>
        /// <param name="les_PickInStockTask"></param>
        /// <param name="_user"></param>
        /// <param name="isExcludeThis">是否排除本项</param>
        /// <returns></returns>
        Task<MessageModel<bool>> UpdateIsPickFinishByBurdenWorkNo(bool updateIsPickFinish, Les_PickInStockTask les_PickInStockTask, IUser _user, bool isExcludeThis);


        /// <summary>
        /// 更新 所有本配料任务号和物料号的 的备注
        /// </summary>
        /// <param name="remark">备注</param>
        /// <param name="les_PickInStockTask"></param>
        /// <param name="_user"></param>
        /// <param name="isExcludeThis">是否排除本项</param>
        /// <returns></returns>
        Task<MessageModel<bool>> UpdateRemarkByBurdenWorkNo(string remark, Les_PickInStockTask les_PickInStockTask, IUser _user, bool isExcludeThis);
       
        }
}
