﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 回库搬运服务接口 
	/// </summary>	 
    public interface IV_BackToStoreService : IBaseServices<V_BackToStore> 
	{ 
    } 
} 
