﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 角色工序查询视图服务接口 
	/// </summary>	 
    public interface IV_ProcedureRoleServices :IBaseServices<V_ProcedureRole> 
	{ 
    } 
} 
