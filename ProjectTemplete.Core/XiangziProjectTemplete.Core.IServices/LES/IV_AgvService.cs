﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// AGV查询视图服务接口 
    /// </summary>	 
    public interface IV_AgvServices :IBaseServices<V_Agv> 
	{ 
    } 
} 
