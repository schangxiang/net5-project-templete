﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models; 
 
namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 呼叫记录服务接口 
	/// </summary>	 
    public interface ILes_CallRecordServices :IBaseServices<Les_CallRecord> 
	{ 
    } 
} 
