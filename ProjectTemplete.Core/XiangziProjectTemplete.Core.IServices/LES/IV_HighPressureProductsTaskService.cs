﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary> 
    /// 高压绕线线圈 查询视图服务接口 
    /// </summary>	 
    public interface IV_HighPressureProductsTaskServices :IBaseServices<V_HighPressureProductsTask> 
	{ 
    } 
} 
