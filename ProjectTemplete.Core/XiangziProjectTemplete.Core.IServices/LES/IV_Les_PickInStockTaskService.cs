﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 分拣入库任务视图服务接口 
	/// </summary>	 
    public interface IV_Les_PickInStockTaskServices :IBaseServices<V_Les_PickInStockTask> 
	{ 
    } 
} 
