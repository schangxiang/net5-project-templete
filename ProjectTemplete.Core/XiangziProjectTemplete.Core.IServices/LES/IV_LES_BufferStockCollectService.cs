﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IRepository.UnitOfWork;
using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.Views;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices.XiangziProjectTemplete
{
    /// <summary>
    /// 缓存区库存汇总
    /// </summary>
    public interface IV_LES_BufferStockCollectService : IBaseServices<V_LES_BufferStockCollect>
    {
        /// <summary>
        /// 分拣完成后入库
        /// </summary>
        /// <param name="param"></param>
        /// <param name="_base_MaterialServices"></param>
        /// <param name="_unitOfWork"></param>
        /// <param name="base_StationServices"></param>
        /// <param name="_user"></param>
        /// <returns></returns>
        Task<MessageModel<string>> InStoreForSortSuccess(InStorePostParam param,
           IBase_MaterialServices _base_MaterialServices, IUnitOfWork _unitOfWork,
           IBase_StationServices base_StationServices, IUser _user, IBase_CodeItemsServices base_CodeItemsServices,
           ILes_PickInStockTaskServices _Les_PickInStockTaskServices,
           ILes_Mater_V_StationServices _les_Mater_V_StationServices,
            ILes_AgvTaskServices les_AgvTaskServices
          );
    }
}
