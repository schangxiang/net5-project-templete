﻿using XiangziProjectTemplete.Core.IServices.BASE; 
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.IServices 
{	 
	/// <summary> 
	/// 呼叫任务 视图服务接口 
	/// </summary>	 
    public interface IV_CallOrderServices :IBaseServices<V_CallOrder> 
	{ 
    } 
} 
