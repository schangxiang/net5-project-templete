﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.IDS4DbModels;

namespace XiangziProjectTemplete.Core.IServices
{
    public partial interface IApplicationUserServices : IBaseServices<ApplicationUser>
    {
    }
}