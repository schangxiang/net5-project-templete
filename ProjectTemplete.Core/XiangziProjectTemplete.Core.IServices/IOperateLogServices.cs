﻿using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary>
    /// IOperateLogServices
    /// </summary>	
    public interface IOperateLogServices : IBaseServices<OperateLog>
	{
       
    }
}
                    