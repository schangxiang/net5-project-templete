using XiangziProjectTemplete.Core.IServices.BASE;
using XiangziProjectTemplete.Core.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.IServices
{
    /// <summary>
    /// UserRoleServices
    /// </summary>	
    public interface IUserRoleServices : IBaseServices<UserRole>
    {

        Task<UserRole> SaveUserRole(int uid, int rid);
        Task<int> GetRoleIdByUid(int uid);

        /// <summary>
        /// 根据用户ID获取他的角色列表
        /// </summary>
        /// <param name="uID"></param>
        /// <returns></returns>
        Task<List<Role>> GetRoleListByUserId(int uID);
    }
}

