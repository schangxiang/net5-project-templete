﻿using XiangziProjectTemplete.Core.Common.Helper;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete.LESPublicCommon;
using XiangziProjectTemplete.Core.Model.Models;
using LesCCService;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Api.Common
{
    public class WcfService
    {
        /// <summary>
        /// 验证呼叫订单
        /// </summary>
        /// <param name="requestStr"></param>
        /// <returns></returns>
        public static async Task<MessageModel<string>> ValidateCreateCallOrder(string requestStr)
        {
            using (LesCCService.LesCCServiceClient client = new LesCCService.LesCCServiceClient())
            {
                SdaResEntity sdaRes = await client.ValidateCreateCallOrderAsync(requestStr);
                if (sdaRes.resultk__BackingField)
                {
                    return MessageModel<string>.Success("成功");
                }
                else
                {
                    return MessageModel<string>.Fail("验证失败:" + sdaRes.resMsgk__BackingField,
                        sdaRes.resDatak__BackingField == null ? "" : sdaRes.resDatak__BackingField.ToString());
                }
            }
        }


        /// <summary>
        /// 呼叫订单创建
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userName"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static async Task<MessageModel<string>> CreateCallOrder(Les_CallOrder request, Thingworx_CallOrder thingworx_CallOrder, string userName, int? userId)
        {
            request.Id = Guid.NewGuid().ToString();
            var thingworx_CallOrder_CallOrderTaskNo = request.CallOrderTaskNo = TaskNoHelper.GenerateLesTaskNo("CALL");

            LesTaskModeEnum lesTaskModeEnum = (LesTaskModeEnum)Enum.Parse(typeof(LesTaskModeEnum), request.LesTaskMode.ToString());
            request.LesTaskModeName = lesTaskModeEnum.ToString();



            request.Status = Convert.ToInt32(LesTaskStatusEnum.新建任务);
            request.StatusName = Convert.ToString(LesTaskStatusEnum.新建任务);

            request.CreateTime = request.ModifyTime = DateTime.Now;
            request.CreateBy = request.ModifyBy = userName;
            request.CreateId = request.ModifyId = userId;
            request.OperationRemark = "添加";


            if (thingworx_CallOrder != null)
            {
                thingworx_CallOrder.LesTaskModeName = request.LesTaskModeName;

                thingworx_CallOrder.Status = request.Status;
                thingworx_CallOrder.StatusName = request.StatusName;

                thingworx_CallOrder.CreateTime = request.CreateTime;
                thingworx_CallOrder.CreateBy = request.CreateBy;
                thingworx_CallOrder.CreateId = request.CreateId;
                thingworx_CallOrder.OperationRemark = request.OperationRemark;

                thingworx_CallOrder.LesTaskId = request.Id;
                thingworx_CallOrder.CallOrderTaskNo = request.CallOrderTaskNo;

                thingworx_CallOrder.Id = Guid.NewGuid().ToString();

            }

            string requestStr = JsonConvert.SerializeObject(request);
            using (LesCCService.LesCCServiceClient client = new LesCCService.LesCCServiceClient())
            {
                SdaResEntity sdaRes = await client.CreateCallOrderAsync(requestStr, JsonConvert.SerializeObject(thingworx_CallOrder));
                if (sdaRes.resultk__BackingField)
                {
                    return MessageModel<string>.Success("成功", thingworx_CallOrder_CallOrderTaskNo);
                }
                else
                {
                    return MessageModel<string>.Fail("失败:" + sdaRes.resMsgk__BackingField);
                }
            }
        }
    }
}
