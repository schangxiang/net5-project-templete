﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels;
using XiangziProjectTemplete.Core.Model.Views;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Api.Common
{
    public class LesApiCommon
    {
        /// <summary>
        /// 验证用户是否登出
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_user"></param>
        /// <returns></returns>
        public static MessageModel<T> IsUserLogout<T>(IUser _user)
        {
            if (_user == null || _user.ID == 0)
            {
                return MessageModel<T>.Fail("登录用户已过期，请重新登录");
            }
            return null;
        }

        /// <summary>
        /// 判断用户是否是管理员角色
        /// </summary>
        /// <param name="userRoleServices"></param>
        /// <param name="_user"></param>
        /// <returns></returns>
        public static async Task<bool> IsManagerRole(IUserRoleServices userRoleServices, IUser _user)
        {
            List<Role> roleList = await userRoleServices.GetRoleListByUserId(_user.ID);
            if (roleList != null && roleList.Count > 0)
            {
                if (roleList.Exists(x => x.Name == "SuperAdmin"))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Base_Station 列表转换为CascaderSelectViewModel列表
        /// </summary>
        /// <param name="stationList"></param>
        /// <returns></returns>
        public static List<CascaderSelectViewModel> ConvertToCascaderSelectViewModel(List<Base_Station> stationList)
        {
            List<CascaderSelectViewModel> cascaderSelectViewModels = new List<CascaderSelectViewModel>();
            if (stationList != null)
            {
                foreach (var item in stationList)
                {
                    cascaderSelectViewModels.Add(new CascaderSelectViewModel()
                    {
                        label = item.StationName,
                        value = item.Id,
                        name = item.StationName
                    });
                }
            }
            return cascaderSelectViewModels;
        }

        /// <summary>
        /// Base_CodeItems列表转换为CascaderSelectViewModel列表
        /// </summary>
        /// <param name="codeItems"></param>
        /// <returns></returns>
        public static List<CascaderSelectViewModel> ConvertToCascaderSelectViewModel(List<Base_CodeItems> codeItems)
        {
            List<CascaderSelectViewModel> cascaderSelectViewModels = new List<CascaderSelectViewModel>();
            if (codeItems != null)
            {
                foreach (var item in codeItems)
                {
                    cascaderSelectViewModels.Add(new CascaderSelectViewModel()
                    {
                        label = item.name,
                        value = item.Id,
                        name = item.code
                    });
                }
            }
            return cascaderSelectViewModels;
        }

        /// <summary>
        /// 根据入库场景判断工位类型
        /// </summary>
        /// <param name="_InStoreSceneEnum"></param>
        /// <returns></returns>
        public static StationPlaceTypeEnum GetStationPlaceTypeByInStoreScene(int _InStoreSceneEnum)
        {
            InStoreSceneEnum inStoreSceneEnum = (InStoreSceneEnum)Enum.Parse(typeof(InStoreSceneEnum), _InStoreSceneEnum.ToString());
            switch (inStoreSceneEnum)
            {
                case InStoreSceneEnum.立库分拣完成后入缓存库:
                case InStoreSceneEnum.高压绕线线圈送货:
                case InStoreSceneEnum.高压绕线内膜送货:
                case InStoreSceneEnum.低压线圈送货:
                case InStoreSceneEnum.低压内模送货:
                    return StationPlaceTypeEnum.货物工位;
                default:
                    return StationPlaceTypeEnum.货物工位;
            }
        }

        ///// <summary>
        ///// 处理枚举的显示值
        ///// </summary>
        ///// <param name="dataList"></param>
        ///// <param name="_coilSalverServices"></param>
        ///// <returns></returns>
        //public async Task<List<WorkPieceViewModel>> ConverEnumForWorkPiece(List<Wip_WorkPiece> dataList, IWip_CoilSalverServices _coilSalverServices)
        //{
        //    if (dataList == null || dataList.Count == 0)
        //    {
        //        return new List<WorkPieceViewModel>();
        //    }
        //    var dataViewList = ListHelper.T1ToT2<List<Wip_WorkPiece>, List<WorkPieceViewModel>>(dataList);
        //    List<WorkPieceViewModel> newList = new List<WorkPieceViewModel>();
        //    foreach (var item in dataViewList)
        //    {
        //        item.WorkPieceStatusName = EnumberHelper.GetEnumDescription((WorkPieceStatusEnum)Enum.Parse(typeof(WorkPieceStatusEnum), item.WorkPieceStatus.ToString()));
        //        item.WorkPieceTaskTypeName = EnumberHelper.GetEnumDescription((WorkPieceTaskTypeEnum)Enum.Parse(typeof(WorkPieceTaskTypeEnum), item.WorkPieceTaskType.ToString()));
        //        var salverResult = await _coilSalverServices.GetCoilSalverByCode(item.SalverCode, false);
        //        if (salverResult.data != null)
        //        {
        //            int wipSalverType = ((Wip_CoilSalver)salverResult.data).WipSalverType;
        //            item.SalverCodeTypeName = EnumberHelper.GetEnumDescription((WipSalverTypeEnum)Enum.Parse(typeof(WipSalverTypeEnum), wipSalverType.ToString()));
        //        }

        //        newList.Add(item);
        //    }
        //    return newList;
        //}

        ///// <summary>
        ///// 处理枚举的显示值
        ///// </summary>
        ///// <param name="dataList"></param>
        ///// <param name="_coilSalverServices"></param>
        ///// <returns></returns>
        //public async Task<List<WorkPieceViewModel>> ConverEnumForWorkPiece(List<V_OnLineWorkPiece> dataList, IWip_CoilSalverServices _coilSalverServices)
        //{
        //    if (dataList == null || dataList.Count == 0)
        //    {
        //        return new List<WorkPieceViewModel>();
        //    }
        //    var dataViewList = ListHelper.T1ToT2<List<V_OnLineWorkPiece>, List<WorkPieceViewModel>>(dataList);
        //    List<WorkPieceViewModel> newList = new List<WorkPieceViewModel>();
        //    foreach (var item in dataViewList)
        //    {
        //        item.WorkPieceStatusName = EnumberHelper.GetEnumDescription((WorkPieceStatusEnum)Enum.Parse(typeof(WorkPieceStatusEnum), item.WorkPieceStatus.ToString()));
        //        item.WorkPieceTaskTypeName = EnumberHelper.GetEnumDescription((WorkPieceTaskTypeEnum)Enum.Parse(typeof(WorkPieceTaskTypeEnum), item.WorkPieceTaskType.ToString()));
        //        var salverResult = await _coilSalverServices.GetCoilSalverByCode(item.SalverCode, false);
        //        if (salverResult.data != null)
        //        {
        //            int wipSalverType = ((Wip_CoilSalver)salverResult.data).WipSalverType;
        //            item.SalverCodeTypeName = EnumberHelper.GetEnumDescription((WipSalverTypeEnum)Enum.Parse(typeof(WipSalverTypeEnum), wipSalverType.ToString()));
        //        }
        //        newList.Add(item);
        //    }
        //    return newList;
        //}


        public static List<V_LES_BufferStockCollectByStation> GetDataByRow(int minValue, int maxValue, List<V_LES_BufferStockCollectByStation> data)
        {
            var _list = data.Where(x =>
              Convert.ToInt32(x.StationCode.Substring(2, x.StationCode.Length - 2)) >= minValue
              && Convert.ToInt32(x.StationCode.Substring(2, x.StationCode.Length - 2)) <= maxValue
          ).OrderByDescending(x => Convert.ToInt32(x.StationCode.Substring(2, x.StationCode.Length - 2))).ToList();
            return _list;
        }


        /// <summary>
        /// 通过过滤关键字进行升序排序
        /// </summary>
        /// <param name="likeValue"></param>
        /// <param name="data"></param>
        /// <returns></returns>

        public static List<V_LES_BufferStockCollectByStation> GetDataByCodeFilterForLVDeMold(string likeValue, List<V_LES_BufferStockCollectByStation> data)
        {
            var _list = data.Where(x => x.StationCode.Contains(likeValue)
          ).OrderBy(x => x.StationCode).ToList();
            //并且特殊处理,只显示后半部分
            _list.ForEach((item) =>
            {
                //Demold-A-01,截取 A-01
                item.StationCode = item.StationCode.Substring(7, item.StationCode.Length - 7);
            });
            return _list;
        }
    }
}
