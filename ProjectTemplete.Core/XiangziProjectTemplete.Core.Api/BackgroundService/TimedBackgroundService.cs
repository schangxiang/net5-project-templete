﻿using XiangziProjectTemplete.Core.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core
{
    public class TimedBackgroundService : BackgroundService
    {
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Console.WriteLine("MyServiceA is starting.");

            //stoppingToken.Register(() => File.Create($"E:\\dotnetCore\\Practice\\Practice\\{DateTime.Now.Millisecond}.txt"));


            while (!stoppingToken.IsCancellationRequested)
            {
                var startTime = DateTime.Now;
                Console.WriteLine("MyServiceA 开始执行 " + startTime.ToString("yyyy-MM-dd HH:mm:ss"));

                await Task.Delay(TimeSpan.FromSeconds(5), stoppingToken);


                var ednTime = DateTime.Now;
                TimeSpan ts = ednTime - startTime;
                Console.WriteLine("继续执行" + ednTime.ToString("yyyy-MM-dd HH:mm:ss") + "，间隔" + ts.TotalSeconds + "秒");
            }

            Console.WriteLine("MyServiceA background task is stopping.");
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
