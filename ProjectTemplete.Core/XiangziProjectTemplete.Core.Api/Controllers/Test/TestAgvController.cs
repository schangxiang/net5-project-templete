﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.Extensions.Others;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.CommonModel;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ParamModels;
using XiangziProjectTemplete.Core.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    /// <summary> 
    /// Agv车辆 
    /// </summary> 
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Permissions.Name)]
    public class TestAgvController : ControllerBase
    {
        private readonly ILes_AgvServices _Les_AgvServices;
        private readonly IV_AgvServices _v_AgvServices;
        private readonly IUser _user;
        private readonly ILogger<Les_AgvController> _logger;

        public TestAgvController(IV_AgvServices v_AgvServices, ILes_AgvServices Les_AgvServices, IUser user, ILogger<Les_AgvController> logger)
        {
            this._v_AgvServices = v_AgvServices;
            _Les_AgvServices = Les_AgvServices;
            _user = user;
            _logger = logger;
        }


        /*
         *  这是两个接口，一个是 GetAllVehicles，一个是Get，那么这两个接口就是两个Scoped会话（Http级别）。
其中会话GetAllVehicles中有调用两次_v_AgvServices服务。

1、如果使用Transient注册的话，那么这三个 _v_AgvServices 对象实例都是不相同的。也就是每次调用_v_AgvServices都会需要重新new一个_v_AgvServices实例

2、如果使用Scoped注册的话，那么会话 GetAllVehicles中使用两次的_v_AgvServices是同一个实例，
会话 Get中使用的_v_AgvServices是一个新的实例。

3、如果使用Singleton的话，那么这三个 _v_AgvServices是同一个实例。单例嘛
         * 
         */

        [HttpGet]
        [AllowAnonymous]
        public async Task<MessageModel<List<V_Agv>>> GetAllVehicles()
        {
            var dataList = await _v_AgvServices.Query();
            dataList = dataList.OrderBy(x => x.AgvName).ToList();

            dataList = await _v_AgvServices.Query();

            return new MessageModel<List<V_Agv>>()
            {
                msg = "获取成功",
                success = true,
                response = dataList
            };
        }

        [HttpGet]
        public async Task<MessageModel<Les_Agv>> Get(int id = 0)
        {
            return new MessageModel<Les_Agv>()
            {
                msg = "获取成功",
                success = true,
                response = await _v_AgvServices.QueryById(id)
            };
        }


    }
}
