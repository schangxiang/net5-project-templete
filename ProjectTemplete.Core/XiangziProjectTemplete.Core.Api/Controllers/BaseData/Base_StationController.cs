﻿using AutoMapper;
using XiangziProjectTemplete.Core.Api.Common;
using XiangziProjectTemplete.Core.Common.Helper;
using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.Extensions.Others;
using XiangziProjectTemplete.Core.IRepository.UnitOfWork;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ParamModels;
using XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.ViewModels.BasicData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.ViewModels;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    /// <summary> 
    /// 站点管理控制器 
    /// </summary> 
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Permissions.Name)]
    public class Base_StationController : ControllerBase
    {
        private readonly IBase_StationServices _Base_StationServices;
        private readonly IBase_CodeItemsServices _base_CodeItemsServices;
        private readonly IUser _user;
        private readonly ILogger<Base_StationController> _logger;
        private readonly ILes_Mater_V_StationServices _mater_V_StationService;
        private readonly ILes_PersonHandlerRecordServices _les_PersonHandlerRecordServices;
        private readonly IMapper _autoMapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBase_BigMaterialStationServices _base_BigMaterialStationServices;
        private readonly IBase_UserStationServices _base_UserStationServices;
        private readonly IV_CodeItemsServices _v_CodeItemsServices;
        private readonly ILes_ProcedureRoleServices _les_ProcedureRoleServices;


        public Base_StationController(
            IV_CodeItemsServices v_CodeItemsServices,
            IBase_UserStationServices base_UserStationServices,
            IBase_BigMaterialStationServices base_BigMaterialStationServices,
            IUnitOfWork unitOfWork,
            IMapper autoMapper,
            ILes_PersonHandlerRecordServices les_PersonHandlerRecordServices,
            ILes_Mater_V_StationServices mater_V_StationService,
            IBase_StationServices Base_StationServices,
            IUser user, IBase_CodeItemsServices base_CodeItemsServices, ILogger<Base_StationController> logger,
            ILes_ProcedureRoleServices les_ProcedureRoleServices
            )
        {
            this._les_ProcedureRoleServices = les_ProcedureRoleServices;
            this._v_CodeItemsServices = v_CodeItemsServices;
            this._base_UserStationServices = base_UserStationServices;
            this._mater_V_StationService = mater_V_StationService;
            _Base_StationServices = Base_StationServices;
            _user = user;
            _base_CodeItemsServices = base_CodeItemsServices;
            _logger = logger;
            this._les_PersonHandlerRecordServices = les_PersonHandlerRecordServices;
            this._autoMapper = autoMapper;
            this._unitOfWork = unitOfWork;
            this._base_BigMaterialStationServices = base_BigMaterialStationServices;
        }

        /// <summary> 
        /// 分页获取站点管理列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost]
        public async Task<MessageModel<PageModel<StationViewModel>>> Get([FromBody] Base_StationParam param)
        {
            MessageModel<PageModel<StationViewModel>> result = new MessageModel<PageModel<StationViewModel>>();

            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal))
            {
                param.searchVal = string.Empty;
            }
            //这里写筛选条件 
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<Base_Station, Base_StationParam>(param);
            if (!whereConditions.IsSuccess)
            {
                return new MessageModel<PageModel<StationViewModel>>()
                {
                    msg = whereConditions.ErrMsg,
                    success = false,
                    response = null
                };
            }
;
            var queryData = await _Base_StationServices.QueryPage(whereConditions.data, param.page, param.pageSize);
            var newQueryData = ListHelper.T1ToT2<PageModel<Base_Station>, PageModel<StationViewModel>>(queryData);
            foreach (var item in newQueryData.data)
            {
                if (!string.IsNullOrEmpty(item.AllowLine))
                {
                    var aa = await _base_CodeItemsServices.QueryById(item.AllowLine);
                    item.AllowLineName = aa.name;
                }

                if (!string.IsNullOrEmpty(item.AllowProcedure))
                {
                    var procedureName = await _base_CodeItemsServices.QueryById(Convert.ToInt32(item.AllowProcedure));
                    item.AllowProcedureName = procedureName.name;
                }

                if (!string.IsNullOrEmpty(item.StoreArea))
                {
                    var storeAreaName = await _base_CodeItemsServices.QueryById(Convert.ToInt32(item.StoreArea));
                    item.StoreAreaName = storeAreaName.name;
                }

                if (!string.IsNullOrEmpty(item.PlaceType))
                {
                    var bb = await _base_CodeItemsServices.QueryById(item.PlaceType);
                    item.PlaceTypeName = bb.name;
                }

                //处理大类物料的绑定
                await GetBigMaterialStation(item);
                //var bmsList = await _base_BigMaterialStationServices.Query(x => x.StationId == item.Id);
                //if (bmsList != null && bmsList.Count > 0)
                //{
                //    item.BigMaterialStations = new List<int[]>();
                //    foreach (var bms in bmsList)
                //    {
                //        int[] arr = { bms.ProcedureCodeItemsId, bms.BigMaterialCodeItemsId };
                //        item.BigMaterialStations.Add(arr);
                //    }
                //}
            }

            result.msg = "获取成功";
            result.response = newQueryData;
            return result;

        }

        private async Task<StationViewModel> GetBigMaterialStation(StationViewModel item)
        {
            //处理大类物料的绑定
            var bmsList = await _base_BigMaterialStationServices.Query(x => x.StationId == item.Id);
            if (bmsList != null && bmsList.Count > 0)
            {
                item.BigMaterialStations = new List<int[]>();
                foreach (var bms in bmsList)
                {
                    int[] arr = { bms.ProcedureCodeItemsId, bms.BigMaterialCodeItemsId };
                    item.BigMaterialStations.Add(arr);
                }
                var bigMateridCodeItemIds = bmsList.Select(x => x.BigMaterialCodeItemsId).ToList();
                var codeList = await _base_CodeItemsServices.Query();
                var list = codeList.Where(d => bigMateridCodeItemIds.Contains(d.Id)).Select(d => d.name).ToList();
                item.Str_BigMaterialStations = string.Join(",", list);
            }
            return item;
        }

        /// <summary> 
        /// 获取单个站点管理 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet]
        [AllowAnonymous]
        public async Task<MessageModel<StationViewModel>> GetSingle(int id = 0)
        {
            Base_Station base_Station = await _Base_StationServices.QueryById(id);
            StationViewModel request = _autoMapper.Map<StationViewModel>(base_Station);
            //处理大类物料的绑定
            await GetBigMaterialStation(request);
            return new MessageModel<StationViewModel>()
            {
                msg = "获取成功",
                success = true,
                response = request
            };
        }



        /// <summary>
        /// 根据工序的码表编号和站点展示 可以看到的物料列表
        /// </summary>
        /// <param name="processSetCode"></param>
        /// <param name="stationId"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<MessageModel<List<SelectViewModel>>> GetMaterialsCodeItemsByProcessAndStation(string processSetCode, int stationId)
        {
            List<SelectViewModel> reusltDataList = new List<SelectViewModel>();
            var queryData = await _v_CodeItemsServices.Query(x => x.setCodeCode == processSetCode);
            if (queryData == null || queryData.Count == 0)
            {
                return new MessageModel<List<SelectViewModel>>()
                {
                    msg = "获取成功",
                    success = true,
                    response = new List<SelectViewModel>()
                };
            }
            List<V_CodeItems> realList = new List<V_CodeItems>();
            //根据站点ID获取这个站点能看到的物料
            MessageModel<StationViewModel> mm_station = await GetSingle(stationId);
            StationViewModel station = mm_station.response;
            if (station != null && station.BigMaterialStations != null && station.BigMaterialStations.Count > 0)
            {
                List<int> seeList = new List<int>();
                foreach (var item in station.BigMaterialStations)
                {
                    seeList.Add(item[1]);
                }
                realList = queryData.Where(x => seeList.Contains(x.Id)).ToList();
            }
            else
            {
                realList = queryData;
            }
            foreach (var item in realList)
            {
                reusltDataList.Add(new SelectViewModel()
                {
                    value = item.Id,//码表的ID值
                    label = item.name,
                    name = item.code
                });
            }
            return new MessageModel<List<SelectViewModel>>()
            {
                msg = "获取成功",
                success = true,
                response = reusltDataList
            };
        }

        #region 获取下拉列表

        /// <summary>
        /// 获取空库位的列表
        /// </summary>
        /// <param name="StoreAreaCode"></param>
        /// <param name="InStoreSceneEnum"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<MessageModel<List<Base_Station>>> GetEmptyStationList(string StoreAreaCode, int InStoreSceneEnum)
        {
            try
            {
                StationPlaceTypeEnum StationPlaceType = LesApiCommon.GetStationPlaceTypeByInStoreScene(InStoreSceneEnum);
                //验证库存区域编码是否存在
                var area_codeItemsResult = await _base_CodeItemsServices.GetSingleCodeItemByCode(StoreAreaCode);
                if (!area_codeItemsResult.success)
                {
                    return MessageModel<List<Base_Station>>.Fail(area_codeItemsResult.msg, null);
                }
                //验证站点工位类型编码是否存在
                var placetype_codeItemsResult = await _base_CodeItemsServices.GetSingleCodeItemByCode(Convert.ToInt32(StationPlaceType).ToString());
                if (!placetype_codeItemsResult.success)
                {
                    return MessageModel<List<Base_Station>>.Fail(placetype_codeItemsResult.msg, null);
                }

                return await _Base_StationServices.GetEmptyStationList(area_codeItemsResult.response.Id.ToString(), placetype_codeItemsResult.response.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError("获取空库位的列表 出现异常", ex);
                return MessageModel<List<Base_Station>>.Fail("失败:" + ex.Message);
            }
        }


        /// <summary>
        /// 用于下拉列表，展示所有的库位，并显示状态
        /// </summary>
        /// <param name="StoreAreaCode"></param>
        /// <param name="InStoreSceneEnum"></param>
        /// <param name="isNeedStationPermission">是否需要根据 用户和站点的绑定过滤</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<MessageModel<List<SelectStationViewModel>>> GetAllStationList(string StoreAreaCode, int InStoreSceneEnum, bool isNeedStationPermission)
        {
            try
            {
                StationPlaceTypeEnum StationPlaceType = LesApiCommon.GetStationPlaceTypeByInStoreScene(InStoreSceneEnum);
                //验证库存区域编码是否存在
                var area_codeItemsResult = await _base_CodeItemsServices.GetSingleCodeItemByCode(StoreAreaCode);
                if (!area_codeItemsResult.success)
                {
                    return MessageModel<List<SelectStationViewModel>>.Fail(area_codeItemsResult.msg, null);
                }
                if (area_codeItemsResult.response == null)
                {
                    return MessageModel<List<SelectStationViewModel>>.Fail("没有获取到库存区域", null);
                }
                //验证站点工位类型编码是否存在
                var placetype_codeItemsResult = await _base_CodeItemsServices.GetSingleCodeItemByCode(Convert.ToInt32(StationPlaceType).ToString());
                if (!placetype_codeItemsResult.success)
                {
                    return MessageModel<List<SelectStationViewModel>>.Fail(placetype_codeItemsResult.msg, null);
                }

                var result = await _Base_StationServices.GetAllStationList(area_codeItemsResult.response.Id.ToString(), placetype_codeItemsResult.response.Id);
                return await CommonForRetSelectStationViewModel(result, true, isNeedStationPermission);
            }
            catch (Exception ex)
            {
                _logger.LogError("获取库位的列表 出现异常", ex);
                return MessageModel<List<SelectStationViewModel>>.Fail("失败:" + ex.Message);
            }
        }

        /// <summary>
        /// 根据工序获取工序下的站点列表（显示全部）
        /// </summary>
        /// <param name="procedureCodeItemId">工序的码表ID</param>
        /// <param name="isNeedSetDisabled">是否需要设定禁用状态</param>
        /// <returns></returns>
        [HttpGet]
        //注意：必须带权限，因为里面有根据用户过滤站点，没有权限的话，就显示出来工序下的全部站点了的bug了。。
        //[AllowAnonymous]
        public async Task<MessageModel<List<SelectStationViewModel>>> GetStationsByProcedure(int procedureCodeItemId,
            bool isNeedSetDisabled)
        {
            var result = await _Base_StationServices.GetStationsByProcedure(procedureCodeItemId);
            return await CommonForRetSelectStationViewModel(result, isNeedSetDisabled, true);
        }


        /// <summary>
        /// 根据用户的工序权限，获取他下面的工序获取工序下的站点列表（显示全部）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<MessageModel<List<SelectStationViewModel>>> GetAllStationsByUserProcedure()
        {
            if (_user == null || _user.ID == 0)
            {
                return MessageModel<List<SelectStationViewModel>>.Fail("用户未登陆");
            }
            var result = await _Base_StationServices.GetAllStationsByUserProcedure(_user, _les_ProcedureRoleServices);
            return await CommonForRetSelectStationViewModel(result, false, false);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="isNeedSetDisabled">是否需要设定禁用状态</param>
        /// <param name="isNeedStationPermission">是否需要使用站点权限</param>
        /// <returns></returns>
        private async Task<MessageModel<List<SelectStationViewModel>>> CommonForRetSelectStationViewModel(MessageModel<List<Base_Station>> result,
            bool isNeedSetDisabled, bool isNeedStationPermission)
        {
            if (result.success)
            {
                List<SelectStationViewModel> selectStationViewModels = new List<SelectStationViewModel>();
                if (result.response != null && result.response.Count > 0)
                {
                    result.response = result.response.OrderBy(a => a.StationName).ToList();//重新排序
                    SelectStationViewModel selectStationView = null;

                    if (isNeedStationPermission)
                    {
                        #region 根据用户进行过滤所能看到的站点权限
                        SeeUserStationViewModel seeStationView = await _base_UserStationServices.GetSeeStationListByUser(_user);
                        List<int> seeStationList = new List<int>();
                        if (seeStationView != null && seeStationView.StationIdList.Count > 0)
                        {
                            seeStationList = seeStationView.StationIdList;
                            result.response = result.response.Where(a => seeStationList.Contains(Convert.ToInt32(a.Id))).ToList();
                        }
                        #endregion
                    }
                    foreach (var item in result.response)
                    {
                        selectStationView = ClassHelper.RotationMapping<SelectStationViewModel, Base_Station>(item);
                        if (selectStationView.IsDeleted)
                        {
                            selectStationView.DisabledMsg += "被禁用,";
                            selectStationView.Disabled = true;
                        }
                        else if (selectStationView.IsFull == 1)
                        {
                            selectStationView.DisabledMsg += "有货,";
                            selectStationView.Disabled = true;
                        }
                        else if (selectStationView.IsLock == 1)
                        {
                            selectStationView.Disabled = true;
                            selectStationView.DisabledMsg += "被锁定,";
                        }
                        else
                        {
                            //判断MVS是否有东西
                            var mvsResult = await _mater_V_StationService.GetLes_Mater_V_StationByStationId(selectStationView.Id);
                            if (mvsResult.success && mvsResult.response != null)
                            {
                                selectStationView.Disabled = true;
                                selectStationView.DisabledMsg += "有货绑定,";
                            }
                        }
                        if (!string.IsNullOrEmpty(selectStationView.DisabledMsg))
                        {
                            selectStationView.DisabledMsg = selectStationView.DisabledMsg.Substring(0, selectStationView.DisabledMsg.Length - 1);
                        }
                        else
                        {
                            selectStationView.DisabledMsg = "空闲";
                        }
                        if (isNeedSetDisabled == false)
                        {
                            selectStationView.Disabled = false;
                        }
                        selectStationViewModels.Add(selectStationView);
                    }
                }
                return MessageModel<List<SelectStationViewModel>>.Success("成功", selectStationViewModels);
            }
            else
            {
                return MessageModel<List<SelectStationViewModel>>.Fail(result.msg);
            }
        }

        #endregion


        /// <summary> 
        /// 新增站点管理 
        /// </summary> 
        /// <param name="param">要新增的站点管理对象</param> 
        /// <returns></returns> 
        [HttpPost]
        public async Task<MessageModel<string>> Post([FromBody] AddEditStationPostParam param)
        {
            var data = new MessageModel<string>();
            Base_Station request = _autoMapper.Map<Base_Station>(param);
            #region 验证站点编号和站点名称是否重复
            var isExistMessageModel = await _Base_StationServices.ValidateIsRepate(request.StationCode, request.StationName);
            if (!isExistMessageModel.success)
            {
                data.success = false;
                data.msg = isExistMessageModel.msg;
                return data;
            }
            #endregion

            request.CreateTime = request.ModifyTime = DateTime.Now;
            request.CreateBy = request.ModifyBy = _user.Name;
            request.CreateId = request.ModifyId = _user.ID;
            request.OperationRemark = "添加";

            return await _Base_StationServices.AddEditStation(param, request, AddEditDeleteEnum.Add, _unitOfWork, _user);
        }

        /// <summary> 
        /// 更新站点管理 
        /// </summary> 
        /// <param name="param">要更新的站点管理对象</param> 
        /// <returns>更新结果</returns> 
        [HttpPut]
        public async Task<MessageModel<string>> Put([FromBody] AddEditStationPostParam param)
        {
            var data = new MessageModel<string>();
            Base_Station request = _autoMapper.Map<Base_Station>(param);
            if (request.Id > 0)
            {
                #region 验证站点编号和站点名称是否重复
                var isExistMessageModel = await _Base_StationServices.ValidateIsRepate(request.StationCode, request.StationName, request.Id);
                if (!isExistMessageModel.success)
                {
                    data.success = false;
                    data.msg = isExistMessageModel.msg;
                    return data;
                }
                #endregion

                request.ModifyTime = DateTime.Now;
                request.ModifyBy = _user.Name;
                request.ModifyId = _user.ID;
                request.OperationRemark = "更新";

                return await _Base_StationServices.AddEditStation(param, request, AddEditDeleteEnum.Edit, _unitOfWork, _user);
            }
            else
            {
                data.success = false;
                data.msg = "更新失败,该站点信息不存在";
            }
            return data;
        }

        ///// <summary> 
        ///// 禁用站点管理 
        ///// </summary> 
        ///// <param name="id">主键</param> 
        ///// <returns>禁用结果</returns> 
        //[HttpDelete]
        //public async Task<MessageModel<string>> Delete(int id = 0)
        //{
        //    var data = new MessageModel<string>();
        //    if (id > 0)
        //    {
        //        var detail = await _Base_StationServices.QueryById(id);

        //        detail.ModifyTime = DateTime.Now;
        //        detail.ModifyBy = _user.Name;
        //        detail.ModifyId = _user.ID;
        //        detail.IsDeleted = true;
        //        detail.OperationRemark = "禁用站点";

        //        if (detail != null)
        //        {
        //            data.success = await _Base_StationServices.Update(detail);
        //            if (data.success)
        //            {
        //                data.msg = "禁用站点成功";
        //                data.response = detail?.Id.ObjToString();
        //            }
        //        }
        //    }
        //    else
        //    {
        //        data.success = false;
        //        data.msg = "禁用站点失败,该站点信息不存在";
        //    }

        //    return data;
        //}


        /// <summary>
        /// 更新 是否禁用
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [AllowAnonymous]
        public async Task<MessageModel<string>> UpdateIsDeleted([FromBody] Base_Station request)
        {
            var data = new MessageModel<string>();
            if (request.Id > 0)
            {
                var detail = await _Base_StationServices.QueryById(request.Id);
                if (detail != null)
                {
                    detail.ModifyTime = DateTime.Now;
                    detail.ModifyBy = _user.Name;
                    detail.ModifyId = _user.ID;
                    detail.IsDeleted = request.IsDeleted;
                    detail.Remark = detail.OperationRemark = request.OperationRemark;
                    data.success = await _Base_StationServices.Update(detail);
                    if (data.success)
                    {
                        data.msg = "更新是否禁用成功";
                        data.response = detail?.Id.ObjToString();

                        //保存人工处理日志
                        await _les_PersonHandlerRecordServices.SavePersonHandlerRecord(_user, "更新站点是否禁用", detail.Remark, "表Base_Station，id:" + request.Id
                            + ",code:" + request.StationCode + ",更新 IsDeleted：" + request.IsDeleted, "", "", request);
                    }
                }
            }
            else
            {
                data.success = false;
                data.msg = "更新是否禁用失败，数据不存在";
            }
            return data;
        }


        /// <summary>
        /// 更新 是否锁定
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [AllowAnonymous]
        public async Task<MessageModel<string>> UpdateIsLock([FromBody] Base_Station request)
        {
            var data = new MessageModel<string>();
            if (request.Id > 0)
            {
                var detail = await _Base_StationServices.QueryById(request.Id);
                if (detail != null)
                {
                    detail.ModifyTime = DateTime.Now;
                    detail.ModifyBy = _user.Name;
                    detail.ModifyId = _user.ID;
                    detail.IsLock = request.IsLock;
                    detail.Remark = detail.OperationRemark = request.OperationRemark;
                    data.success = await _Base_StationServices.Update(detail);
                    if (data.success)
                    {
                        data.msg = "更新是否锁定成功";
                        data.response = detail?.Id.ObjToString();

                        //保存人工处理日志
                        await _les_PersonHandlerRecordServices.SavePersonHandlerRecord(_user, "更新站点是否锁定", detail.Remark,

                            "表Base_Station，id:" + request.Id + ",code:" + request.StationCode + ",更新IsLock：" + request.IsLock, "", "", request);
                    }
                }
            }
            else
            {
                data.success = false;
                data.msg = "更新是否锁定失败，数据不存在";
            }
            return data;
        }

        /// <summary>
        /// 更新 是否有货
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<MessageModel<string>> UpdateIsFull([FromBody] Base_Station request)
        {
            var data = new MessageModel<string>();
            if (request.Id > 0)
            {
                var detail = await _Base_StationServices.QueryById(request.Id);
                if (detail != null)
                {
                    //判断关联表是否有数据
                    if (request.IsFull == 0)
                    {//说明要修改成无货状态
                        var mvsResult = await _mater_V_StationService.GetLes_Mater_V_StationByStationId(request.Id);
                        if (mvsResult.success == false)
                        {
                            return MessageModel<string>.Fail(mvsResult.msg);
                        }
                        if (mvsResult.response != null)
                        {
                            return MessageModel<string>.Fail("该站点已经有货物绑定数据，不允许删除！");
                        }
                    }

                    detail.ModifyTime = DateTime.Now;
                    detail.ModifyBy = _user.Name;
                    detail.ModifyId = _user.ID;
                    detail.IsFull = request.IsFull;
                    detail.Remark = detail.OperationRemark = request.OperationRemark;

                    data.success = await _Base_StationServices.Update(detail);
                    if (data.success)
                    {
                        data.msg = "更新是否有货成功";
                        data.response = detail?.Id.ObjToString();

                        //保存人工处理日志
                        await _les_PersonHandlerRecordServices.SavePersonHandlerRecord(_user,
                            "更新站点是否有货", detail.Remark, "表Base_Station，id:" + request.Id + ",更新IsFull：" + request.IsFull, "", "", request);
                    }
                }
            }
            else
            {
                data.success = false;
                data.msg = "更新是否有货失败，数据不存在";
            }
            return data;
        }
    }
}
