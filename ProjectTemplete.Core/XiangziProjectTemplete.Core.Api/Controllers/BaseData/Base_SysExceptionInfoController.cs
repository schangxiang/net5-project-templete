﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.Extensions.Others;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ParamModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    /// <summary> 
    /// 异常日志表 
    /// </summary> 
    [Route("api/[controller]/[action]")] 
    [ApiController] 
    [Authorize(Permissions.Name)] 
    public class Base_SysExceptionInfoController : ControllerBase 
    { 
        private readonly IBase_SysExceptionInfoServices _Base_SysExceptionInfoServices; 
        private readonly IUser _user; 
		private readonly ILogger<Base_SysExceptionInfoController> _logger; 
 
        public Base_SysExceptionInfoController(IBase_SysExceptionInfoServices Base_SysExceptionInfoServices, IUser user, ILogger<Base_SysExceptionInfoController> logger) 
        { 
            _Base_SysExceptionInfoServices = Base_SysExceptionInfoServices; 
            _user = user; 
			_logger = logger; 
        } 
 
        /// <summary> 
        /// 分页获取异常日志表列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost] 
        public async Task<MessageModel<PageModel<Base_SysExceptionInfo>>> Get([FromBody] Base_SysExceptionInfoParam param) 
        { 
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal)) 
            { 
                param.searchVal = string.Empty; 
            } 
             
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<Base_SysExceptionInfo, Base_SysExceptionInfoParam>(param); 
            if (!whereConditions.IsSuccess) { 
                return new MessageModel<PageModel<Base_SysExceptionInfo>>() 
                { 
                    msg = whereConditions.ErrMsg, 
                    success = false, 
                    response = null 
                }; 
            } 
			var data = await _Base_SysExceptionInfoServices.QueryPage(whereConditions.data, param.page, param.pageSize, " CreateTime desc "); 
            return new MessageModel<PageModel<Base_SysExceptionInfo>>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = data 
            }; 
 
        } 
 
        /// <summary> 
        /// 获取单个异常日志表 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet] 
        public async Task<MessageModel<Base_SysExceptionInfo>> Get(int id = 0) 
        { 
            return new MessageModel<Base_SysExceptionInfo>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = await _Base_SysExceptionInfoServices.QueryById(id) 
            }; 
        } 
    } 
} 
