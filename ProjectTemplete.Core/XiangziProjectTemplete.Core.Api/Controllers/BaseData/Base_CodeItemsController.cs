﻿using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ParamModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq;
using XiangziProjectTemplete.Core.Common.HttpContextUser;
using System.Collections.Generic;
using XiangziProjectTemplete.Core.Model.ViewModels.BasicData;
using XiangziProjectTemplete.Core.Model.ViewModels;
using XiangziProjectTemplete.Core.Extensions.Others;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Permissions.Name)]
    public class Base_CodeItemsController : ControllerBase
    {
        /// <summary>
        /// 服务器接口，因为是模板生成，所以首字母是大写的，自己可以重构下
        /// </summary>
        private readonly IBase_CodeItemsServices _base_CodeItemsServices;
        private readonly IBase_CodeSetsServices _base_CodeSetsServices;
        private readonly IV_CodeItemsServices _v_CodeItemsServices;
        private readonly IUser _user;
        private readonly ILes_PersonHandlerRecordServices _les_PersonHandlerRecordServices;
        private readonly ILes_ProcedureRoleServices _les_ProcedureRoleServices;

        public Base_CodeItemsController(
            ILes_ProcedureRoleServices les_ProcedureRoleServices,
        ILes_PersonHandlerRecordServices les_PersonHandlerRecordServices,
            IBase_CodeItemsServices Base_CodeItemsServices, IBase_CodeSetsServices Base_CodeSetsServices, IUser user
            , IV_CodeItemsServices v_CodeItemsServices)
        {
            this._les_ProcedureRoleServices = les_ProcedureRoleServices;
            _base_CodeItemsServices = Base_CodeItemsServices;
            _base_CodeSetsServices = Base_CodeSetsServices;
            _user = user;
            _v_CodeItemsServices = v_CodeItemsServices;
            this._les_PersonHandlerRecordServices = les_PersonHandlerRecordServices;
        }

        [HttpPost]
        public async Task<MessageModel<PageModel<V_CodeItems>>> Get([FromBody] Base_CodeItemsParam param)
        {
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal))
            {
                param.searchVal = string.Empty;
            }
            //这里写筛选条件
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<V_CodeItems, Base_CodeItemsParam>(param);
            if (!whereConditions.IsSuccess)
            {
                return new MessageModel<PageModel<V_CodeItems>>()
                {
                    msg = whereConditions.ErrMsg,
                    success = false,
                    response = null
                };
            }
            var dataList = await _v_CodeItemsServices.QueryPage(whereConditions.data, param.page, param.pageSize);
            if (dataList != null && dataList.data != null)
            {
                foreach (var item in dataList.data)
                {
                    // var codeSets = await _base_CodeSetsServices.Query(x => x.code == item.setCode);
                    // item.setCodeName = codeSets.FirstOrDefault() == null ? "" : codeSets.FirstOrDefault().name;
                }
            }

            return new MessageModel<PageModel<V_CodeItems>>()
            {
                msg = "获取成功",
                success = true,
                response = dataList
            };

        }

        [HttpGet]
        public async Task<MessageModel<Base_CodeItems>> Get(int id = 0)
        {
            return new MessageModel<Base_CodeItems>()
            {
                msg = "获取成功",
                success = true,
                response = await _base_CodeItemsServices.QueryById(id)
            };
        }


        /// <summary>
        /// 通过代码集编码获取代码项集合
        /// </summary>
        /// <param name="setCode">代码集编码</param>
        /// <returns>代码项集合</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<MessageModel<List<SelectViewModel>>> GetCodeItemsBySetCode(string setCode)
        {
            List<SelectViewModel> reusltDataList = new List<SelectViewModel>();
            var queryData = await _v_CodeItemsServices.Query(x => x.setCodeCode == setCode);
            foreach (var item in queryData)
            {
                reusltDataList.Add(new SelectViewModel()
                {
                    value = item.Id,//码表的ID值
                    label = item.name,
                    name = item.code
                });
            }
            return new MessageModel<List<SelectViewModel>>()
            {
                msg = "获取成功",
                success = true,
                response = reusltDataList
            };
        }

        /// <summary>
        /// 查询工序列表，使用权限，通过代码集编码获取代码项集合
        /// </summary>
        /// <returns>代码项集合</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<MessageModel<List<SelectViewModel>>> GetCodeItemsBySetCodeForGX()
        {
            var setCode = "LES_GX";
            List<SelectViewModel> reusltDataList = new List<SelectViewModel>();
            var queryData = await _v_CodeItemsServices.Query(x => x.setCodeCode == setCode);

            SeeProcedureRoleViewModel seeProcedureRoleViewModel = await _les_ProcedureRoleServices.GetProcedureListByUser(_user);
            List<int> proList = new List<int>();
            if (seeProcedureRoleViewModel.IsHasAllProcedure)
            {//全部权限
                foreach (var item in queryData)
                {
                    reusltDataList.Add(new SelectViewModel()
                    {
                        value = item.Id,//码表的ID值
                        label = item.name,
                        name = item.code
                    });
                }
            }
            else
            {
                proList = seeProcedureRoleViewModel.ProcedureList;
                foreach (var item in queryData)
                {
                    if (proList.Contains(item.Id))
                    {
                        reusltDataList.Add(new SelectViewModel()
                        {
                            value = item.Id,//码表的ID值
                            label = item.name,
                            name = item.code
                        });
                    }
                }
            }
            return new MessageModel<List<SelectViewModel>>()
            {
                msg = "获取成功",
                success = true,
                response = reusltDataList
            };
        }


        /// <summary>
        /// 通过代码编码获取代码项对象
        /// </summary>
        /// <param name="code">代码编码</param>
        /// <returns>代码项对象</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<MessageModel<Base_CodeItems>> GetCodeItemsByCode(string code)
        {
            var _list = await _base_CodeItemsServices.Query(x => x.code == code);
            if (_list == null || _list.Count == 0)
            {
                return new MessageModel<Base_CodeItems>()
                {
                    msg = "获取成功",
                    success = true,
                    response = null
                };
            }
            return new MessageModel<Base_CodeItems>()
            {
                msg = "获取成功",
                success = true,
                response = _list[0]
            };
        }

        /// <summary>
        /// 通过代码集获取代码项集合
        /// </summary>
        /// <param name="setId">代码集编码ID</param>
        /// <returns>代码项集合</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<MessageModel<List<SelectViewModel>>> GetCodeItemsBySetId(int setId)
        {
            List<SelectViewModel> reusltDataList = new List<SelectViewModel>();
            var queryData = await _v_CodeItemsServices.Query(x => x.setCode == setId);
            foreach (var item in queryData)
            {
                reusltDataList.Add(new SelectViewModel()
                {
                    value = item.Id,//码表的ID值
                    label = item.name,
                    name = item.code
                });
            }
            return new MessageModel<List<SelectViewModel>>()
            {
                msg = "获取成功",
                success = true,
                response = reusltDataList
            };
        }

        [HttpPost]
        public async Task<MessageModel<string>> Post([FromBody] Base_CodeItems request)
        {
            var data = new MessageModel<string>();

            //判断是否已经存在
            if (await _base_CodeItemsServices.IsExistCodeItemBySetCode(request.setCode, request.code))
            {
                data.msg = "该码表集已经存在，不允许重复添加！";
                return data;
            }

            request.creator = request.lastModifier = _user.Name;
            request.createTime = request.lastModifyTime = DateTime.Now;
            var id = await _base_CodeItemsServices.Add(request);
            data.success = id > 0;

            if (data.success)
            {
                data.response = id.ObjToString();
                data.msg = "添加成功";
            }

            return data;
        }

        [HttpPut]
        public async Task<MessageModel<string>> Put([FromBody] Base_CodeItems request)
        {
            var data = new MessageModel<string>();
            if (request.Id > 0)
            {
                //判断是否已经存在
                if (await _base_CodeItemsServices.IsExistCodeItemBySetCodeExcludeId(request.setCode, request.code, request.Id))
                {
                    data.msg = "该码表集已经存在，不允许更新为该码表集！";
                    return data;
                }


                request.lastModifier = _user.Name;
                request.lastModifyTime = DateTime.Now;
                data.success = await _base_CodeItemsServices.Update(request);
                if (data.success)
                {
                    data.msg = "更新成功";
                    data.response = request?.Id.ObjToString();
                }
            }

            return data;
        }

        [HttpDelete]
        public async Task<MessageModel<string>> Delete(int id = 0)
        {
            var data = new MessageModel<string>();
            if (id > 0)
            {
                var detail = await _base_CodeItemsServices.QueryById(id);

                if (detail != null)
                {
                    await _base_CodeItemsServices.Delete(detail);

                    //记录日志
                    //保存人工处理日志
                    await _les_PersonHandlerRecordServices.SavePersonHandlerRecord(_user,
                        "删除码表项", "删除", "表 Base_CodeItems ，id:" + detail.Id + ",name：" + detail.name + ",code:" + detail.code, "", "");

                    return MessageModel<string>.Success("成功");
                }
                else
                {
                    return MessageModel<string>.Fail("失败，没有找到码表项");
                }
            }
            else
            {
                return MessageModel<string>.Fail("失败，id不能等于0");
            }
        }
    }
}