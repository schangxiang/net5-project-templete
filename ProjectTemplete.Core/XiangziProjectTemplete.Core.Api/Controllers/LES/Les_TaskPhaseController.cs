﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    /// <summary> 
    /// XiangziProjectTemplete任务阶段 
    /// </summary> 
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[Authorize(Permissions.Name)]
    public class Les_TaskPhaseController : ControllerBase
    {
        private readonly ILes_TaskPhaseServices _Les_TaskPhaseServices;
        private readonly IUser _user;
        private readonly ILogger<Les_TaskPhaseController> _logger;

        public Les_TaskPhaseController(ILes_TaskPhaseServices Les_TaskPhaseServices, IUser user, ILogger<Les_TaskPhaseController> logger)
        {
            _Les_TaskPhaseServices = Les_TaskPhaseServices;
            _user = user;
            _logger = logger;
        }


        /// <summary> 
        /// 获取单个XiangziProjectTemplete任务阶段 
        /// </summary> 
        /// <param name="lesTaskId">XiangziProjectTemplete任务ID</param> 
        /// <returns>获取结果</returns> 
        [HttpGet]
        public async Task<MessageModel<List<Les_TaskPhase>>> Get(string lesTaskId)
        {
            var resultlist = await _Les_TaskPhaseServices.Query(x => x.LesTaskId == lesTaskId);
            if (resultlist != null && resultlist.Count > 0)
            {
                resultlist = resultlist.OrderBy(x => x.CreateTime).ToList();
            }
            return new MessageModel<List<Les_TaskPhase>>()
            {
                msg = "获取成功",
                success = true,
                response = resultlist
            };
        }

        /// <summary> 
        /// 获取单个XiangziProjectTemplete任务阶段 
        /// </summary> 
        /// <param name="lesTaskNo">XiangziProjectTemplete任务号</param> 
        /// <returns>获取结果</returns> 
        [HttpGet]
        [AllowAnonymous]
        public async Task<MessageModel<List<Les_TaskPhase>>> GetTaskPhaseListByLesTaskNo(string lesTaskNo)
        {
            var resultlist = await _Les_TaskPhaseServices.Query(x => x.LesTaskNo == lesTaskNo);
            if (resultlist != null && resultlist.Count > 0)
            {
                resultlist = resultlist.OrderBy(x => x.CreateTime).ToList();
            }
            return new MessageModel<List<Les_TaskPhase>>()
            {
                msg = "获取成功",
                success = true,
                response = resultlist
            };
        }
    }
}
