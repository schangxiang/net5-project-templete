﻿using XiangziProjectTemplete.Core.Common.HttpContextUser; 
using XiangziProjectTemplete.Core.Extensions.Others; 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Model.ParamModels; 
using Microsoft.AspNetCore.Authorization; 
using Microsoft.AspNetCore.Mvc; 
using System.Collections.Generic; 
using System.Threading.Tasks; 
using System; 
 
namespace XiangziProjectTemplete.Core.Api.Controllers 
{ 
    /// <summary> 
    /// 呼叫记录 
    /// </summary> 
    [Route("api/[controller]/[action]")] 
    [ApiController] 
    [Authorize(Permissions.Name)] 
    public class Les_CallRecordController : ControllerBase 
    { 
        private readonly ILes_CallRecordServices _Les_CallRecordServices; 
        private readonly IUser _user; 
 
        public Les_CallRecordController(ILes_CallRecordServices Les_CallRecordServices, IUser user) 
        { 
            _Les_CallRecordServices = Les_CallRecordServices; 
            _user = user; 
        } 
 
        /// <summary> 
        /// 分页获取呼叫记录列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost] 
        public async Task<MessageModel<PageModel<Les_CallRecord>>> Get([FromBody] Les_CallRecordParam param) 
        { 
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal)) 
            { 
                param.searchVal = string.Empty; 
            } 
             
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<Les_CallRecord, Les_CallRecordParam>(param); 
            if (!whereConditions.IsSuccess) { 
                return new MessageModel<PageModel<Les_CallRecord>>() 
                { 
                    msg = whereConditions.ErrMsg, 
                    success = false, 
                    response = null 
                }; 
            } 
			var data = await _Les_CallRecordServices.QueryPage(whereConditions.data, param.page, param.pageSize, " ModifyTime desc "); 
            return new MessageModel<PageModel<Les_CallRecord>>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = data 
            }; 
 
        } 
 
        /// <summary> 
        /// 获取单个呼叫记录 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet] 
        public async Task<MessageModel<Les_CallRecord>> Get(int id = 0) 
        { 
            return new MessageModel<Les_CallRecord>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = await _Les_CallRecordServices.QueryById(id) 
            }; 
        } 
 
        /// <summary> 
        /// 新增呼叫记录 
        /// </summary> 
        /// <param name="request">要新增的呼叫记录对象</param> 
        /// <returns></returns> 
        [HttpPost] 
        public async Task<MessageModel<string>> Post([FromBody] Les_CallRecord request) 
        { 
            var data = new MessageModel<string>(); 
 
            request.CreateTime = request.ModifyTime = DateTime.Now; 
            request.CreateBy = request.ModifyBy = _user.Name; 
            request.CreateId = request.ModifyId = _user.ID; 
            request.OperationRemark = "添加"; 
            var id = await _Les_CallRecordServices.Add(request); 
            data.success = id > 0; 
 
            if (data.success) 
            { 
                data.response = id.ObjToString(); 
                data.msg = "添加成功"; 
            } 
 
            return data; 
        } 
 
        /// <summary> 
        /// 更新呼叫记录 
        /// </summary> 
        /// <param name="request">要更新的呼叫记录对象</param> 
        /// <returns>更新结果</returns> 
        [HttpPut] 
        public async Task<MessageModel<string>> Put([FromBody] Les_CallRecord request) 
        { 
            var data = new MessageModel<string>(); 
            if (request.Id !=string.Empty) 
            { 
                request.ModifyTime = DateTime.Now; 
                request.ModifyBy = _user.Name; 
                request.ModifyId = _user.ID; 
                request.OperationRemark = "更新"; 
                data.success = await _Les_CallRecordServices.Update(request); 
                if (data.success) 
                { 
                    data.msg = "更新成功"; 
                    data.response = request?.Id.ObjToString(); 
                } 
            } 
 
            return data; 
        } 
 
        /// <summary> 
        /// 删除呼叫记录 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>删除结果</returns> 
        [HttpDelete] 
        public async Task<MessageModel<string>> Delete(int id = 0) 
        { 
            var data = new MessageModel<string>(); 
            if (id > 0) 
            { 
                var detail = await _Les_CallRecordServices.QueryById(id); 
                if (detail != null) 
                { 
                    data.success = await _Les_CallRecordServices.DeleteById(detail.Id); 
                    if (data.success) 
                    { 
                        data.msg = "删除成功"; 
                        data.response = detail?.Id.ObjToString(); 
                    } 
                } 
            } 
 
            return data; 
        } 
 
		/// <summary> 
        /// 逻辑删除呼叫记录 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>删除结果</returns> 
        [HttpDelete] 
        public async Task<MessageModel<string>> LogicDelete(int id = 0) 
        { 
            var data = new MessageModel<string>(); 
            if (id > 0) 
            { 
                var detail = await _Les_CallRecordServices.QueryById(id); 
				detail.ModifyTime = DateTime.Now; 
                detail.ModifyBy = _user.Name; 
                detail.ModifyId = _user.ID; 
                detail.IsDeleted = true; 
                detail.OperationRemark = "删除"; 
 
                if (detail != null) 
                { 
                    data.success = await _Les_CallRecordServices.Update(detail); 
                    if (data.success) 
                    { 
                        data.msg = "删除成功"; 
                        data.response = detail?.Id.ObjToString(); 
                    } 
                } 
            } 
 
            return data; 
        } 
    } 
} 
