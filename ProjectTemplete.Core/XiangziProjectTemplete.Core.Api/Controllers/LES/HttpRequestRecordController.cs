﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.Extensions.Others;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ParamModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    /// <summary> 
    /// 请求日志表 
    /// </summary> 
    [Route("api/[controller]/[action]")] 
    [ApiController] 
    [Authorize(Permissions.Name)] 
    public class HttpRequestRecordController : ControllerBase 
    { 
        private readonly IHttpRequestRecordServices _HttpRequestRecordServices; 
        private readonly IUser _user; 
		private readonly ILogger<HttpRequestRecordController> _logger; 
 
        public HttpRequestRecordController(IHttpRequestRecordServices HttpRequestRecordServices, IUser user, ILogger<HttpRequestRecordController> logger) 
        { 
            _HttpRequestRecordServices = HttpRequestRecordServices; 
            _user = user; 
			_logger = logger; 
        } 
 
        /// <summary> 
        /// 分页获取请求日志表列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost] 
        public async Task<MessageModel<PageModel<HttpRequestRecord>>> Get([FromBody] HttpRequestRecordParam param) 
        { 
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal)) 
            { 
                param.searchVal = string.Empty; 
            } 
             
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<HttpRequestRecord, HttpRequestRecordParam>(param); 
            if (!whereConditions.IsSuccess) { 
                return new MessageModel<PageModel<HttpRequestRecord>>() 
                { 
                    msg = whereConditions.ErrMsg, 
                    success = false, 
                    response = null 
                }; 
            } 
			var data = await _HttpRequestRecordServices.QueryPage(whereConditions.data, param.page, param.pageSize, " CreateTime desc "); 
            return new MessageModel<PageModel<HttpRequestRecord>>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = data 
            }; 
 
        } 
 
        /// <summary> 
        /// 获取单个请求日志表 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet] 
        public async Task<MessageModel<HttpRequestRecord>> Get(int id = 0) 
        { 
            return new MessageModel<HttpRequestRecord>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = await _HttpRequestRecordServices.QueryById(id) 
            }; 
        } 
 
      
    } 
} 
