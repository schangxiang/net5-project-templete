﻿using iWareModels;
using XiangziProjectTemplete.Core.Common.Helper;
using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.Extensions.Others;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ParamModels;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    /// <summary> 
    /// Agv异常控制器 
    /// </summary> 
    [Route("api/[controller]/[action]")] 
    [ApiController] 
    [Authorize(Permissions.Name)] 
    public class Les_AgvExcepitonController : ControllerBase 
    { 
        private readonly ILes_AgvExcepitonServices _Les_AgvExcepitonServices; 
        private readonly IUser _user; 
 
        public Les_AgvExcepitonController(ILes_AgvExcepitonServices Les_AgvExcepitonServices, IUser user) 
        { 
            _Les_AgvExcepitonServices = Les_AgvExcepitonServices; 
            _user = user; 
        } 
 
        /// <summary> 
        /// 分页获取Agv异常列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost] 
        public async Task<MessageModel<PageModel<AgvExcepitonViewModel>>> Get([FromBody] Les_AgvExcepitonParam param) 
        { 
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal)) 
            { 
                param.searchVal = string.Empty; 
            } 
             
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<Les_AgvExcepiton, Les_AgvExcepitonParam>(param); 
            if (!whereConditions.IsSuccess) { 
                return new MessageModel<PageModel<AgvExcepitonViewModel>>() 
                { 
                    msg = whereConditions.ErrMsg, 
                    success = false, 
                    response = null 
                }; 
            } 
			var data = await _Les_AgvExcepitonServices.QueryPage(whereConditions.data, param.page, param.pageSize, " ModifyTime desc ");
            //转换
            var newQueryData = ListHelper.T1ToT2<PageModel<Les_AgvExcepiton>, PageModel<AgvExcepitonViewModel>>(data);
            foreach (var item in newQueryData.data)
            {
                item.TaskStatusName = EnumberHelper.GetEnumDescription((TaskStatusEnum)Enum.Parse(typeof(TaskStatusEnum), item.TaskStatus.ToString()));
                item.TaskTypeName = EnumberHelper.GetEnumDescription((AGVTaskType)Enum.Parse(typeof(AGVTaskType), item.TaskType.ToString()));
            }
            return new MessageModel<PageModel<AgvExcepitonViewModel>>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = newQueryData
            }; 
 
        } 
 
        /// <summary> 
        /// 获取单个Agv异常 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet] 
        public async Task<MessageModel<Les_AgvExcepiton>> Get(int id = 0) 
        { 
            return new MessageModel<Les_AgvExcepiton>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = await _Les_AgvExcepitonServices.QueryById(id) 
            }; 
        } 
 
        /// <summary> 
        /// 新增Agv异常 
        /// </summary> 
        /// <param name="request">要新增的Agv异常对象</param> 
        /// <returns></returns> 
        [HttpPost] 
        public async Task<MessageModel<string>> Post([FromBody] Les_AgvExcepiton request) 
        { 
            var data = new MessageModel<string>(); 
 
            request.CreateTime = request.ModifyTime = DateTime.Now; 
            request.CreateBy = request.ModifyBy = _user.Name; 
            request.CreateId = request.ModifyId = _user.ID; 
            request.OperationRemark = "添加"; 
            var id = await _Les_AgvExcepitonServices.Add(request); 
            data.success = id > 0; 
 
            if (data.success) 
            { 
                data.response = id.ObjToString(); 
                data.msg = "添加成功"; 
            } 
 
            return data; 
        } 
 
        /// <summary> 
        /// 更新Agv异常 
        /// </summary> 
        /// <param name="request">要更新的Agv异常对象</param> 
        /// <returns>更新结果</returns> 
        [HttpPut] 
        public async Task<MessageModel<string>> Put([FromBody] Les_AgvExcepiton request) 
        { 
            var data = new MessageModel<string>(); 
            if (request.Id !=string.Empty) 
            { 
                request.ModifyTime = DateTime.Now; 
                request.ModifyBy = _user.Name; 
                request.ModifyId = _user.ID; 
                request.OperationRemark = "更新"; 
                data.success = await _Les_AgvExcepitonServices.Update(request); 
                if (data.success) 
                { 
                    data.msg = "更新成功"; 
                    data.response = request?.Id.ObjToString(); 
                } 
            } 
 
            return data; 
        } 
 
        /// <summary> 
        /// 删除Agv异常 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>删除结果</returns> 
        [HttpDelete] 
        public async Task<MessageModel<string>> Delete(int id = 0) 
        { 
            var data = new MessageModel<string>(); 
            if (id > 0) 
            { 
                var detail = await _Les_AgvExcepitonServices.QueryById(id); 
                if (detail != null) 
                { 
                    data.success = await _Les_AgvExcepitonServices.DeleteById(detail.Id); 
                    if (data.success) 
                    { 
                        data.msg = "删除成功"; 
                        data.response = detail?.Id.ObjToString(); 
                    } 
                } 
            } 
 
            return data; 
        } 
 
    } 
} 
