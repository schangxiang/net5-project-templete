﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.Extensions.Others;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ParamModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    /// <summary> 
    /// 出入库记录 
    /// </summary> 
    [Route("api/[controller]/[action]")] 
    [ApiController] 
    [Authorize(Permissions.Name)] 
    public class Les_InOutStockRecordController : ControllerBase 
    { 
        private readonly ILes_InOutStockRecordServices _Les_InOutStockRecordServices; 
        private readonly IUser _user; 
		private readonly ILogger<Les_InOutStockRecordController> _logger; 
 
        public Les_InOutStockRecordController(ILes_InOutStockRecordServices Les_InOutStockRecordServices, IUser user, ILogger<Les_InOutStockRecordController> logger) 
        { 
            _Les_InOutStockRecordServices = Les_InOutStockRecordServices; 
            _user = user; 
			_logger = logger; 
        } 
 
        /// <summary> 
        /// 分页获取出入库记录列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost] 
        public async Task<MessageModel<PageModel<Les_InOutStockRecord>>> Get([FromBody] Les_InOutStockRecordParam param) 
        { 
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal)) 
            { 
                param.searchVal = string.Empty; 
            } 
             
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<Les_InOutStockRecord, Les_InOutStockRecordParam>(param); 
            if (!whereConditions.IsSuccess) { 
                return new MessageModel<PageModel<Les_InOutStockRecord>>() 
                { 
                    msg = whereConditions.ErrMsg, 
                    success = false, 
                    response = null 
                }; 
            } 
			var data = await _Les_InOutStockRecordServices.QueryPage(whereConditions.data, param.page, param.pageSize, " CreateTime desc "); 
            return new MessageModel<PageModel<Les_InOutStockRecord>>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = data 
            }; 
 
        } 
    } 
} 
