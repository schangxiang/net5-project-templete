﻿using XiangziProjectTemplete.Core.Common.HttpContextUser; 
using XiangziProjectTemplete.Core.Extensions.Others; 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model; 
using XiangziProjectTemplete.Core.Model.CommonModel; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Model.ParamModels; 
using XiangziProjectTemplete.Core.Utility; 
using Microsoft.AspNetCore.Authorization; 
using Microsoft.AspNetCore.Mvc; 
using Microsoft.Extensions.Logging; 
using Newtonsoft.Json; 
using System.Collections.Generic; 
using System.Threading.Tasks; 
using System; 
 
namespace XiangziProjectTemplete.Core.Api.Controllers 
{ 
    /// <summary> 
    /// Thingworx 备料通知表 
    /// </summary> 
    [Route("api/[controller]/[action]")] 
    [ApiController] 
    //[Authorize(Permissions.Name)] 
    public class Thingworx_StockPreparationNoticeController : ControllerBase 
    { 
        private readonly IThingworx_StockPreparationNoticeServices _Thingworx_StockPreparationNoticeServices; 
        private readonly IUser _user; 
		private readonly ILogger<Thingworx_StockPreparationNoticeController> _logger; 
 
        public Thingworx_StockPreparationNoticeController(IThingworx_StockPreparationNoticeServices Thingworx_StockPreparationNoticeServices, IUser user, ILogger<Thingworx_StockPreparationNoticeController> logger) 
        { 
            _Thingworx_StockPreparationNoticeServices = Thingworx_StockPreparationNoticeServices; 
            _user = user; 
			_logger = logger; 
        } 
 
        /// <summary> 
        /// 分页获取Thingworx 备料通知表列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost] 
        public async Task<MessageModel<PageModel<Thingworx_StockPreparationNotice>>> Get([FromBody] Thingworx_StockPreparationNoticeParam param) 
        { 
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal)) 
            { 
                param.searchVal = string.Empty; 
            } 
             
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<Thingworx_StockPreparationNotice, Thingworx_StockPreparationNoticeParam>(param); 
            if (!whereConditions.IsSuccess) { 
                return new MessageModel<PageModel<Thingworx_StockPreparationNotice>>() 
                { 
                    msg = whereConditions.ErrMsg, 
                    success = false, 
                    response = null 
                }; 
            } 
			var data = await _Thingworx_StockPreparationNoticeServices.QueryPage(whereConditions.data, param.page, param.pageSize, " CreateTime desc "); 
            return new MessageModel<PageModel<Thingworx_StockPreparationNotice>>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = data 
            }; 
 
        } 
 
        /// <summary> 
        /// 获取单个Thingworx 备料通知表 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet] 
        public async Task<MessageModel<Thingworx_StockPreparationNotice>> Get(int id = 0) 
        { 
            return new MessageModel<Thingworx_StockPreparationNotice>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = await _Thingworx_StockPreparationNoticeServices.QueryById(id) 
            }; 
        } 
 
    } 
} 
