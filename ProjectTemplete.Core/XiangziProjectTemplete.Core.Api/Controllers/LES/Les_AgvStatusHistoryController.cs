﻿using XiangziProjectTemplete.Core.Common.HttpContextUser; 
using XiangziProjectTemplete.Core.Extensions.Others; 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model; 
using XiangziProjectTemplete.Core.Model.CommonModel; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Model.ParamModels; 
using XiangziProjectTemplete.Core.Utility; 
using Microsoft.AspNetCore.Authorization; 
using Microsoft.AspNetCore.Mvc; 
using Microsoft.Extensions.Logging; 
using Newtonsoft.Json; 
using System.Collections.Generic; 
using System.Threading.Tasks; 
using System; 
 
namespace XiangziProjectTemplete.Core.Api.Controllers 
{ 
    /// <summary> 
    /// Agv车辆状态历史记录 
    /// </summary> 
    [Route("api/[controller]/[action]")] 
    [ApiController] 
    [Authorize(Permissions.Name)] 
    public class Les_AgvStatusHistoryController : ControllerBase 
    { 
        private readonly ILes_AgvStatusHistoryServices _Les_AgvStatusHistoryServices; 
        private readonly IUser _user; 
		private readonly ILogger<Les_AgvStatusHistoryController> _logger; 
 
        public Les_AgvStatusHistoryController(ILes_AgvStatusHistoryServices Les_AgvStatusHistoryServices, IUser user, ILogger<Les_AgvStatusHistoryController> logger) 
        { 
            _Les_AgvStatusHistoryServices = Les_AgvStatusHistoryServices; 
            _user = user; 
			_logger = logger; 
        } 
 
        /// <summary> 
        /// 分页获取Agv车辆状态历史记录列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost] 
        public async Task<MessageModel<PageModel<Les_AgvStatusHistory>>> Get([FromBody] Les_AgvStatusHistoryParam param) 
        { 
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal)) 
            { 
                param.searchVal = string.Empty; 
            } 
             
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<Les_AgvStatusHistory, Les_AgvStatusHistoryParam>(param); 
            if (!whereConditions.IsSuccess) { 
                return new MessageModel<PageModel<Les_AgvStatusHistory>>() 
                { 
                    msg = whereConditions.ErrMsg, 
                    success = false, 
                    response = null 
                }; 
            } 
			var data = await _Les_AgvStatusHistoryServices.QueryPage(whereConditions.data, param.page, param.pageSize, " CreateTime desc "); 
            return new MessageModel<PageModel<Les_AgvStatusHistory>>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = data 
            }; 
 
        } 
 
        /// <summary> 
        /// 获取单个Agv车辆状态历史记录 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet] 
        public async Task<MessageModel<Les_AgvStatusHistory>> Get(int id = 0) 
        { 
            return new MessageModel<Les_AgvStatusHistory>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = await _Les_AgvStatusHistoryServices.QueryById(id) 
            }; 
        } 
 
    } 
} 
