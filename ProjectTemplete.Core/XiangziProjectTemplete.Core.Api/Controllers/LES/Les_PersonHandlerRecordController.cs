﻿using XiangziProjectTemplete.Core.Common.HttpContextUser; 
using XiangziProjectTemplete.Core.Extensions.Others; 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Model.ParamModels; 
using Microsoft.AspNetCore.Authorization; 
using Microsoft.AspNetCore.Mvc; 
using System.Collections.Generic; 
using System.Threading.Tasks; 
using System; 
 
namespace XiangziProjectTemplete.Core.Api.Controllers 
{ 
    /// <summary> 
    /// 人工处理数据记录表 
    /// </summary> 
    [Route("api/[controller]/[action]")] 
    [ApiController] 
    [Authorize(Permissions.Name)] 
    public class Les_PersonHandlerRecordController : ControllerBase 
    { 
        private readonly ILes_PersonHandlerRecordServices _Les_PersonHandlerRecordServices; 
        private readonly IUser _user; 
 
        public Les_PersonHandlerRecordController(ILes_PersonHandlerRecordServices Les_PersonHandlerRecordServices, IUser user) 
        { 
            _Les_PersonHandlerRecordServices = Les_PersonHandlerRecordServices; 
            _user = user; 
        } 
 
        /// <summary> 
        /// 分页获取人工处理数据记录表列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost] 
        public async Task<MessageModel<PageModel<Les_PersonHandlerRecord>>> Get([FromBody] Les_PersonHandlerRecordParam param) 
        { 
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal)) 
            { 
                param.searchVal = string.Empty; 
            } 
             
            var whereConditions = WhereConditionsExtensions.GetWhereConditions<Les_PersonHandlerRecord, Les_PersonHandlerRecordParam>(param); 
            if (!whereConditions.IsSuccess) { 
                return new MessageModel<PageModel<Les_PersonHandlerRecord>>() 
                { 
                    msg = whereConditions.ErrMsg, 
                    success = false, 
                    response = null 
                }; 
            } 
			var data = await _Les_PersonHandlerRecordServices.QueryPage(whereConditions.data, param.page, param.pageSize, " ModifyTime desc "); 
            return new MessageModel<PageModel<Les_PersonHandlerRecord>>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = data 
            }; 
 
        } 
 
        /// <summary> 
        /// 获取单个人工处理数据记录表 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet] 
        public async Task<MessageModel<Les_PersonHandlerRecord>> Get(int id = 0) 
        { 
            return new MessageModel<Les_PersonHandlerRecord>() 
            { 
                msg = "获取成功", 
                success = true, 
                response = await _Les_PersonHandlerRecordServices.QueryById(id) 
            }; 
        } 
 
        /// <summary> 
        /// 新增人工处理数据记录表 
        /// </summary> 
        /// <param name="request">要新增的人工处理数据记录表对象</param> 
        /// <returns></returns> 
        [HttpPost] 
        public async Task<MessageModel<string>> Post([FromBody] Les_PersonHandlerRecord request) 
        { 
            var data = new MessageModel<string>(); 
 
            request.CreateTime = request.ModifyTime = DateTime.Now; 
            request.CreateBy = request.ModifyBy = _user.Name; 
            request.CreateId = request.ModifyId = _user.ID; 
            request.OperationRemark = "添加"; 
            var id = await _Les_PersonHandlerRecordServices.Add(request); 
            data.success = id > 0; 
 
            if (data.success) 
            { 
                data.response = id.ObjToString(); 
                data.msg = "添加成功"; 
            } 
 
            return data; 
        } 
 
 
        /// <summary> 
        /// 删除人工处理数据记录表 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>删除结果</returns> 
        [HttpDelete] 
        public async Task<MessageModel<string>> Delete(int id = 0) 
        { 
            var data = new MessageModel<string>(); 
            if (id > 0) 
            { 
                var detail = await _Les_PersonHandlerRecordServices.QueryById(id); 
                if (detail != null) 
                { 
                    data.success = await _Les_PersonHandlerRecordServices.DeleteById(detail.Id); 
                    if (data.success) 
                    { 
                        data.msg = "删除成功"; 
                        data.response = detail?.Id.ObjToString(); 
                    } 
                } 
            } 
			else 
            { 
                data.success = false; 
                data.msg = "删除失败,该人工处理数据记录表不存在"; 
            } 
            return data; 
        } 
 
		/// <summary> 
        /// 逻辑删除人工处理数据记录表 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>删除结果</returns> 
        [HttpDelete] 
        public async Task<MessageModel<string>> LogicDelete(int id = 0) 
        { 
            var data = new MessageModel<string>(); 
            if (id > 0) 
            { 
                var detail = await _Les_PersonHandlerRecordServices.QueryById(id); 
                if (detail != null) 
                { 
					detail.ModifyTime = DateTime.Now; 
					detail.ModifyBy = _user.Name; 
					detail.ModifyId = _user.ID; 
					detail.IsDeleted = true; 
					detail.OperationRemark = "删除"; 
                    data.success = await _Les_PersonHandlerRecordServices.Update(detail); 
                    if (data.success) 
                    { 
                        data.msg = "删除成功"; 
                        data.response = detail?.Id.ObjToString(); 
                    } 
                } 
            } 
			else 
            { 
                data.success = false; 
                data.msg = "删除失败,该人工处理数据记录表不存在"; 
            } 
            return data; 
        } 
    } 
} 
