﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.Extensions.Others;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.CommonModel;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ParamModels;
using XiangziProjectTemplete.Core.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    /// <summary> 
    /// Thingworx发送的呼叫任务 
    /// </summary> 
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Permissions.Name)]
    public class Thingworx_CallOrderController : ControllerBase
    {
        private readonly IThingworx_CallOrderServices _Thingworx_CallOrderServices;
        private readonly IUser _user;
        private readonly ILogger<Thingworx_CallOrderController> _logger;

        public Thingworx_CallOrderController(IThingworx_CallOrderServices Thingworx_CallOrderServices, IUser user, ILogger<Thingworx_CallOrderController> logger)
        {
            _Thingworx_CallOrderServices = Thingworx_CallOrderServices;
            _user = user;
            _logger = logger;
        }

        /// <summary> 
        /// 分页获取Thingworx发送的呼叫任务列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost]
        public async Task<MessageModel<PageModel<Thingworx_CallOrder>>> Get([FromBody] Thingworx_CallOrderParam param)
        {
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal))
            {
                param.searchVal = string.Empty;
            }

            var whereConditions = WhereConditionsExtensions.GetWhereConditions<Thingworx_CallOrder, Thingworx_CallOrderParam>(param);
            if (!whereConditions.IsSuccess)
            {
                return new MessageModel<PageModel<Thingworx_CallOrder>>()
                {
                    msg = whereConditions.ErrMsg,
                    success = false,
                    response = null
                };
            }
            var data = await _Thingworx_CallOrderServices.QueryPage(whereConditions.data, param.page, param.pageSize, " ModifyTime desc ");
            return new MessageModel<PageModel<Thingworx_CallOrder>>()
            {
                msg = "获取成功",
                success = true,
                response = data
            };

        }

        /// <summary> 
        /// 获取单个Thingworx发送的呼叫任务 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet]
        public async Task<MessageModel<Thingworx_CallOrder>> Get(int id = 0)
        {
            return new MessageModel<Thingworx_CallOrder>()
            {
                msg = "获取成功",
                success = true,
                response = await _Thingworx_CallOrderServices.QueryById(id)
            };
        }
    }
}
