﻿using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.Extensions.Others;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ParamModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.Api.Controllers
{
    /// <summary> 
    /// 回库搬运 
    /// </summary> 
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Permissions.Name)]
    public class Les_BackToStoreController : ControllerBase
    {
        private readonly ILes_BackToStoreServices _Les_BackToStoreServices;
        private readonly IV_BackToStoreService _v_BackToStoreServices;
        private readonly IUser _user;

        public Les_BackToStoreController(ILes_BackToStoreServices Les_BackToStoreServices, IUser user, IV_BackToStoreService v_BackToStoreServices)
        {
            _Les_BackToStoreServices = Les_BackToStoreServices;
            _user = user;
            _v_BackToStoreServices = v_BackToStoreServices;
        }

        /// <summary> 
        /// 分页获取回库搬运列表 
        /// </summary> 
        /// <param name="param">筛选条件</param> 
        /// <returns>获取结果</returns> 
        [HttpPost]
        public async Task<MessageModel<PageModel<V_BackToStore>>> Get([FromBody] Les_BackToStoreParam param)
        {
            if (string.IsNullOrEmpty(param.searchVal) || string.IsNullOrWhiteSpace(param.searchVal))
            {
                param.searchVal = string.Empty;
            }

            var whereConditions = WhereConditionsExtensions.GetWhereConditions<V_BackToStore, Les_BackToStoreParam>(param);
            if (!whereConditions.IsSuccess)
            {
                return new MessageModel<PageModel<V_BackToStore>>()
                {
                    msg = whereConditions.ErrMsg,
                    success = false,
                    response = null
                };
            }
            var data = await _v_BackToStoreServices.QueryPage(whereConditions.data, param.page, param.pageSize, " ModifyTime desc ");
            data.data.ForEach(x =>
            {
                x.StatusName = x.Status >= 0 ? Enum.Parse(typeof(BackToStoreStatusEnum), x.Status.ToString()).ToString() : "";
            });
            return new MessageModel<PageModel<V_BackToStore>>()
            {
                msg = "获取成功",
                success = true,
                response = data
            };

        }

        /// <summary> 
        /// 获取单个回库搬运 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>获取结果</returns> 
        [HttpGet]
        public async Task<MessageModel<Les_BackToStore>> Get(int id = 0)
        {
            return new MessageModel<Les_BackToStore>()
            {
                msg = "获取成功",
                success = true,
                response = await _Les_BackToStoreServices.QueryById(id)
            };
        }

        /// <summary> 
        /// 新增回库搬运 
        /// </summary> 
        /// <param name="request">要新增的回库搬运对象</param> 
        /// <returns></returns> 
        [HttpPost]
        public async Task<MessageModel<string>> Post([FromBody] Les_BackToStore request)
        {
            var data = new MessageModel<string>();

            request.CreateTime = request.ModifyTime = DateTime.Now;
            request.CreateBy = request.ModifyBy = _user.Name;
            request.CreateId = request.ModifyId = _user.ID;
            request.OperationRemark = "添加";
            var id = await _Les_BackToStoreServices.Add(request);
            data.success = id > 0;

            if (data.success)
            {
                data.response = id.ObjToString();
                data.msg = "添加成功";
            }

            return data;
        }

        /// <summary> 
        /// 更新回库搬运 
        /// </summary> 
        /// <param name="request">要更新的回库搬运对象</param> 
        /// <returns>更新结果</returns> 
        [HttpPut]
        public async Task<MessageModel<string>> Put([FromBody] Les_BackToStore request)
        {
            var data = new MessageModel<string>();
            if (request.Id > 0)
            {
                request.ModifyTime = DateTime.Now;
                request.ModifyBy = _user.Name;
                request.ModifyId = _user.ID;
                request.OperationRemark = "更新";
                data.success = await _Les_BackToStoreServices.Update(request);
                if (data.success)
                {
                    data.msg = "更新成功";
                    data.response = request?.Id.ObjToString();
                }
            }

            return data;
        }

        /// <summary> 
        /// 删除回库搬运 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>删除结果</returns> 
        [HttpDelete]
        public async Task<MessageModel<string>> Delete(int id = 0)
        {
            var data = new MessageModel<string>();
            if (id > 0)
            {
                var detail = await _Les_BackToStoreServices.QueryById(id);
                if (detail != null)
                {
                    data.success = await _Les_BackToStoreServices.DeleteById(detail.Id);
                    if (data.success)
                    {
                        data.msg = "删除成功";
                        data.response = detail?.Id.ObjToString();
                    }
                }
            }

            return data;
        }

        /// <summary> 
        /// 逻辑删除回库搬运 
        /// </summary> 
        /// <param name="id">主键</param> 
        /// <returns>删除结果</returns> 
        [HttpDelete]
        public async Task<MessageModel<string>> LogicDelete(int id = 0)
        {
            var data = new MessageModel<string>();
            if (id > 0)
            {
                var detail = await _Les_BackToStoreServices.QueryById(id);
                detail.ModifyTime = DateTime.Now;
                detail.ModifyBy = _user.Name;
                detail.ModifyId = _user.ID;
                detail.IsDeleted = true;
                detail.OperationRemark = "删除";

                if (detail != null)
                {
                    data.success = await _Les_BackToStoreServices.Update(detail);
                    if (data.success)
                    {
                        data.msg = "删除成功";
                        data.response = detail?.Id.ObjToString();
                    }
                }
            }

            return data;
        }
    }
}
