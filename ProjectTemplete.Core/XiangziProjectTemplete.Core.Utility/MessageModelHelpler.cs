﻿using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;

namespace XiangziProjectTemplete.Core.Utility
{
    public class MessageModelHelpler
    {
        /// <summary>
        /// 公共返回方法
        /// </summary>
        /// <typeparam name="T">返回T</typeparam>
        /// <param name="redCode">返回编号</param>
        /// <param name="resMsg">返回提示</param>
        /// <param name="t">返回t</param>
        /// <returns></returns>
        public static MessageModel<T> GetReturnBody<T>(bool _success, string resMsg, T t = default(T))
        {
            return new MessageModel<T>()
            {
                msg = resMsg,
                success = _success,
                response = t
            };
        }
        /// <summary>
        /// 公共返回方法
        /// </summary>
        /// <typeparam name="T">返回T</typeparam>
        /// <param name="redCode">返回编号</param>
        /// <param name="exception">异常信息</param>
        /// <param name="resMsg">返回提示</param>
        /// <param name="t">返回t</param>
        /// <returns></returns>
        public static MessageModel<T> GetReturnBody<T>(bool _success, string resMsg, ExceptionInfo exception, T t = default(T))
        {
            //if (resCode != ResCode.SUCCESS)
            //{
            //    WipLogHelper.WriteExceptionInfo(exception);
            //}
            return new MessageModel<T>()
            {
                msg = resMsg,
                success = _success,
                response = t
            };
        }

    }
}
