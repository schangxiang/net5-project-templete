﻿using AutoMapper;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.ViewModels.BasicData;

namespace XiangziProjectTemplete.Core.AutoMapper
{
    public class CustomProfile : Profile
    {
        /// <summary>
        /// 配置构造函数，用来创建关系映射
        /// </summary>
        public CustomProfile()
        {
            CreateMap<AddEditStationPostParam, Base_Station>();
            CreateMap<Base_Station, StationViewModel>();
            //CreateMap<BlogViewModels, BlogArticle>();
        }
    }
}
