﻿
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.ViewModels.BasicData;
using XiangziProjectTemplete.Core.Services.BASE;

namespace XiangziProjectTemplete.Core.Services
{
    public class V_CodeItemsServices : BaseServices<Model.ViewModels.BasicData.V_CodeItems>, IV_CodeItemsServices
    {
        private readonly IBaseRepository<V_CodeItems> _dal;
        public V_CodeItemsServices(IBaseRepository<V_CodeItems> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }
    }
}