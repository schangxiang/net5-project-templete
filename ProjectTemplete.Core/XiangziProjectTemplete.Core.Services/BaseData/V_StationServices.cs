﻿
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Model.ViewModels.BasicData;
using XiangziProjectTemplete.Core.Services.BASE;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Services
{
    public class V_StationServices : BaseServices<V_Station>, IV_StationServices
    {
        private readonly IBaseRepository<V_Station> _dal;
        public V_StationServices(IBaseRepository<V_Station> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        /// <summary>
        /// 根据分类获取分类集合
        /// </summary>
        /// <param name="category">分类</param>
        /// <returns></returns>
        public async Task<List<V_Station>> GetStationByCategory(string category)
        {
            var searchFiler = "";
            switch (category)
            {
                //case "出库口":
                //    searchFiler = Convert.ToInt32(StationPlaceTypeEnum.出库口).ToString();
                //    break;
                //case "产线工位":
                //    searchFiler = Convert.ToInt32(StationPlaceTypeEnum.产线工位).ToString();
                //    break;
                //case "入库口":
                //    searchFiler = Convert.ToInt32(StationPlaceTypeEnum.入库口).ToString();
                //    break;
                default:
                    return null;
            }
            return await _dal.Query(x => x.PlaceTypeCode == searchFiler &&  x.IsDeleted == false);
        }
    }
}