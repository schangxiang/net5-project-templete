﻿
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Services
{
    public class Base_CodeItemsServices : BaseServices<Base_CodeItems>, IBase_CodeItemsServices
    {
        private readonly IBaseRepository<Base_CodeItems> _dal;
        private readonly IBaseRepository<Base_CodeSets> _dal_CodeSets;
        public Base_CodeItemsServices(IBaseRepository<Base_CodeSets> dal_CodeSets, IBaseRepository<Base_CodeItems> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            this._dal_CodeSets = dal_CodeSets;
        }
        /// <summary>
        /// 同一个码表集，是否已经存在了该码表项
        /// </summary>
        /// <param name="setCode">码表集编码</param>
        ///  <param name="code">码表项编码</param>
        /// <returns>true:已存在，false：不存在</returns>
        public async Task<bool> IsExistCodeItemBySetCode(int setCode, string code)
        {
            var result = false;
            var queryData = await _dal.Query(x => x.setCode == setCode && x.code == code);
            if (queryData != null && queryData.Count > 0)
            {
                result = true;
            }
            return result;
        }

        public async Task<bool> IsExistCodeItemBySetCodeExcludeId(int setCode, string code, int id)
        {
            var result = false;
            var queryData = await _dal.Query(x => x.setCode == setCode && x.code == code && x.Id != id);
            if (queryData != null && queryData.Count > 0)
            {
                result = true;
            }
            return result;
        }



        /// <summary>
        /// 根据代码编码获取码表项
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<MessageModel<Base_CodeItems>> GetSingleCodeItemByCode(string code)
        {
            var queryData = await _dal.Query(x => x.code == code);
            if (queryData != null && queryData.Count > 0)
            {
                if (queryData.Count > 1)
                {
                    return MessageModel<Base_CodeItems>.Fail("找到编码为'" + code + "'的代码项有" + queryData.Count + "条");
                }
                return MessageModel<Base_CodeItems>.Success("成功", queryData[0]);
            }
            return MessageModel<Base_CodeItems>.Success("成功", null);
        }


        /// <summary>
        /// 根据 代码集编码 获取码表项列表
        /// </summary>
        /// <param name="setCode">代码集编码</param>
        /// <returns></returns>
        public async Task<MessageModel<List<Base_CodeItems>>> GetCodeItemsBySetCode(string setCode)
        {
            var setCodeObjs = await _dal_CodeSets.Query(x => x.code == setCode);
            if (setCodeObjs != null && setCodeObjs.Count > 0)
            {
                if (setCodeObjs.Count > 1)
                {
                    return MessageModel<List<Base_CodeItems>>.Fail("找到 代码集编码 为'" + setCode + "'的代码项有" + setCodeObjs.Count + "条");
                }
                return MessageModel<List<Base_CodeItems>>.Success("成功", await _dal.Query(x => x.setCode == setCodeObjs[0].Id));
            }
            else
            {
                return MessageModel<List<Base_CodeItems>>.Success("成功", null);
            }
        }
    }
}