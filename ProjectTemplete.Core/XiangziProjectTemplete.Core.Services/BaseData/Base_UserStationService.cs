﻿
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.IRepository.Base;
using System.Threading.Tasks;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using System.Collections.Generic;
using XiangziProjectTemplete.Core.Common.HttpContextUser;
using System;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 用户站点绑定表服务接口实现 
    /// </summary>	 
    public class Base_UserStationServices : BaseServices<Base_UserStation>, IBase_UserStationServices
    {
        private readonly IBaseRepository<Base_UserStation> _dal;
        public Base_UserStationServices(IBaseRepository<Base_UserStation> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }


        /// <summary>
        /// 根据用户获取他能看到的工序列表
        /// </summary>
        /// <param name="_user"></param>
        /// <returns></returns>
        public async Task<SeeUserStationViewModel> GetSeeStationListByUser(IUser _user)
        {
            SeeUserStationViewModel seeUserStationViewModel = new SeeUserStationViewModel();
            seeUserStationViewModel.StationIdList = new List<int>();

            List<int> stationList = new List<int>();
            try
            {
                //根据 用户 获取能看到的站点列表
                List<Base_UserStation> usList = await _dal.Query(x => x.uID == _user.ID);
                if (usList != null && usList.Count == 1)
                {
                    var bindStr = usList[0].BindStation;
                    if (string.IsNullOrEmpty(bindStr) == false)
                    {
                        var arr = bindStr.Split(',');
                        foreach (var a in arr)
                        {
                            stationList.Add(Convert.ToInt32(a));
                        }
                        seeUserStationViewModel.StationIdList = stationList;
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }

            return seeUserStationViewModel;
        }
    }
}
