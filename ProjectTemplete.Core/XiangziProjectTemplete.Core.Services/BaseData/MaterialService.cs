﻿
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.Model;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 物料管理表服务接口实现 
    /// </summary>	 
    public class Base_MaterialServices : BaseServices<Base_Material>, IBase_MaterialServices
    {
        private readonly IBaseRepository<Base_Material> _dal;
        public Base_MaterialServices(IBaseRepository<Base_Material> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        /// <summary>
        /// 根据物料名称查找物料
        /// </summary>
        /// <param name="materialName">物料名称</param>
        /// <param name="noFoundThrowError">如果没有发现物料，是否需要报警</param>
        /// <returns></returns>
        public async Task<MessageModel<Base_Material>> GetMaterialByName(string materialName, bool noFoundThrowError)
        {
            var maters = await _dal.Query(x => x.MaterialName == materialName);

            if (maters == null || maters.Count == 0)
            {
                if (noFoundThrowError)
                {
                    return MessageModel<Base_Material>.Fail("根据名称'" + materialName + "'没有找到物料");
                }
                else
                {
                    return MessageModel<Base_Material>.Success("成功", null);
                }
            }
            if (maters.Count > 1)
            {
                return MessageModel<Base_Material>.Fail("根据名称'" + materialName + "'找到" + maters.Count + "个物料");
            }
            return MessageModel<Base_Material>.Success("成功", maters[0]);
        }
    }
}
