﻿
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.IRepository.Base;
using System.Threading.Tasks;
using XiangziProjectTemplete.Core.Model;

namespace XiangziProjectTemplete.Core.Services
{
    public class Base_SalverServices : BaseServices<Base_Salver>, IBase_SalverServices
    {
        private readonly IBaseRepository<Base_Salver> _dal;
        public Base_SalverServices(IBaseRepository<Base_Salver> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        /// <summary>
        /// 根据编号查找托盘
        /// </summary>
        /// <param name="salverCode">托盘号</param>
        /// <param name="noFoundThrowError">如果没有发现托盘，是否需要报警</param>
        /// <returns></returns>
        public async Task<MessageModel<Base_Salver>> GetSalverByCode(string salverCode, bool noFoundThrowError)
        {
            var salvers = await _dal.Query(x => x.SalverCode == salverCode);

            if (salvers == null || salvers.Count == 0)
            {
                if (noFoundThrowError)
                {
                    return MessageModel<Base_Salver>.Fail("根据编号'" + salverCode + "'没有找到托盘");
                }
                else
                {
                    return MessageModel<Base_Salver>.Success("成功", null);
                }
            }
            if (salvers.Count > 1)
            {
                return MessageModel<Base_Salver>.Fail("根据编号'" + salverCode + "'找到" + salvers.Count + "个托盘");
            }
            return MessageModel<Base_Salver>.Success("成功", salvers[0]);
        }
    }
}