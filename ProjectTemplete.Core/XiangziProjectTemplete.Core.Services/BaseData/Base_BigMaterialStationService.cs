﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 大类物料和站点绑定信息服务接口实现 
	/// </summary>	 
    public class Base_BigMaterialStationServices : BaseServices<Base_BigMaterialStation>, IBase_BigMaterialStationServices 
    { 
        private readonly IBaseRepository<Base_BigMaterialStation> _dal; 
        public Base_BigMaterialStationServices(IBaseRepository<Base_BigMaterialStation> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
