﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base;
using System;
using XiangziProjectTemplete.Core.Model;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 异常日志表服务接口实现 
	/// </summary>	 
    public class Base_SysExceptionInfoServices : BaseServices<Base_SysExceptionInfo>, IBase_SysExceptionInfoServices 
    {
        private readonly IBaseRepository<Base_SysExceptionInfo> _dal;
        private readonly ILogger<Base_SysExceptionInfoServices> _logger;

        public Base_SysExceptionInfoServices(IBaseRepository<Base_SysExceptionInfo> dal, ILogger<Base_SysExceptionInfoServices> logger)
        {
            this._dal = dal;
            this._logger = logger;
            base.BaseDal = dal;
        }


        /// <summary>
        /// 初始化异常信息类(仅仅是获取异常信息对象)
        /// </summary>
        /// <typeparam name="T">入参类</typeparam>
        /// <param name="namespaceName">日志类型</param>
        /// <param name="namespaceName">当前命名空间名</param>
        /// <param name="exceptionFun">方法名</param>
        /// <param name="param">入参类</param>
        /// <param name="exceptionSource">异常方向，默认是WIP接收</param>
        /// <param name="msgSource">消息来源 </param>
        /// <returns></returns>
        public static Base_SysExceptionInfo GetExceptionInfo<T>(string namespaceName, string exceptionFun, T param,
            ExceptionLevel exceptionLevel = ExceptionLevel.BusinessError,
        string key1 = "", string key2 = "",
            ExceptionSource exceptionSource = ExceptionSource.Receive
             , string msgSource = "")
        {
            Base_SysExceptionInfo exception = new Base_SysExceptionInfo()
            {
                Id = Guid.NewGuid().ToString(),
                creator = "sys",
                key1 = key1,
                key2 = key2,
                exceptionLevel = Convert.ToInt32(exceptionLevel).ToString(),//异常级别：默认是业务错误
                exceptionFun = namespaceName + "." + exceptionFun,//异常方法名
                exceptionSource = Convert.ToInt32(exceptionSource).ToString(),//异常方向
                sourceData = JsonConvert.SerializeObject(param), //入参
            };
            return exception;
        }


        /// <summary>
        /// Error情况下的异常信息赋值
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="exception"></param>
        public static void GetExceptionInfoForError(string errMsg, Exception ex, ref Base_SysExceptionInfo exception)
        {
            exception.exceptionMsg = errMsg + ":" + ex.Message + "|错误发生位置:" + ex.StackTrace;//增加错误位置 【EditBy shaocx,2020-04-01】
            exception.exceptionLevel = Convert.ToInt32(ExceptionLevel.Error).ToString();
            exception.exceptionData = JsonConvert.SerializeObject(ex);
            //*/
        }
        /// <summary>
        /// 插入异常数据内容
        /// </summary>
        /// <param name="eie">异常对象</param>
        /// <param name="isWriteTextLog">是否需要写入文本日志</param>
        public void InsertExceptionInfo(Base_SysExceptionInfo eie, bool isWriteTextLog = false)
        {
            Task.Run(() =>
            {
                try
                {
                    eie.createTime = DateTime.Now;
                    eie.host = "";

                    _dal.Add(eie);
                }
                catch (Exception ex)
                {
                    _logger.LogError("插入异常数据内容出现异常:" + JsonConvert.SerializeObject(ex) + ",eie:" + JsonConvert.SerializeObject(eie));
                }
            });
        }
    } 
} 
