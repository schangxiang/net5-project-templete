﻿using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Services.BASE;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 用户站点绑定服务接口实现 
    /// </summary>	 
    public class V_UserStationServices : BaseServices<V_UserStation>, IV_UserStationServices 
    { 
        private readonly IBaseRepository<V_UserStation> _dal; 
        public V_UserStationServices(IBaseRepository<V_UserStation> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        }
    } 
} 
