﻿
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.IRepository.Base;

namespace XiangziProjectTemplete.Core.Services
{
    public class Base_CodeSetsServices : BaseServices<Base_CodeSets>, IBase_CodeSetsServices
    {
        private readonly IBaseRepository<Base_CodeSets> _dal;
        public Base_CodeSetsServices(IBaseRepository<Base_CodeSets> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }
    }
}