using XiangziProjectTemplete.Core.Common;
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary>
    /// UserRoleServices
    /// </summary>	
    public class UserRoleServices : BaseServices<UserRole>, IUserRoleServices
    {
        private IBaseRepository<Role> _dal_Role;
        IBaseRepository<UserRole> _dal;
        public UserRoleServices(IBaseRepository<UserRole> dal, IBaseRepository<Role> dal_Role)
        {
            this._dal_Role = dal_Role;
            this._dal = dal;
            base.BaseDal = dal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="rid"></param>
        /// <returns></returns>
        public async Task<UserRole> SaveUserRole(int uid, int rid)
        {
            UserRole userRole = new UserRole(uid, rid);

            UserRole model = new UserRole();
            var userList = await base.Query(a => a.UserId == userRole.UserId && a.RoleId == userRole.RoleId);
            if (userList.Count > 0)
            {
                model = userList.FirstOrDefault();
            }
            else
            {
                var id = await base.Add(userRole);
                model = await base.QueryById(id);
            }

            return model;

        }

        /// <summary>
        /// 根据用户ID获取他的角色列表
        /// </summary>
        /// <param name="uID"></param>
        /// <returns></returns>
        public async Task<List<Role>> GetRoleListByUserId(int uID)
        {
            //根据用户ID获取角色列表
            var allUserRoles = await _dal.Query(d => d.IsDeleted == false);
            var currentUserRoles = allUserRoles.Where(d => d.UserId == uID).Select(d => d.RoleId).ToList();
            return await _dal_Role.Query(x => currentUserRoles.Contains(x.Id));
        }

        [Caching(AbsoluteExpiration = 30)]
        public async Task<int> GetRoleIdByUid(int uid)
        {
            return ((await base.Query(d => d.UserId == uid)).OrderByDescending(d => d.Id).LastOrDefault()?.RoleId).ObjToInt();
        }
    }
}
