using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.IRepository;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.IRepository.Base;

namespace XiangziProjectTemplete.Core.Services
{	
	/// <summary>
	/// ModulePermissionServices
	/// </summary>	
	public class ModulePermissionServices : BaseServices<ModulePermission>, IModulePermissionServices
    {

        IBaseRepository<ModulePermission> _dal;
        public ModulePermissionServices(IBaseRepository<ModulePermission> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }
       
    }
}
