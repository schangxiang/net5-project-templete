﻿using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.Model.IDS4DbModels;
using XiangziProjectTemplete.Core.Services.BASE;

namespace XiangziProjectTemplete.Core.IServices
{
    public class ApplicationUserServices : BaseServices<ApplicationUser>, IApplicationUserServices
    {

        IBaseRepository<ApplicationUser> _dal;
        public ApplicationUserServices(IBaseRepository<ApplicationUser> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

    }
}