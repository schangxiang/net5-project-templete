using XiangziProjectTemplete.Core.Common;
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary>
    /// RoleServices
    /// </summary>	
    public class RoleServices : BaseServices<Role>, IRoleServices
    {

        IBaseRepository<Role> _dal;
        public RoleServices(IBaseRepository<Role> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public async Task<Role> SaveRole(string roleName)
        {
            Role role = new Role(roleName);
            Role model = new Role();
            var userList = await base.Query(a => a.Name == role.Name && a.Enabled);
            if (userList.Count > 0)
            {
                model = userList.FirstOrDefault();
            }
            else
            {
                var id = await base.Add(role);
                model = await base.QueryById(id);
            }

            return model;

        }

        [Caching(AbsoluteExpiration = 30)]
        public async Task<string> GetRoleNameByRid(int rid)
        {
            return ((await base.QueryById(rid))?.Name);
        }

        /// <summary>
        /// 是否存在同名的角色
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> IsExistSameRoleName(string roleName, int id = 0)
        {
            var result = false;
            List<Role> dbRoles = null;
            if (id != 0)
            {
                dbRoles = await _dal.Query(x => (x.Name == roleName) && x.Id != id);
            }
            else
            {
                dbRoles = await _dal.Query(x => x.Name == roleName);
            }
            if (dbRoles != null && dbRoles.Count > 0)
            {
                result = true;
            }
            return result;
        }
    }
}
