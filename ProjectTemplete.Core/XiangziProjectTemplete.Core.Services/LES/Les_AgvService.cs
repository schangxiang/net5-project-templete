﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// Agv车辆服务接口实现 
	/// </summary>	 
    public class Les_AgvServices : BaseServices<Les_Agv>, ILes_AgvServices 
    { 
        private readonly IBaseRepository<Les_Agv> _dal; 
        public Les_AgvServices(IBaseRepository<Les_Agv> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
