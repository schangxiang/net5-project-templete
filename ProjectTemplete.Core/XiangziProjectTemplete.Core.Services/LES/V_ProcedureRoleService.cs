﻿
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Services.BASE;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 角色工序查询视图服务接口实现 
    /// </summary>	 
    public class V_ProcedureRoleServices : BaseServices<V_ProcedureRole>, IV_ProcedureRoleServices 
    { 
        private readonly IBaseRepository<V_ProcedureRole> _dal; 
        public V_ProcedureRoleServices(IBaseRepository<V_ProcedureRole> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
