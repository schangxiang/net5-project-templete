﻿
using XiangziProjectTemplete.Core.Common.HttpContextUser;
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Services.BASE;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 角色工序绑定服务接口实现 
    /// </summary>	 
    public class Les_ProcedureRoleServices : BaseServices<Les_ProcedureRole>, ILes_ProcedureRoleServices
    {
        private readonly IBaseRepository<Les_ProcedureRole> _dal;
        private readonly IBaseRepository<UserRole> _dal_UserRole;
        public Les_ProcedureRoleServices(IBaseRepository<UserRole> dal_UserRole, IBaseRepository<Les_ProcedureRole> dal)
        {
            this._dal_UserRole = dal_UserRole;
            this._dal = dal;
            base.BaseDal = dal;
        }

        /// <summary>
        /// 根据用户角色获取他能看到的工序列表
        /// </summary>
        /// <param name="_user"></param>
        /// <returns></returns>
        public async Task<SeeProcedureRoleViewModel> GetProcedureListByUser(IUser _user)
        {
            SeeProcedureRoleViewModel seeProcedureRoleViewModel = new SeeProcedureRoleViewModel();
            seeProcedureRoleViewModel.IsHasAllProcedure = false;
            seeProcedureRoleViewModel.ProcedureList = new List<int>();

            List<int> procedureList = new List<int>();
            try
            {
                //根据用户ID获取角色列表
                var allUserRoles = await _dal_UserRole.Query(d => d.IsDeleted == false);
                var currentUserRoles = allUserRoles.Where(d => d.UserId == _user.ID).Select(d => d.RoleId).ToList();
                List<int> roleIds = currentUserRoles;
                //根据角色列表获取能看到的工序列表
                List<Les_ProcedureRole> prList = await _dal.Query(x => roleIds.Contains(x.RoleId));
                if (prList != null && prList.Count > 0)
                {
                    prList.ForEach(x =>
                    {
                        if (x.IsHasAllProcedure)
                        {//全部权限
                            seeProcedureRoleViewModel.IsHasAllProcedure = true;
                        }
                        else if (!string.IsNullOrEmpty(x.AllowProcedure))
                        {
                            seeProcedureRoleViewModel.IsHasAllProcedure = false;
                            string[] arr = x.AllowProcedure.Split(",");
                            foreach (var a in arr)
                            {
                                procedureList.Add(a.ObjToInt());
                            }
                            seeProcedureRoleViewModel.ProcedureList = procedureList;
                        }
                    });
                }
            }
            catch (System.Exception)
            {
                throw;
            }

            return seeProcedureRoleViewModel;
        }
    }
}
