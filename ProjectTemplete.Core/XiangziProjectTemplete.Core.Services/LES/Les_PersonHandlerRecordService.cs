﻿
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.Model;
using System.Threading.Tasks;
using XiangziProjectTemplete.Core.Common.HttpContextUser;
using System;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 人工处理数据记录表服务接口实现 
    /// </summary>	 
    public class Les_PersonHandlerRecordServices : BaseServices<Les_PersonHandlerRecord>, ILes_PersonHandlerRecordServices
    {
        private readonly IBaseRepository<Les_PersonHandlerRecord> _dal;
        public Les_PersonHandlerRecordServices(IBaseRepository<Les_PersonHandlerRecord> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }


        /// <summary>
        ///  保存人工处理数据记录
        /// </summary>
        /// <param name="piece"></param>
        /// <param name="_user"></param>
        /// <param name="manualHandlerRemark"></param>
        /// <returns></returns>
        public async Task<MessageModel<bool>> SavePersonHandlerRecord(IUser _user,
            string handlerType, string manualHandlerRemark
            , string operationRemark,
            string key1, string key2,
            Base_Station _Station = null)
        {
            try
            {
                Les_PersonHandlerRecord wip_PersonHandler = new Les_PersonHandlerRecord()
                {
                    Id = Guid.NewGuid().ToString(),

                    ManualHandlerRemark = manualHandlerRemark,
                    ManualHandlerUser = _user.Name,
                    ManualHandlerTime = DateTime.Now,

                    HandlerType = handlerType,


                    OperationRemark = operationRemark,



                    CreateBy = _user.Name,
                    CreateId = _user.ID,
                    ModifyId = _user.ID,
                    ModifyBy = _user.Name,
                    ModifyTime = DateTime.Now,
                    CreateTime = DateTime.Now,

                    Key1 = key1,
                    Key2 = key2

                };

                if (_Station != null)
                {
                    wip_PersonHandler.StationCode = _Station.StationCode;
                    wip_PersonHandler.StationName = _Station.StationName;
                }

                var i = await _dal.Add(wip_PersonHandler);
                if (i <= 0)
                {
                    return new MessageModel<bool>()
                    {
                        msg = "保存人工处理数据记录表失败",
                        success = false,
                        response = false
                    };
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            return new MessageModel<bool>()
            {
                msg = "成功",
                success = true,
                response = true
            };
        }

    }
}
