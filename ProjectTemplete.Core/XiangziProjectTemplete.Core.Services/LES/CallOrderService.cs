﻿
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.IRepository.UnitOfWork;
using XiangziProjectTemplete.Core.Common.HttpContextUser;
using System.Threading.Tasks;
using XiangziProjectTemplete.Core.Model;
using System;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 叫料工单服务接口实现 
    /// </summary>	 
    public class Les_CallOrderServices : BaseServices<Les_CallOrder>, ILes_CallOrderServices
    {
        private readonly IBaseRepository<Les_CallOrder> _dal;
        public Les_CallOrderServices(IBaseRepository<Les_CallOrder> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }



        public async Task<MessageModel<bool>> AddCallOrder(CallOrderPostParam param,
           IBase_MaterialServices _base_MaterialServices, IUnitOfWork _unitOfWork,
           IBase_StationServices base_StationServices, IUser _user, IBase_CodeItemsServices base_CodeItemsServices
          )
        {
            try
            {
                var operationRemark = "工作号:" + param.WorkNo + ",物料:" + param.MaterialCodeItemId + "入库。";
                _unitOfWork.BeginTran();//开启事务

                //1、验证要叫料的物料在立库缓存区是否都分拣完成，是否有料
                //2、验证目标位置是否有空闲位置
                //3、验证通过，新建缺料呼叫
                //3、新建AGV任务

                //保存物料信息
                //Base_Material material = new Base_Material();

                //material.MaterialCodeItemId = param.MaterialCodeItemId;
                //var codeItem_Material = await base_CodeItemsServices.QueryById(param.MaterialCodeItemId);
                //if (codeItem_Material == null)
                //{
                //    return MessageModel<bool>.Fail("没有找到物料码表项值为'" + param.MaterialCodeItemId + "'的物料");
                //}
                //material.MaterialName = codeItem_Material.name;
                //material.MaterialCode = codeItem_Material.code;

                //material.AllowProcedure = param.AllowProcedure;
                //material.CargoNum = param.CargoNum;
                //material.CargoType = param.CargoType;
                //material.CargoWeight = param.CargoWeight;

                //material.Id = Guid.NewGuid().ToString();
                //material.CreateTime = material.ModifyTime = DateTime.Now;
                //material.CreateBy = material.ModifyBy = _user.Name;
                //material.CreateId = material.ModifyId = _user.ID;
                //material.InStoreTime = DateTime.Now;
                //material.OperationRemark = operationRemark + "新增物料";
                //await _dal_Material.Add(material);

                ////搜索库位
                //var toStation = await base_StationServices.QueryById(param.StationId);
                //if (toStation == null)
                //{
                //    return MessageModel<bool>.Fail("指定的站点不存在！");
                //}
                //if (toStation.IsLock == 1) return MessageModel<bool>.Fail("指定的站点'" + toStation.StationName + "'已经被锁定！");
                //if (toStation.IsFull == 1) return MessageModel<bool>.Fail("指定的站点'" + toStation.StationName + "'已经有货！");

                ////修改库位状态
                //toStation.IsFull = 1;
                ////toStation.IsLock = 1;//不需要锁定库位！
                //toStation.ModifyBy = _user.Name;
                //toStation.ModifyTime = DateTime.Now;
                //toStation.OperationRemark = operationRemark + "修改为库存有货";
                //await _dal_Station.Update(toStation);

                ////绑定库位
                //Les_Mater_V_Station les_Mater_V_Station = new Les_Mater_V_Station()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    StationId = toStation.Id,
                //    MaterialId = material.Id,
                //    CreateTime = DateTime.Now,
                //    ModifyTime = DateTime.Now,
                //    CreateBy = _user.Name,
                //    ModifyBy = _user.Name,
                //    OperationRemark = operationRemark + "绑定库位"
                //};
                //await _dal_Les_Mater_V_Station.Add(les_Mater_V_Station);

                _unitOfWork.CommitTran();
            }
            catch (System.Exception)
            {
                _unitOfWork.RollbackTran();
                throw;
            }

            return MessageModel<bool>.Success("成功");
        }
    }
}
