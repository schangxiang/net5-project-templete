﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 回库搬运服务接口实现 
	/// </summary>	 
    public class Les_BackToStoreServices : BaseServices<Les_BackToStore>, ILes_BackToStoreServices
    { 
        private readonly IBaseRepository<Les_BackToStore> _dal; 
        public Les_BackToStoreServices(IBaseRepository<Les_BackToStore> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    }
} 
