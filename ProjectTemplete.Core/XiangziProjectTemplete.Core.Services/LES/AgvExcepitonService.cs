﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// Agv异常服务接口实现 
	/// </summary>	 
    public class Les_AgvExcepitonServices : BaseServices<Les_AgvExcepiton>, ILes_AgvExcepitonServices 
    { 
        private readonly IBaseRepository<Les_AgvExcepiton> _dal; 
        public Les_AgvExcepitonServices(IBaseRepository<Les_AgvExcepiton> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
