﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 分拣入库任务视图服务接口实现 
	/// </summary>	 
    public class V_Les_PickInStockTaskServices : BaseServices<V_Les_PickInStockTask>, IV_Les_PickInStockTaskServices 
    { 
        private readonly IBaseRepository<V_Les_PickInStockTask> _dal; 
        public V_Les_PickInStockTaskServices(IBaseRepository<V_Les_PickInStockTask> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
