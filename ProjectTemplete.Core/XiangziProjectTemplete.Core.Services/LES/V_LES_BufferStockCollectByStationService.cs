﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.Model.Views;

namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 根据站点查询的库存查询视图服务接口实现 
	/// </summary>	 
    public class V_LES_BufferStockCollectByStationServices : BaseServices<V_LES_BufferStockCollectByStation>, IV_LES_BufferStockCollectByStationServices 
    { 
        private readonly IBaseRepository<V_LES_BufferStockCollectByStation> _dal; 
        public V_LES_BufferStockCollectByStationServices(IBaseRepository<V_LES_BufferStockCollectByStation> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
