﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 请求日志表服务接口实现 
	/// </summary>	 
    public class HttpRequestRecordServices : BaseServices<HttpRequestRecord>, IHttpRequestRecordServices 
    { 
        private readonly IBaseRepository<HttpRequestRecord> _dal; 
        public HttpRequestRecordServices(IBaseRepository<HttpRequestRecord> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
