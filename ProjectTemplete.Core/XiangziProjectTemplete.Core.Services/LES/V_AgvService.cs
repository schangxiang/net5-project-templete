﻿
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Services.BASE;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// AGV查询视图服务接口实现 
    /// </summary>	 
    public class V_AgvServices : BaseServices<V_Agv>, IV_AgvServices 
    { 
        private readonly IBaseRepository<V_Agv> _dal; 
        public V_AgvServices(IBaseRepository<V_Agv> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
