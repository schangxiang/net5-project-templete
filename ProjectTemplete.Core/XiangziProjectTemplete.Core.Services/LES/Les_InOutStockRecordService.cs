﻿
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.IRepository.Base;
using System.Threading.Tasks;
using XiangziProjectTemplete.Core.Model;
using XiangziProjectTemplete.Core.Common.HttpContextUser;
using System;
using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete.LESPublicCommon;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 出入库记录服务接口实现 
    /// </summary>	 
    public class Les_InOutStockRecordServices : BaseServices<Les_InOutStockRecord>, ILes_InOutStockRecordServices
    {
        private readonly IBaseRepository<Les_InOutStockRecord> _dal;
        public Les_InOutStockRecordServices(IBaseRepository<Les_InOutStockRecord> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }


        /// <summary>
        /// 保存 出入库记录
        /// </summary>
        /// <param name="_user"></param>
        /// <param name="inOutStockTypeEnum"></param>
        /// <param name="handlerType"></param>
        /// <param name="manualHandlerRemark"></param>
        /// <param name="operationRemark"></param>
        /// <param name="base_Station"></param>
        /// <returns></returns>
        public async Task<MessageModel<bool>> SaveInOutStockRecord(IUser _user,
             InOutStockTypeEnum inOutStockTypeEnum, string operationRemark, Base_Station base_Station)
        {
            try
            {
                Les_InOutStockRecord record = new Les_InOutStockRecord()
                {
                    Id = Guid.NewGuid().ToString(),

                    InOutStockType = Convert.ToInt32(inOutStockTypeEnum),
                    InOutStockTypeName = inOutStockTypeEnum.ToString(),



                    CreateTime = DateTime.Now,
                    CreateId = _user.ID,
                    CreateBy = _user.Name,

                    //MaterialCode = currentTask.MaterialCode,
                    //MaterialName = currentTask.MaterialName,


                    //AGV_TaskType = currentTask.TaskType,
                    //AGV_TaskDesc = currentTask.TaskDesc,
                    //AGV_TaskTypeName = currentTask.TaskTypeName,
                    //AgvNameDesc = currentTask.AgvNameDesc,
                    //ProcessingVehicle = currentTask.ProcessingVehicle,


                    OperationRemark = operationRemark

                };
                record.StationId = base_Station.Id;
                record.StationCode = base_Station.StationCode;
                record.StationName = base_Station.StationName;


                var i = await _dal.Add(record);
                if (i <= 0)
                {
                    return new MessageModel<bool>()
                    {
                        msg = "保存 出入库记录 失败",
                        success = false,
                        response = false
                    };
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            return new MessageModel<bool>()
            {
                msg = "成功",
                success = true,
                response = true
            };
        }
    }
}
