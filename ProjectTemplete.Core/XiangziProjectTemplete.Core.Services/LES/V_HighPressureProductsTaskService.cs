﻿
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Services.BASE;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 高压绕线线圈 查询视图服务接口实现 
    /// </summary>	 
    public class V_HighPressureProductsTaskServices : BaseServices<V_HighPressureProductsTask>, IV_HighPressureProductsTaskServices 
    { 
        private readonly IBaseRepository<V_HighPressureProductsTask> _dal; 
        public V_HighPressureProductsTaskServices(IBaseRepository<V_HighPressureProductsTask> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
