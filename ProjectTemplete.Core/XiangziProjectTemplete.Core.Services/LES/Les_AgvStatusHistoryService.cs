﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// Agv车辆状态历史记录服务接口实现 
	/// </summary>	 
    public class Les_AgvStatusHistoryServices : BaseServices<Les_AgvStatusHistory>, ILes_AgvStatusHistoryServices 
    { 
        private readonly IBaseRepository<Les_AgvStatusHistory> _dal; 
        public Les_AgvStatusHistoryServices(IBaseRepository<Les_AgvStatusHistory> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
