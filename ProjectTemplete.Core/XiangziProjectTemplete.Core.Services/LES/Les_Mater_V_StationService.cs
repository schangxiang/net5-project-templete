﻿
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.IRepository.Base;
using System.Threading.Tasks;
using XiangziProjectTemplete.Core.Model;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 物料和站点绑定信息服务接口实现 
    /// </summary>	 
    public class Les_Mater_V_StationServices : BaseServices<Les_Mater_V_Station>, ILes_Mater_V_StationServices
    {
        private readonly IBaseRepository<Les_Mater_V_Station> _dal;
        public Les_Mater_V_StationServices(IBaseRepository<Les_Mater_V_Station> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        /// <summary>
        /// 根据站点id寻找绑定数据
        /// </summary>
        /// <param name="stationId"></param>
        /// <returns></returns>
        public async Task<MessageModel<Les_Mater_V_Station>> GetLes_Mater_V_StationByStationId(int stationId)
        {
            var queryData = await _dal.Query(x => x.StationId == stationId);
            if (queryData != null && queryData.Count > 0)
            {
                if (queryData.Count > 1)
                {
                    return MessageModel<Les_Mater_V_Station>.Fail("找到站点ID为'" + stationId + "'的绑定数据有" + queryData.Count + "条");
                }
                return MessageModel<Les_Mater_V_Station>.Success("成功", queryData[0]);
            }
            return MessageModel<Les_Mater_V_Station>.Success("成功", null);
        }
    }
}
