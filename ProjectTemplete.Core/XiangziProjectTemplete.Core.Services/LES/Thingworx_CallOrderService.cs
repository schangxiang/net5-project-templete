﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// Thingworx发送的呼叫任务服务接口实现 
	/// </summary>	 
    public class Thingworx_CallOrderServices : BaseServices<Thingworx_CallOrder>, IThingworx_CallOrderServices 
    { 
        private readonly IBaseRepository<Thingworx_CallOrder> _dal; 
        public Thingworx_CallOrderServices(IBaseRepository<Thingworx_CallOrder> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
