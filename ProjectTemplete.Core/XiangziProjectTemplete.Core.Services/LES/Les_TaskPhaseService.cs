﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// XiangziProjectTemplete任务阶段服务接口实现 
	/// </summary>	 
    public class Les_TaskPhaseServices : BaseServices<Les_TaskPhase>, ILes_TaskPhaseServices 
    { 
        private readonly IBaseRepository<Les_TaskPhase> _dal; 
        public Les_TaskPhaseServices(IBaseRepository<Les_TaskPhase> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
