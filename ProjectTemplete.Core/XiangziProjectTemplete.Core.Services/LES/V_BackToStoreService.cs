﻿
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;
using XiangziProjectTemplete.Core.Services.BASE;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 回库搬运服务接口实现 
    /// </summary>	 
    public class V_BackToStoreService : BaseServices<V_BackToStore>, IV_BackToStoreService
    {
        private readonly IBaseRepository<V_BackToStore> _dal;
        public V_BackToStoreService(IBaseRepository<V_BackToStore> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }
    }
}
