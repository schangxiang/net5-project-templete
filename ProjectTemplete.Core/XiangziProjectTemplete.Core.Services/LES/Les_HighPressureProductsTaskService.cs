﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 高压绕线线圈任务服务接口实现 
	/// </summary>	 
    public class Les_HighPressureProductsTaskServices : BaseServices<Les_HighPressureProductsTask>, ILes_HighPressureProductsTaskServices 
    { 
        private readonly IBaseRepository<Les_HighPressureProductsTask> _dal; 
        public Les_HighPressureProductsTaskServices(IBaseRepository<Les_HighPressureProductsTask> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
