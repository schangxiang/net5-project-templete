﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 呼叫任务 视图服务接口实现 
	/// </summary>	 
    public class V_CallOrderServices : BaseServices<V_CallOrder>, IV_CallOrderServices 
    { 
        private readonly IBaseRepository<V_CallOrder> _dal; 
        public V_CallOrderServices(IBaseRepository<V_CallOrder> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
