﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 呼叫记录服务接口实现 
	/// </summary>	 
    public class Les_CallRecordServices : BaseServices<Les_CallRecord>, ILes_CallRecordServices 
    { 
        private readonly IBaseRepository<Les_CallRecord> _dal; 
        public Les_CallRecordServices(IBaseRepository<Les_CallRecord> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
