﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// 出库记录服务接口实现 
	/// </summary>	 
    public class Les_OutStockRecordServices : BaseServices<Les_OutStockRecord>, ILes_OutStockRecordServices
    { 
        private readonly IBaseRepository<Les_OutStockRecord> _dal; 
        public Les_OutStockRecordServices(IBaseRepository<Les_OutStockRecord> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
