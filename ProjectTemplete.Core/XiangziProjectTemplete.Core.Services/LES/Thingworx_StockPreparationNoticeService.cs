﻿ 
using XiangziProjectTemplete.Core.IServices; 
using XiangziProjectTemplete.Core.Model.Models; 
using XiangziProjectTemplete.Core.Services.BASE; 
using XiangziProjectTemplete.Core.IRepository.Base; 
 
namespace XiangziProjectTemplete.Core.Services 
{ 
    /// <summary> 
	/// Thingworx 备料通知表服务接口实现 
	/// </summary>	 
    public class Thingworx_StockPreparationNoticeServices : BaseServices<Thingworx_StockPreparationNotice>, IThingworx_StockPreparationNoticeServices 
    { 
        private readonly IBaseRepository<Thingworx_StockPreparationNotice> _dal; 
        public Thingworx_StockPreparationNoticeServices(IBaseRepository<Thingworx_StockPreparationNotice> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
