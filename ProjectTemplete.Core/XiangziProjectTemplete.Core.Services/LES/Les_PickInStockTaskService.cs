﻿
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.Model;
using System.Threading.Tasks;
using XiangziProjectTemplete.Core.Common.HttpContextUser;
using System;
using System.Linq;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 分拣入库任务服务接口实现 
    /// </summary>	 
    public class Les_PickInStockTaskServices : BaseServices<Les_PickInStockTask>, ILes_PickInStockTaskServices
    {
        private readonly IBaseRepository<Les_PickInStockTask> _dal;
        public Les_PickInStockTaskServices(IBaseRepository<Les_PickInStockTask> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }


        /// <summary>
        /// 更新 所有本配料任务号和物料号的 是否分拣完成状态
        /// </summary>
        /// <param name="updateIsPickFinish">要更新成的分拣状态</param>
        /// <param name="les_PickInStockTask"></param>
        /// <param name="_user"></param>
        /// <param name="isExcludeThis">是否排除本项</param>
        /// <returns></returns>
        public async Task<MessageModel<bool>> UpdateIsPickFinishByBurdenWorkNo(bool updateIsPickFinish, Les_PickInStockTask les_PickInStockTask, IUser _user, bool isExcludeThis)
        {
            var msg = "将所有该任务号[" + les_PickInStockTask.BurdenWorkNo + "]的该物料全部更新为";
            if (updateIsPickFinish)
            {
                //将所有该任务号的该物料全部更新为 已分拣完成
                msg = " 已分拣完成";
            }
            else
            {
                msg = " 未分拣完成";
            }
            var samePickInStockTasks = await _dal.Query(x => x.BurdenWorkNo == les_PickInStockTask.BurdenWorkNo
            && x.MaterialCodeItemId == les_PickInStockTask.MaterialCodeItemId);
            if (isExcludeThis)
            {
                if (samePickInStockTasks != null && samePickInStockTasks.Count > 0)
                {
                    samePickInStockTasks = samePickInStockTasks.Where(x => x.Id != les_PickInStockTask.Id).ToList();
                }
            }
            foreach (var x in samePickInStockTasks)
            {
                x.ModifyBy = _user.Name;
                x.IsPickFinish = updateIsPickFinish;
                x.ModifyTime = DateTime.Now;
                x.OperationRemark = msg;
                await _dal.Update(x);
            }
            
            return MessageModel<bool>.Success("成功");
        }


        /// <summary>
        /// 更新 所有本配料任务号和物料号的 的备注
        /// </summary>
        /// <param name="remark">备注</param>
        /// <param name="les_PickInStockTask"></param>
        /// <param name="_user"></param>
        /// <param name="isExcludeThis">是否排除本项</param>
        /// <returns></returns>
        public async Task<MessageModel<bool>> UpdateRemarkByBurdenWorkNo(string remark, Les_PickInStockTask les_PickInStockTask, IUser _user, bool isExcludeThis)
        {
            var msg = "将所有该任务号[" + les_PickInStockTask.BurdenWorkNo + "]的备注更改为" + remark;

            var samePickInStockTasks = await _dal.Query(x => x.BurdenWorkNo == les_PickInStockTask.BurdenWorkNo
            && x.MaterialCodeItemId == les_PickInStockTask.MaterialCodeItemId);
            if (isExcludeThis)
            {
                if (samePickInStockTasks != null && samePickInStockTasks.Count > 0)
                {
                    samePickInStockTasks = samePickInStockTasks.Where(x => x.Id != les_PickInStockTask.Id).ToList();
                }
            }
            foreach (var x in samePickInStockTasks)
            {
                x.ModifyBy = _user.Name;
                x.Remark = remark;
                x.ModifyTime = DateTime.Now;
                x.OperationRemark = msg;
                await _dal.Update(x);
            }
            return MessageModel<bool>.Success("成功");
        }

    }
}
