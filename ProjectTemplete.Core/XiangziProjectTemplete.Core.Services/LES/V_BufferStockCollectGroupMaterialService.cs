﻿
using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Views;
using XiangziProjectTemplete.Core.Services.BASE;

namespace XiangziProjectTemplete.Core.Services
{
    /// <summary> 
    /// 分组查询立库缓存区的物料服务接口实现 
    /// </summary>	 
    public class V_BufferStockCollectGroupMaterialServices : BaseServices<V_BufferStockCollectGroupMaterial>, IV_BufferStockCollectGroupMaterialServices 
    { 
        private readonly IBaseRepository<V_BufferStockCollectGroupMaterial> _dal; 
        public V_BufferStockCollectGroupMaterialServices(IBaseRepository<V_BufferStockCollectGroupMaterial> dal) 
        { 
            this._dal = dal; 
            base.BaseDal = dal; 
        } 
    } 
} 
