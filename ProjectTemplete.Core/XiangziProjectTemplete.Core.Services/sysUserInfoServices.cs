using XiangziProjectTemplete.Core.IRepository.Base;
using XiangziProjectTemplete.Core.IServices;
using XiangziProjectTemplete.Core.Model.Models;
using XiangziProjectTemplete.Core.Services.BASE;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.FrameWork.Services
{
    /// <summary>
    /// sysUserInfoServices
    /// </summary>	
    public class SysUserInfoServices : BaseServices<sysUserInfo>, ISysUserInfoServices
    {

        private readonly IBaseRepository<sysUserInfo> _dal;
        private readonly IBaseRepository<UserRole> _userRoleRepository;
        private readonly IBaseRepository<Role> _roleRepository;
        public SysUserInfoServices(IBaseRepository<sysUserInfo> dal, IBaseRepository<UserRole> userRoleRepository, IBaseRepository<Role> roleRepository)
        {
            this._dal = dal;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            base.BaseDal = dal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="loginPwd"></param>
        /// <returns></returns>
        public async Task<sysUserInfo> SaveUserInfo(string loginName, string loginPwd)
        {
            sysUserInfo sysUserInfo = new sysUserInfo(loginName, loginPwd);
            sysUserInfo model = new sysUserInfo();
            var userList = await base.Query(a => a.uLoginName == sysUserInfo.uLoginName && a.uLoginPWD == sysUserInfo.uLoginPWD);
            if (userList.Count > 0)
            {
                model = userList.FirstOrDefault();
            }
            else
            {
                var id = await base.Add(sysUserInfo);
                model = await base.QueryById(id);
            }

            return model;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="loginPwd"></param>
        /// <returns></returns>
        public async Task<string> GetUserRoleNameStr(string loginName, string loginPwd)
        {
            string roleName = "";
            var user = (await base.Query(a => a.uLoginName == loginName && a.uLoginPWD == loginPwd)).FirstOrDefault();
            var roleList = await _roleRepository.Query(a => a.IsDeleted == false);
            if (user != null)
            {
                var userRoles = await _userRoleRepository.Query(ur => ur.UserId == user.uID);
                if (userRoles.Count > 0)
                {
                    var arr = userRoles.Select(ur => ur.RoleId.ObjToString()).ToList();
                    var roles = roleList.Where(d => arr.Contains(d.Id.ObjToString()));

                    roleName = string.Join(',', roles.Select(r => r.Name).ToArray());
                }
            }
            return roleName;
        }


        /// <summary>
        /// 是否存在同登录名或昵称的用户
        /// </summary>
        /// <param name="uLoginName">登录名</param>
        /// <param name="uRealName">昵称</param>
        /// <param name="uID">是否需要过滤用户ID</param>
        /// <returns></returns>
        public async Task<bool> IsExistSameUserName(string uLoginName, string uRealName, int uID = 0)
        {
            var result = false;
            List<sysUserInfo> dbUsers = null;
            if (uID != 0)
            {
                dbUsers = await _dal.Query(x => (x.uLoginName == uLoginName || x.uRealName == uRealName) && x.uID != uID);
            }
            else
            {
                dbUsers = await _dal.Query(x => (x.uLoginName == uLoginName || x.uRealName == uRealName));
            }
            if (dbUsers != null && dbUsers.Count > 0)
            {
                result = true;
            }
            return result;
        }
    }
}

//----------sysUserInfo结束----------
