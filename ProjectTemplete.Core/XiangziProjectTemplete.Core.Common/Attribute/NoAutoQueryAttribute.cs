﻿using System;

namespace XiangziProjectTemplete.Core.Common
{
    /// <summary>
    /// 不自动查询特性
    /// 用于不需要自动分解查询的字段
    /// </summary>
    public class NoAutoQueryAttribute : Attribute
    {
    }
}
