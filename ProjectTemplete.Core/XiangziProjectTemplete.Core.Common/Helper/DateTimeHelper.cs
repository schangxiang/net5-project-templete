﻿using System;

namespace XiangziProjectTemplete.Core.Common.Helper
{
    public class DateTimeHelper
    {
        /// <summary>
        /// 转换日期格式为字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string ConvertToString(DateTime? dt)
        {
            if (dt == null)
            {
                return string.Empty;
            }
            return Convert.ToDateTime(dt).ToString("yyyy-MM-dd HH:mm:ss");
        }

        /// <summary>
        /// 格林威治时间戳转换成DateTime，为NUll表示转换失败 
        /// 格式:  日/月/年 时:分:秒
        /// </summary>
        /// <param name="greenwichTimeStampStr"></param>
        /// <returns></returns>
        public static DateTime? ForamtGreenwichTimeStampToDateTime(string greenwichTimeStampStr)
        {
            var yyyy = "";
            var MM = "";
            var dd = "";
            var hh = "";
            var mm = "";
            var ss = "";
            var newDate = "";
            var nowDate = DateTime.Now;
            try
            {

                if (!string.IsNullOrEmpty(greenwichTimeStampStr))
                {
                    var dates = greenwichTimeStampStr.Split(' ');
                    if (dates.Length != 2)
                    {
                        return null;
                    }
                    var yyMMdd = dates[0];
                    var yyMMdds = yyMMdd.Split('/');
                    if (yyMMdds.Length != 3)
                        return null;

                    yyyy = yyMMdds[2];
                    MM = yyMMdds[1];
                    dd = yyMMdds[0];

                    var hhmmss = dates[1];
                    var hhmmss_s = hhmmss.Split(':');
                    if (hhmmss_s.Length != 3)
                        return null;

                    hh = hhmmss_s[0];
                    mm = hhmmss_s[1];
                    ss = hhmmss_s[2];


                    newDate = yyyy + "-" + MM.PadLeft(2, '0') + "-" + dd.PadLeft(2, '0')
                        + " " +
                        hh.PadLeft(2, '0') + ":" + mm.PadLeft(2, '0') + ":" + ss.PadLeft(2, '0');
                    return Convert.ToDateTime(newDate);
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 获取日期的开始时刻
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime GetDayStart(DateTime dt)
        {
            return Convert.ToDateTime(dt.ToString("yyyy-MM-dd 00:00:00"));
        }

        /// <summary>
        /// 获取日期的最后时刻
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime GetDayEnd(DateTime dt)
        {
            return Convert.ToDateTime(dt.ToString("yyyy-MM-dd 23:59:59"));
        }


        /// <summary>
        /// 计算两个时间的时间差，返回秒数
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static int GetTimeDiffer(DateTime? startTime, DateTime? endTime, ref string msg)
        {
            if (startTime == null || endTime == null)
            {
                return 0;
            }
            TimeSpan ts = ((DateTime)endTime - (DateTime)startTime);
            msg = "";
            if (ts.Days != 0)
            {
                msg += ts.Days + "天";
            }
            if (ts.Hours != 0)
            {
                msg += ts.Hours + "小时";
            }
            if (ts.Minutes != 0)
            {
                msg += ts.Minutes + "分钟";
            }
            if (ts.Seconds != 0)
            {
                msg += ts.Seconds + "秒";
            }
            return (int)ts.TotalSeconds;
        }
    }
}
