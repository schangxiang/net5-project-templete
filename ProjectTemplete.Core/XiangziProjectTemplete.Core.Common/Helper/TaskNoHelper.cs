﻿using System;
using System.Threading;

namespace XiangziProjectTemplete.Core.Common.Helper
{
    /// <summary>
    /// 任务号帮助类
    /// </summary>
    public class TaskNoHelper
    {
        private readonly static object obj = new object();

        /// <summary>
        /// 生成唯一的XiangziProjectTemplete任务号
        /// </summary>
        /// <param name="categroy">类别</param>
        /// <returns></returns>
        public static string GenerateLesTaskNo(string categroy)
        {
            var newTaskNo = "";
            lock (obj)
            {
                Thread.Sleep(1000);//休眠1秒
                newTaskNo = categroy + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            }
            return newTaskNo;
        }
    }
}
