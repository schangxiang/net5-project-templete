﻿using System;

namespace XiangziProjectTemplete.Core.Common.Helper
{
    /// <summary>
    /// 业务帮助类
    /// </summary>
    public class BusinessHelper
    {
        /// <summary>
        /// 随机生成1-10000的PLC任务号
        /// </summary>
        /// <returns></returns>
        public static string CreatePlcTaskId()
        {
            int iSeed = 10000;
            return new Random(Guid.NewGuid().GetHashCode()).Next(1, iSeed).ToString();
        }
        /// <summary>
        /// 创建输送线任务号，随机生成(1, 3000)的PLC任务号
        /// </summary>
        /// <returns></returns>
        public static string CreatePlcTaskIdForConveyorTask()
        {
            int iSeed = 3000;
            return new Random(Guid.NewGuid().GetHashCode()).Next(1, iSeed).ToString();
        }
        /// <summary>
        /// 创建堆垛机任务号，随机生成 （3001, 10000）的PLC任务号
        /// </summary>
        /// <returns></returns>
        public static string CreatePlcTaskIdForSrmTask()
        {
            int iSeed = 10000;
            return new Random(Guid.NewGuid().GetHashCode()).Next(3001, iSeed).ToString();
        }
    }
}
