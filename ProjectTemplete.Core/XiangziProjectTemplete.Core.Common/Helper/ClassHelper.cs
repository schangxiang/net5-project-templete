﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace XiangziProjectTemplete.Core.Common.Helper
{
    /// <summary>
    /// 类帮助
    /// </summary>
    public class ClassHelper
    {
        /// <summary>
        /// 实体互转
        /// </summary>
        /// <typeparam name="T">新转换的实体</typeparam>
        /// <typeparam name="S">要转换的实体</typeparam>
        /// <param name="s"></param>
        /// <returns></returns>
        public static T RotationMapping<T, S>(S s)
        {
            T target = Activator.CreateInstance<T>();
            var originalObj = s.GetType();
            var targetObj = typeof(T);
            foreach (PropertyInfo original in originalObj.GetProperties())
            {
                foreach (PropertyInfo t in targetObj.GetProperties())
                {
                    if (t.Name == original.Name)
                    {
                        t.SetValue(target, original.GetValue(s, null), null);
                    }
                }
            }
            return target;
        }

        /// <summary>
        /// 获取属性对象列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="proName"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public static PropertyInfo[] GetPropertyInfoList<T>(T t)
        {
            var pros = typeof(T).GetProperties();
            return pros;
        }

        /// <summary>
        /// 获取属性对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="proName"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public static PropertyInfo GetPropertyInfo<T>(T t, string proName, out string errMsg)
        {
            errMsg = "";
            var pro = typeof(T).GetProperty(proName);
            if (pro == null)
            {
                errMsg = "属性名'" + proName + "'不存在类'" + typeof(T).Name + "'中";
                return null;
            }
            return pro;
        }

        /// <summary>
        /// 获取属性的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="proName"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public static string GetPropertyValue<T>(T t, string proName, out string errMsg)
        {
            var pro = GetPropertyInfo<T>(t, proName, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return string.Empty;
            }
            var pro_value = pro.GetValue(t, null);
            var str = pro_value == null ? string.Empty : Convert.ToString(pro_value);
            return str;
        }


        /// <summary>
        /// 通过属性对象获取属性的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="pro"></param>
        /// <returns></returns>
        public static string GetPropertyValueByObject<T>(T t, PropertyInfo pro)
        {
            var pro_value = pro.GetValue(t, null);
            var str = pro_value == null ? string.Empty : Convert.ToString(pro_value);
            return str;
        }

        /// <summary>
        /// 通过属性对象获取属性的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="pro"></param>
        /// <returns></returns>
        public static object GetPropertyValue<T>(T t, PropertyInfo pro)
        {
            var pro_value = pro.GetValue(t, null);
            return pro_value;
        }

        /// <summary>
        /// 获取特性【高级查询范围查询特性】
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="pro"></param>
        /// <returns></returns>
        public static object[] GetHighSearchRangeAttributeByPro(PropertyInfo pro)
        {
            object[] Attributes = pro.GetCustomAttributes(typeof(HighSearchRangeAttribute), false);
            return Attributes;
        }

        /// <summary>
        /// 获取特性【不自动查询特性】
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="pro"></param>
        /// <returns></returns>
        public static bool IsExistNoAutoQueryAttribute(PropertyInfo pro)
        {
            object[] attributes = pro.GetCustomAttributes(typeof(NoAutoQueryAttribute), false);
            if (attributes.Length > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 获取属性的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="proName"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public static List<string> GetPropertyValueForList<T>(T t, string proName, out string errMsg)
        {
            var pro = GetPropertyInfo<T>(t, proName, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return null;
            }
            var pro_value = pro.GetValue(t, null);
            var list = pro_value == null ? null : (List<string>)pro_value;
            return list;
        }
    }
}
