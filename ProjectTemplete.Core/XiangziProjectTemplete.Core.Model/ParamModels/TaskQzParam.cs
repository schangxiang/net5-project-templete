﻿using XiangziProjectTemplete.Core.Common;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 查询调度任务类 
    /// </summary> 
    public class TaskQzParam : PageParam
    {
        public string JobGroup { get; set; }

        public string JobGroup_FilterMode { get; set; }

        public string Name { get; set; }

        public string Name_FilterMode { get; set; }

    }
}
