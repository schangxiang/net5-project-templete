﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询物料管理表类 
    /// </summary> 
    public class Base_MaterialParam : PageParam 
    { 
                /// <summary>
        /// 物料号
        /// </summary>
        public string MaterialCode { get; set; }

        /// <summary>
        /// 物料号-查询过滤模式 
        /// </summary>
        public string MaterialCode_FilterMode { get; set; }

        /// <summary>
        /// 货物名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 货物名称-查询过滤模式 
        /// </summary>
        public string MaterialName_FilterMode { get; set; }

        /// <summary>
        /// 型号
        /// </summary>
        public string MaterialType { get; set; }

        /// <summary>
        /// 型号-查询过滤模式 
        /// </summary>
        public string MaterialType_FilterMode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 备注-查询过滤模式 
        /// </summary>
        public string Remark_FilterMode { get; set; }

        /// <summary>
        /// 图纸号
        /// </summary>
        public string DrawingNumber { get; set; }

        /// <summary>
        /// 图纸号-查询过滤模式 
        /// </summary>
        public string DrawingNumber_FilterMode { get; set; }

        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询过滤模式 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询过滤模式 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询过滤模式 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 修改者ID
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// 修改者ID-查询过滤模式 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// 修改者-查询过滤模式 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// 是否逻辑删除-查询过滤模式 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }

 
    } 
} 
