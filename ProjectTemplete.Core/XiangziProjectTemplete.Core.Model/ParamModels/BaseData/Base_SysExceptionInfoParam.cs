﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询异常日志表类 
    /// </summary> 
    public class Base_SysExceptionInfoParam : PageParam 
    { 
                /// <summary>
        /// 模块
        /// </summary>
        public string module { get; set; }

        /// <summary>
        /// 模块-查询关系运算符 
        /// </summary>
        public string module_FilterMode { get; set; }

        /// <summary>
        /// 级别
        /// </summary>
        public string exceptionLevel { get; set; }

        /// <summary>
        /// 级别-查询关系运算符 
        /// </summary>
        public string exceptionLevel_FilterMode { get; set; }

        /// <summary>
        /// 错误来源
        /// </summary>
        public string exceptionSource { get; set; }

        /// <summary>
        /// 错误来源-查询关系运算符 
        /// </summary>
        public string exceptionSource_FilterMode { get; set; }

        /// <summary>
        /// 错误方法
        /// </summary>
        public string exceptionFun { get; set; }

        /// <summary>
        /// 错误方法-查询关系运算符 
        /// </summary>
        public string exceptionFun_FilterMode { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        public string sourceData { get; set; }

        /// <summary>
        /// 参数-查询关系运算符 
        /// </summary>
        public string sourceData_FilterMode { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public string exceptionMsg { get; set; }

        /// <summary>
        /// 异常信息-查询关系运算符 
        /// </summary>
        public string exceptionMsg_FilterMode { get; set; }

        /// <summary>
        /// 异常堆栈
        /// </summary>
        public string exceptionData { get; set; }

        /// <summary>
        /// 异常堆栈-查询关系运算符 
        /// </summary>
        public string exceptionData_FilterMode { get; set; }

        /// <summary>
        /// 主机名
        /// </summary>
        public string host { get; set; }

        /// <summary>
        /// 主机名-查询关系运算符 
        /// </summary>
        public string host_FilterMode { get; set; }

        /// <summary>
        /// 关键字1
        /// </summary>
        public string key1 { get; set; }

        /// <summary>
        /// 关键字1-查询关系运算符 
        /// </summary>
        public string key1_FilterMode { get; set; }

        /// <summary>
        /// 关键字2
        /// </summary>
        public string key2 { get; set; }

        /// <summary>
        /// 关键字2-查询关系运算符 
        /// </summary>
        public string key2_FilterMode { get; set; }

        /// <summary>
        /// 关键字3
        /// </summary>
        public string key3 { get; set; }

        /// <summary>
        /// 关键字3-查询关系运算符 
        /// </summary>
        public string key3_FilterMode { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string creator { get; set; }

        /// <summary>
        /// 创建人-查询关系运算符 
        /// </summary>
        public string creator_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> createTime { get; set; }


 
    } 
} 
