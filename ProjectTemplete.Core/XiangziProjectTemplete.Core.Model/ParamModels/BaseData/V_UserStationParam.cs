﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询用户站点绑定类 
    /// </summary> 
    public class V_UserStationParam : PageParam 
    { 
                /// <summary>
        /// 
        /// </summary>
        public string uRealName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string uRealName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? uID { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string uID_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string uLoginName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string uLoginName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BindStation { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string BindStation_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string Remark_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BindStationName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string BindStationName_FilterMode { get; set; }

 
    } 
} 
