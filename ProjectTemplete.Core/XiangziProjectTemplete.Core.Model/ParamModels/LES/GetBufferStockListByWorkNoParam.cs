﻿using XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete;

namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 根据物料号 获取库位列表的查询参数 
    /// </summary> 
    public class GetBufferStockListByWorkNoParam
    {
        /// <summary>
        /// 库存区域
        /// </summary>
        public StoreAreaEnum StoreArea { get; set; }

        /// <summary>
        /// 配料任务号
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 物料的码表ID
        /// </summary>
        public string MaterialCodeItemId { get; set; }

    }
}
