﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询AGV报警类 
    /// </summary> 
    public class Les_AgvWarningParam : PageParam 
    { 
                /// <summary>
        /// 报警时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> WarningTime { get; set; }


        /// <summary>
        /// 车辆名称
        /// </summary>
        public string AgvName { get; set; }

        /// <summary>
        /// 车辆名称-查询关系运算符 
        /// </summary>
        public string AgvName_FilterMode { get; set; }

        /// <summary>
        /// 车辆描述
        /// </summary>
        public string AgvNameDesc { get; set; }

        /// <summary>
        /// 车辆描述-查询关系运算符 
        /// </summary>
        public string AgvNameDesc_FilterMode { get; set; }

        /// <summary>
        /// 报警位置
        /// </summary>
        public string WarningLocation { get; set; }

        /// <summary>
        /// 报警位置-查询关系运算符 
        /// </summary>
        public string WarningLocation_FilterMode { get; set; }

        /// <summary>
        /// 报警内容
        /// </summary>
        public string WarningContent { get; set; }

        /// <summary>
        /// 报警内容-查询关系运算符 
        /// </summary>
        public string WarningContent_FilterMode { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 状态-查询关系运算符 
        /// </summary>
        public string Status_FilterMode { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// 状态-查询关系运算符 
        /// </summary>
        public string StatusName_FilterMode { get; set; }

        /// <summary>
        /// 是否工人已知道
        /// </summary>
        public int? IsKnow { get; set; }

        /// <summary>
        /// 是否工人已知道-查询关系运算符 
        /// </summary>
        public string IsKnow_FilterMode { get; set; }

        /// <summary>
        /// 关闭报警说明
        /// </summary>
        public string CloseContent { get; set; }

        /// <summary>
        /// 关闭报警说明-查询关系运算符 
        /// </summary>
        public string CloseContent_FilterMode { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> FinishTime { get; set; }


        /// <summary>
        /// 持续时间
        /// </summary>
        public string DurationTime { get; set; }

        /// <summary>
        /// 持续时间-查询关系运算符 
        /// </summary>
        public string DurationTime_FilterMode { get; set; }

        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 修改者ID
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// 修改者ID-查询关系运算符 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// 修改者-查询关系运算符 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// 是否逻辑删除-查询关系运算符 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }

 
    } 
} 
