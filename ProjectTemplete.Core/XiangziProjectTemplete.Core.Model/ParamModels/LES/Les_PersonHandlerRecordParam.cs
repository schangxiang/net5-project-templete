﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询人工处理数据记录表类 
    /// </summary> 
    public class Les_PersonHandlerRecordParam : PageParam 
    { 
                /// <summary>
        /// 关键字1
        /// </summary>
        public string Key1 { get; set; }

        /// <summary>
        /// 关键字1-查询关系运算符 
        /// </summary>
        public string Key1_FilterMode { get; set; }

        /// <summary>
        /// 关键字2
        /// </summary>
        public string Key2 { get; set; }

        /// <summary>
        /// 关键字2-查询关系运算符 
        /// </summary>
        public string Key2_FilterMode { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public string HandlerType { get; set; }

        /// <summary>
        /// 类型-查询关系运算符 
        /// </summary>
        public string HandlerType_FilterMode { get; set; }

        /// <summary>
        /// 人工处理说明
        /// </summary>
        public string ManualHandlerRemark { get; set; }

        /// <summary>
        /// 人工处理说明-查询关系运算符 
        /// </summary>
        public string ManualHandlerRemark_FilterMode { get; set; }

        /// <summary>
        /// 强制下线操作人员
        /// </summary>
        public string ManualHandlerUser { get; set; }

        /// <summary>
        /// 强制下线操作人员-查询关系运算符 
        /// </summary>
        public string ManualHandlerUser_FilterMode { get; set; }

        /// <summary>
        /// 人工处理时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ManualHandlerTime { get; set; }


        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 修改者ID
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// 修改者ID-查询关系运算符 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// 修改者-查询关系运算符 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// 是否逻辑删除-查询关系运算符 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }

 
    } 
} 
