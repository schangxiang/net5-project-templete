﻿using XiangziProjectTemplete.Core.Common;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 查询Thingworx呼叫任务类 
    /// </summary> 
    public class Thingworx_CallOrderParam : PageParam
    {
        /// <summary>
        /// 呼叫模式
        /// </summary>
        public int? LesTaskMode { get; set; }

        /// <summary>
        /// 呼叫模式-查询关系运算符 
        /// </summary>
        public string LesTaskMode_FilterMode { get; set; }

        /// <summary>
        /// 呼叫模式
        /// </summary>
        public string LesTaskModeName { get; set; }

        /// <summary>
        /// 呼叫模式-查询关系运算符 
        /// </summary>
        public string LesTaskModeName_FilterMode { get; set; }

        /// <summary>
        /// 工序
        /// </summary>
        public string Procedure { get; set; }

        /// <summary>
        /// 工序-查询关系运算符 
        /// </summary>
        public string Procedure_FilterMode { get; set; }

        /// <summary>
        /// 工位号
        /// </summary>
        public string WorkStationNo { get; set; }

        /// <summary>
        /// 工位号-查询关系运算符 
        /// </summary>
        public string WorkStationNo_FilterMode { get; set; }

        /// <summary>
        /// 点位
        /// </summary>
        public string StationCode { get; set; }

        /// <summary>
        /// 点位-查询关系运算符 
        /// </summary>
        public string StationCode_FilterMode { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 任务号-查询关系运算符 
        /// </summary>
        public string BurdenWorkNo_FilterMode { get; set; }

        /// <summary>
        /// 物料
        /// </summary>
        public string Material { get; set; }

        /// <summary>
        /// 物料-查询关系运算符 
        /// </summary>
        public string Material_FilterMode { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 状态-查询关系运算符 
        /// </summary>
        public string Status_FilterMode { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// 状态-查询关系运算符 
        /// </summary>
        public string StatusName_FilterMode { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务ID
        /// </summary>
        public string LesTaskId { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务ID-查询关系运算符 
        /// </summary>
        public string LesTaskId_FilterMode { get; set; }

        /// <summary>
        /// 呼叫任务号
        /// </summary>
        public string CallOrderTaskNo { get; set; }

        /// <summary>
        /// 呼叫任务号-查询关系运算符 
        /// </summary>
        public string CallOrderTaskNo_FilterMode { get; set; }

        /// <summary>
        /// 下发时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskIssueTime { get; set; }


        /// <summary>
        /// 完成时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskFinishTime { get; set; }


        /// <summary>
        /// 是否通知成功
        /// </summary>
        public string NoticeResult { get; set; }

        /// <summary>
        /// 是否通知成功-查询关系运算符 
        /// </summary>
        public string NoticeResult_FilterMode { get; set; }

        /// <summary>
        /// 通知返回消息
        /// </summary>
        public string ThingworxReturn { get; set; }

        /// <summary>
        /// 通知返回消息-查询关系运算符 
        /// </summary>
        public string ThingworxReturn_FilterMode { get; set; }

        /// <summary>
        /// 通知时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> NoticeTime { get; set; }


        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 修改者ID
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// 修改者ID-查询关系运算符 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// 修改者-查询关系运算符 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// 是否逻辑删除-查询关系运算符 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }


    }
}
