﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询请求日志表类 
    /// </summary> 
    public class HttpRequestRecordParam : PageParam 
    { 
                /// <summary>
        /// 关键词1
        /// </summary>
        public string key1 { get; set; }

        /// <summary>
        /// 关键词1-查询关系运算符 
        /// </summary>
        public string key1_FilterMode { get; set; }

        /// <summary>
        /// 关键词2
        /// </summary>
        public string key2 { get; set; }

        /// <summary>
        /// 关键词2-查询关系运算符 
        /// </summary>
        public string key2_FilterMode { get; set; }

        /// <summary>
        /// 方向( 1  接收 2  推送)
        /// </summary>
        public int? direction { get; set; }

        /// <summary>
        /// 方向( 1  接收 2  推送)-查询关系运算符 
        /// </summary>
        public string direction_FilterMode { get; set; }

        /// <summary>
        /// 方法名
        /// </summary>
        public string fullFun { get; set; }

        /// <summary>
        /// 方法名-查询关系运算符 
        /// </summary>
        public string fullFun_FilterMode { get; set; }

        /// <summary>
        /// 请求host
        /// </summary>
        public string host { get; set; }

        /// <summary>
        /// 请求host-查询关系运算符 
        /// </summary>
        public string host_FilterMode { get; set; }

        /// <summary>
        /// 请求地址
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 请求地址-查询关系运算符 
        /// </summary>
        public string url_FilterMode { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string param { get; set; }

        /// <summary>
        /// 请求参数-查询关系运算符 
        /// </summary>
        public string param_FilterMode { get; set; }

        /// <summary>
        /// 请求返回结果
        /// </summary>
        public string retResult { get; set; }

        /// <summary>
        /// 请求返回结果-查询关系运算符 
        /// </summary>
        public string retResult_FilterMode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// 备注-查询关系运算符 
        /// </summary>
        public string remark_FilterMode { get; set; }

        /// <summary>
        /// 发生的主机host
        /// </summary>
        public string happenHost { get; set; }

        /// <summary>
        /// 发生的主机host-查询关系运算符 
        /// </summary>
        public string happenHost_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


 
    } 
} 
