﻿using XiangziProjectTemplete.Core.Common;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 查询呼叫任务 视图类 
    /// </summary> 
    public class V_CallOrderParam : PageParam
    {

        public string BurdenWorkNo { get; set; }

        public string BurdenWorkNo_FilterMode { get; set; }

        public string BurdenWorkNoActual { get; set; }

        public string BurdenWorkNoActual_FilterMode { get; set; }

        /// <summary>
        /// 呼叫模式
        /// </summary>
        public string LesTaskMode { get; set; }

        /// <summary>
        /// 呼叫模式-查询关系运算符
        /// </summary>
        public string LesTaskMode_FilterMode { get; set; }

        /// <summary>
        /// 数据来源
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// 数据来源-查询关系运算符
        /// </summary>
        public string DataSource_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AllowProcedureName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AllowProcedureName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AllowLineName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AllowLineName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MaterialCode { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string MaterialCode_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string MaterialName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StationCode { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string StationCode_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StationName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string StationName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PlaceTypeCode { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string PlaceTypeCode_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PlaceTypeName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string PlaceTypeName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string TaskNo_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AgvTaskNo { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AgvTaskNo_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AllowProcedure { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AllowProcedure_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AllowLine { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AllowLine_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? StationId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string StationId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MaterialCodeItemId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string MaterialCodeItemId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CallOrderRemark { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CallOrderRemark_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ProjectName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string OrderNo_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string Status_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskIssueTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskFinishTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }


    }
}
