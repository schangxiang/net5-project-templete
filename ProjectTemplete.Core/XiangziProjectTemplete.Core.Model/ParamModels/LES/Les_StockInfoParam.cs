﻿using XiangziProjectTemplete.Core.Common;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 查询库存信息类 
    /// </summary> 
    public class Les_StockInfoParam : PageParam
    {

        /// <summary>
        /// 请求标记
        /// BufferStockMgr:查询立库区（包括 立库缓存区和分拣区）
        /// </summary>
        [NoAutoQuery]
        public string RequestFlag { get; set; }


        /// <summary>
        /// 所属工序
        /// </summary>
        public string AllowProcedure { get; set; }

        /// <summary>
        /// 所属工序-查询过滤模式 
        /// </summary>
        public string AllowProcedure_FilterMode { get; set; }

        /// <summary>
        /// 配送任务号
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 配送任务号-查询过滤模式 
        /// </summary>
        public string BurdenWorkNo_FilterMode { get; set; }

        /// <summary>
        /// 货物类型
        /// </summary>
        public int? CargoType { get; set; }

        /// <summary>
        /// 货物类型-查询过滤模式 
        /// </summary>
        public string CargoType_FilterMode { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 项目名称-查询过滤模式 
        /// </summary>
        public string ProjectName_FilterMode { get; set; }



        /// <summary>
        /// 小车名称
        /// </summary>
        public string AgvName { get; set; }

        /// <summary>
        /// 小车名称-查询过滤模式 
        /// </summary>
        public string AgvName_FilterMode { get; set; }

        /// <summary>
        /// 跟踪号
        /// </summary>
        public string TrackingNo { get; set; }

        /// <summary>
        /// 跟踪号-查询过滤模式 
        /// </summary>
        public string TrackingNo_FilterMode { get; set; }

        /// <summary>
        /// 托盘号
        /// </summary>
        public string SalverNo { get; set; }

        /// <summary>
        /// 托盘号-查询过滤模式 
        /// </summary>
        public string SalverNo_FilterMode { get; set; }

        /// <summary>
        /// 物料号
        /// </summary>
        public string CargoCode { get; set; }

        /// <summary>
        /// 物料号-查询过滤模式 
        /// </summary>
        public string CargoCode_FilterMode { get; set; }

        /// <summary>
        /// 货物名称
        /// </summary>
        public string CargoName { get; set; }

        /// <summary>
        /// 货物名称-查询过滤模式 
        /// </summary>
        public string CargoName_FilterMode { get; set; }

        /// <summary>
        /// 货物数量
        /// </summary>
        public int? CargoNum { get; set; }

        /// <summary>
        /// 货物数量-查询过滤模式 
        /// </summary>
        public string CargoNum_FilterMode { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        public string CargoWeight { get; set; }

        /// <summary>
        /// 重量-查询过滤模式 
        /// </summary>
        public string CargoWeight_FilterMode { get; set; }

        /// <summary>
        /// 型号
        /// </summary>
        public string CargoModel { get; set; }

        /// <summary>
        /// 型号-查询过滤模式 
        /// </summary>
        public string CargoModel_FilterMode { get; set; }

        /// <summary>
        /// 图纸号
        /// </summary>
        public string DrawingNo { get; set; }

        /// <summary>
        /// 图纸号-查询过滤模式 
        /// </summary>
        public string DrawingNo_FilterMode { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ValidityDate { get; set; }


        /// <summary>
        /// 入库时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> InStockDate { get; set; }


        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询过滤模式 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询过滤模式 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询过滤模式 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 修改者ID
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// 修改者ID-查询过滤模式 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// 修改者-查询过滤模式 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// 是否逻辑删除-查询过滤模式 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }


    }
}
