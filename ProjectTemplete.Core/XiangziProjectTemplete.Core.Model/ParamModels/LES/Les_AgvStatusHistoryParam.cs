﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询Agv车辆状态历史记录类 
    /// </summary> 
    public class Les_AgvStatusHistoryParam : PageParam 
    { 
                /// <summary>
        /// 车辆名称
        /// </summary>
        public string AgvName { get; set; }

        /// <summary>
        /// 车辆名称-查询关系运算符 
        /// </summary>
        public string AgvName_FilterMode { get; set; }

        /// <summary>
        /// 车辆描述
        /// </summary>
        public string AgvNameDesc { get; set; }

        /// <summary>
        /// 车辆描述-查询关系运算符 
        /// </summary>
        public string AgvNameDesc_FilterMode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 备注-查询关系运算符 
        /// </summary>
        public string Remark_FilterMode { get; set; }

        /// <summary>
        /// 电量
        /// </summary>
        public int? EnergyLevel { get; set; }

        /// <summary>
        /// 电量-查询关系运算符 
        /// </summary>
        public string EnergyLevel_FilterMode { get; set; }

        /// <summary>
        /// 当前位置
        /// </summary>
        public string CurrentPosition { get; set; }

        /// <summary>
        /// 当前位置-查询关系运算符 
        /// </summary>
        public string CurrentPosition_FilterMode { get; set; }

        /// <summary>
        /// X坐标
        /// </summary>
        public string XCoordinate { get; set; }

        /// <summary>
        /// X坐标-查询关系运算符 
        /// </summary>
        public string XCoordinate_FilterMode { get; set; }

        /// <summary>
        /// Y坐标
        /// </summary>
        public string YCoordinate { get; set; }

        /// <summary>
        /// Y坐标-查询关系运算符 
        /// </summary>
        public string YCoordinate_FilterMode { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 状态-查询关系运算符 
        /// </summary>
        public string State_FilterMode { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string StateName { get; set; }

        /// <summary>
        /// 状态-查询关系运算符 
        /// </summary>
        public string StateName_FilterMode { get; set; }

        /// <summary>
        /// 当前执行的任务
        /// </summary>
        public string TransportOrder { get; set; }

        /// <summary>
        /// 当前执行的任务-查询关系运算符 
        /// </summary>
        public string TransportOrder_FilterMode { get; set; }

        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


 
    } 
} 
