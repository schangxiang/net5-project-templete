﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询高压绕线线圈任务类 
    /// </summary> 
    public class Les_HighPressureProductsTaskParam : PageParam 
    { 
                /// <summary>
        /// XiangziProjectTemplete任务号
        /// </summary>
        public string LesTaskNo { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务号-查询关系运算符 
        /// </summary>
        public string LesTaskNo_FilterMode { get; set; }

        /// <summary>
        /// 生产任务号
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 生产任务号-查询关系运算符 
        /// </summary>
        public string BurdenWorkNo_FilterMode { get; set; }

        /// <summary>
        /// AGV任务1
        /// </summary>
        public string AgvTask1 { get; set; }

        /// <summary>
        /// AGV任务1-查询关系运算符 
        /// </summary>
        public string AgvTask1_FilterMode { get; set; }

        /// <summary>
        /// AGV任务2
        /// </summary>
        public string AgvTask2 { get; set; }

        /// <summary>
        /// AGV任务2-查询关系运算符 
        /// </summary>
        public string AgvTask2_FilterMode { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public int? LesTaskStatus { get; set; }

        /// <summary>
        /// 任务状态-查询关系运算符 
        /// </summary>
        public string LesTaskStatus_FilterMode { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public string LesTaskStatusName { get; set; }

        /// <summary>
        /// 任务状态-查询关系运算符 
        /// </summary>
        public string LesTaskStatusName_FilterMode { get; set; }

        /// <summary>
        /// 关联的库位绑定表ID
        /// </summary>
        public string MVSId { get; set; }

        /// <summary>
        /// 关联的库位绑定表ID-查询关系运算符 
        /// </summary>
        public string MVSId_FilterMode { get; set; }

        /// <summary>
        /// 关联的物料信息表ID
        /// </summary>
        public string MaterialId { get; set; }

        /// <summary>
        /// 关联的物料信息表ID-查询关系运算符 
        /// </summary>
        public string MaterialId_FilterMode { get; set; }

        /// <summary>
        /// 所属工序
        /// </summary>
        public string AllowProcedure { get; set; }

        /// <summary>
        /// 所属工序-查询关系运算符 
        /// </summary>
        public string AllowProcedure_FilterMode { get; set; }

        /// <summary>
        /// 起始站点ID
        /// </summary>
        public int? StationId { get; set; }

        /// <summary>
        /// 起始站点ID-查询关系运算符 
        /// </summary>
        public string StationId_FilterMode { get; set; }

        /// <summary>
        /// 目标站点ID
        /// </summary>
        public int? ToStationId { get; set; }

        /// <summary>
        /// 目标站点ID-查询关系运算符 
        /// </summary>
        public string ToStationId_FilterMode { get; set; }

        /// <summary>
        /// 货物类型
        /// </summary>
        public int? CargoType { get; set; }

        /// <summary>
        /// 货物类型-查询关系运算符 
        /// </summary>
        public string CargoType_FilterMode { get; set; }

        /// <summary>
        /// 物料大类
        /// </summary>
        public string MaterialCodeItemId { get; set; }

        /// <summary>
        /// 物料大类-查询关系运算符 
        /// </summary>
        public string MaterialCodeItemId_FilterMode { get; set; }

        /// <summary>
        /// 物料类型
        /// </summary>
        public int? MaterialType { get; set; }

        /// <summary>
        /// 物料类型-查询关系运算符 
        /// </summary>
        public string MaterialType_FilterMode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 备注-查询关系运算符 
        /// </summary>
        public string Remark_FilterMode { get; set; }

        /// <summary>
        /// 下发时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskIssueTime { get; set; }


        /// <summary>
        /// 完成时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskFinishTime { get; set; }


        /// <summary>
        /// 是否人工处理
        /// </summary>
        public string IsManualHandling { get; set; }

        /// <summary>
        /// 是否人工处理-查询关系运算符 
        /// </summary>
        public string IsManualHandling_FilterMode { get; set; }

        /// <summary>
        /// 人工处理人
        /// </summary>
        public string ManualHandlingUser { get; set; }

        /// <summary>
        /// 人工处理人-查询关系运算符 
        /// </summary>
        public string ManualHandlingUser_FilterMode { get; set; }

        /// <summary>
        /// 人工处理说明
        /// </summary>
        public string ManualHandlingRemark { get; set; }

        /// <summary>
        /// 人工处理说明-查询关系运算符 
        /// </summary>
        public string ManualHandlingRemark_FilterMode { get; set; }

        /// <summary>
        /// 人工处理时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ManualHandlingTime { get; set; }


        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 修改者ID
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// 修改者ID-查询关系运算符 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// 修改者-查询关系运算符 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


 
    } 
} 
