﻿using XiangziProjectTemplete.Core.Common;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 查询分拣入库任务类 
    /// </summary> 
    public class Les_PickInStockTaskParam : PageParam
    {
        /// <summary>
        /// 配料任务号
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 配料任务号-查询关系运算符 
        /// </summary>
        public string BurdenWorkNo_FilterMode { get; set; }

        /// <summary>
        /// 是否分拣完成
        /// </summary>
        public string IsPickFinish { get; set; }

        /// <summary>
        /// 是否分拣完成-查询关系运算符 
        /// </summary>
        public string IsPickFinish_FilterMode { get; set; }

        /// <summary>
        /// 所属工序
        /// </summary>
        public string AllowProcedure { get; set; }

        /// <summary>
        /// 所属工序-查询关系运算符 
        /// </summary>
        public string AllowProcedure_FilterMode { get; set; }

        /// <summary>
        /// 站点ID
        /// </summary>
        public int? StationId { get; set; }

        /// <summary>
        /// 站点ID-查询关系运算符 
        /// </summary>
        public string StationId_FilterMode { get; set; }

        /// <summary>
        /// 货物类型
        /// </summary>
        public int? CargoType { get; set; }

        /// <summary>
        /// 货物类型-查询关系运算符 
        /// </summary>
        public string CargoType_FilterMode { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 项目名称-查询关系运算符 
        /// </summary>
        public string ProjectName_FilterMode { get; set; }

        /// <summary>
        /// 跟踪号
        /// </summary>
        public string TrackingNo { get; set; }

        /// <summary>
        /// 跟踪号-查询关系运算符 
        /// </summary>
        public string TrackingNo_FilterMode { get; set; }

        /// <summary>
        /// 物料大类
        /// </summary>
        public string MaterialCodeItemId { get; set; }

        /// <summary>
        /// 物料大类-查询关系运算符 
        /// </summary>
        public string MaterialCodeItemId_FilterMode { get; set; }

        /// <summary>
        /// 物料类型
        /// </summary>
        public int? MaterialType { get; set; }

        /// <summary>
        /// 物料类型-查询关系运算符 
        /// </summary>
        public string MaterialType_FilterMode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 备注-查询关系运算符 
        /// </summary>
        public string Remark_FilterMode { get; set; }

        /// <summary>
        /// 货物数量
        /// </summary>
        public int? CargoNum { get; set; }

        /// <summary>
        /// 货物数量-查询关系运算符 
        /// </summary>
        public string CargoNum_FilterMode { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 单位-查询关系运算符 
        /// </summary>
        public string Unit_FilterMode { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        public string CargoWeight { get; set; }

        /// <summary>
        /// 重量-查询关系运算符 
        /// </summary>
        public string CargoWeight_FilterMode { get; set; }

        /// <summary>
        /// 入缓存库时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> InStockDate { get; set; }


        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 修改者ID
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// 修改者ID-查询关系运算符 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// 修改者-查询关系运算符 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }



    }
}
