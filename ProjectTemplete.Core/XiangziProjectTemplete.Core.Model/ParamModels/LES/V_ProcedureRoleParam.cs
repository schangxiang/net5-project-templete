﻿using XiangziProjectTemplete.Core.Common;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 查询角色工序查询视图类 
    /// </summary> 
    public class V_ProcedureRoleParam : PageParam
    {

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string Name_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? RoleId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string RoleId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IsHasAllProcedure { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string IsHasAllProcedure_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AllowProcedure { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AllowProcedure_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string Remark_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AllowProcedureName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AllowProcedureName_FilterMode { get; set; }


    }
}
