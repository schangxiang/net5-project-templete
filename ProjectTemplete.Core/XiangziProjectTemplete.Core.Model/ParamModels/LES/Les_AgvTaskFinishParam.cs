﻿namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// AGV任务完成参数类
    /// </summary> 
    public class Les_AgvTaskFinishParam
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 车辆名称
        /// </summary>
        public string VehicleName { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public string Timestamp { get; set; }
    }
}
