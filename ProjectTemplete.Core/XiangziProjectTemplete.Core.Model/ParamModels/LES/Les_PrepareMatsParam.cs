﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询请求备料表类 
    /// </summary> 
    public class Les_PrepareMatsParam : PageParam 
    { 
                /// <summary>
        /// 工作号
        /// </summary>
        public string WorkNo { get; set; }

        /// <summary>
        /// 工作号-查询过滤模式 
        /// </summary>
        public string WorkNo_FilterMode { get; set; }

        /// <summary>
        /// 工位号
        /// </summary>
        public string StationNo { get; set; }

        /// <summary>
        /// 工位号-查询过滤模式 
        /// </summary>
        public string StationNo_FilterMode { get; set; }

        /// <summary>
        /// 工序号
        /// </summary>
        public string ProcessNo { get; set; }

        /// <summary>
        /// 工序号-查询过滤模式 
        /// </summary>
        public string ProcessNo_FilterMode { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int? Qty { get; set; }

        /// <summary>
        /// 数量-查询过滤模式 
        /// </summary>
        public string Qty_FilterMode { get; set; }

        /// <summary>
        /// 班别
        /// </summary>
        public string Shift { get; set; }

        /// <summary>
        /// 班别-查询过滤模式 
        /// </summary>
        public string Shift_FilterMode { get; set; }

        /// <summary>
        /// 跟踪号
        /// </summary>
        public string FllowNo { get; set; }

        /// <summary>
        /// 跟踪号-查询过滤模式 
        /// </summary>
        public string FllowNo_FilterMode { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务号
        /// </summary>
        public string LesTaskNo { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务号-查询过滤模式 
        /// </summary>
        public string LesTaskNo_FilterMode { get; set; }

        /// <summary>
        /// 物料类型
        /// </summary>
        public int? MaterialType { get; set; }

        /// <summary>
        /// 物料类型-查询过滤模式 
        /// </summary>
        public string MaterialType_FilterMode { get; set; }

        /// <summary>
        /// 备料状态
        /// </summary>
        public int? PrepareMatState { get; set; }

        /// <summary>
        /// 备料状态-查询过滤模式 
        /// </summary>
        public string PrepareMatState_FilterMode { get; set; }

        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询过滤模式 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询过滤模式 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询过滤模式 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 修改者ID
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// 修改者ID-查询过滤模式 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// 修改者-查询过滤模式 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// 是否逻辑删除-查询过滤模式 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }

 
    } 
} 
