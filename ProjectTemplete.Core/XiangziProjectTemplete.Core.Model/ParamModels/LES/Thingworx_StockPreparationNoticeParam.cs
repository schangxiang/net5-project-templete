﻿using XiangziProjectTemplete.Core.Common;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 查询Thingworx 备料通知表类 
    /// </summary> 
    public class Thingworx_StockPreparationNoticeParam : PageParam
    {
        /// <summary>
        /// 工序编号
        /// </summary>
        public string Procedure { get; set; }

        /// <summary>
        /// 工序编号-查询关系运算符 
        /// </summary>
        public string Procedure_FilterMode { get; set; }

        /// <summary>
        /// 工序
        /// </summary>
        public string ProcedureName { get; set; }

        /// <summary>
        /// 工序-查询关系运算符 
        /// </summary>
        public string ProcedureName_FilterMode { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 任务号-查询关系运算符 
        /// </summary>
        public string BurdenWorkNo_FilterMode { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material { get; set; }

        /// <summary>
        /// 物料编号-查询关系运算符 
        /// </summary>
        public string Material_FilterMode { get; set; }

        /// <summary>
        /// 物料
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料-查询关系运算符 
        /// </summary>
        public string MaterialName_FilterMode { get; set; }

        /// <summary>
        /// 备料状态
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 备料状态-查询关系运算符 
        /// </summary>
        public string Status_FilterMode { get; set; }

        /// <summary>
        /// 备料状态
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// 备料状态-查询关系运算符 
        /// </summary>
        public string StatusName_FilterMode { get; set; }

        /// <summary>
        /// LES_PickInStockTask的任务号
        /// </summary>
        public string PickInStockTaskID { get; set; }

        /// <summary>
        /// LES_PickInStockTask的任务号-查询关系运算符 
        /// </summary>
        public string PickInStockTaskID_FilterMode { get; set; }

        /// <summary>
        /// Thingworx返回结果
        /// </summary>
        public string ThingworxResult { get; set; }

        /// <summary>
        /// Thingworx返回结果-查询关系运算符 
        /// </summary>
        public string ThingworxResult_FilterMode { get; set; }

        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


    }
}
