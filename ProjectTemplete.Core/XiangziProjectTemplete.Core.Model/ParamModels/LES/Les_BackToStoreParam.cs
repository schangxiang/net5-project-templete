﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询回库搬运类 
    /// </summary> 
    public class Les_BackToStoreParam : PageParam 
    { 
                /// <summary>
        /// 任务类型
        /// </summary>
        public int? TaskType { get; set; }

        /// <summary>
        /// 任务类型-查询过滤模式 
        /// </summary>
        public string TaskType_FilterMode { get; set; }

        /// <summary>
        /// 工位
        /// </summary>
        public string Station { get; set; }

        /// <summary>
        /// 工位-查询过滤模式 
        /// </summary>
        public string Station_FilterMode { get; set; }

        /// <summary>
        /// 起始位置
        /// </summary>
        public string SourceNo { get; set; }

        /// <summary>
        /// 起始位置-查询过滤模式 
        /// </summary>
        public string SourceNo_FilterMode { get; set; }

        /// <summary>
        /// 目标位置
        /// </summary>
        public string ToNo { get; set; }

        /// <summary>
        /// 目标位置-查询过滤模式 
        /// </summary>
        public string ToNo_FilterMode { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 状态-查询过滤模式 
        /// </summary>
        public string Status_FilterMode { get; set; }

        /// <summary>
        /// 制单时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskCrateTime { get; set; }


        /// <summary>
        /// 完成时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskFinishTime { get; set; }


        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询过滤模式 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询过滤模式 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询过滤模式 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 修改者ID
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// 修改者ID-查询过滤模式 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// 修改者-查询过滤模式 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 是否逻辑删除
        /// </summary>
        public string IsDeleted { get; set; }

        /// <summary>
        /// 是否逻辑删除-查询过滤模式 
        /// </summary>
        public string IsDeleted_FilterMode { get; set; }

 
    } 
} 
