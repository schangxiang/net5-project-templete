﻿namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 获取空库位列表的查询参数 
    /// </summary> 
    public class GetEmptyStationListParam
    {
        /// <summary>
        /// 库存区域编码
        /// </summary>
        public string StoreAreaCode { get; set; }

        /// <summary>
        /// 入库场景,枚举 InStoreSceneEnum
        /// </summary>
        public int InStoreSceneEnum { get; set; }
    }
}
