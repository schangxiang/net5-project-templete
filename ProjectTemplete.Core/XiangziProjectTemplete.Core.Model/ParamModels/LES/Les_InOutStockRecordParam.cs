﻿using XiangziProjectTemplete.Core.Common; 
using System.Collections.Generic; 
 
namespace XiangziProjectTemplete.Core.Model.ParamModels 
{ 
    /// <summary> 
    /// 查询出入库记录类 
    /// </summary> 
    public class Les_InOutStockRecordParam : PageParam 
    { 
                /// <summary>
        /// 出入库类型
        /// </summary>
        public int? InOutStockType { get; set; }

        /// <summary>
        /// 出入库类型-查询关系运算符 
        /// </summary>
        public string InOutStockType_FilterMode { get; set; }

        /// <summary>
        /// 出入库类型
        /// </summary>
        public string InOutStockTypeName { get; set; }

        /// <summary>
        /// 出入库类型-查询关系运算符 
        /// </summary>
        public string InOutStockTypeName_FilterMode { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public int? AGV_TaskType { get; set; }

        /// <summary>
        /// 任务类型-查询关系运算符 
        /// </summary>
        public string AGV_TaskType_FilterMode { get; set; }

        /// <summary>
        /// 任务类型名称
        /// </summary>
        public string AGV_TaskTypeName { get; set; }

        /// <summary>
        /// 任务类型名称-查询关系运算符 
        /// </summary>
        public string AGV_TaskTypeName_FilterMode { get; set; }

        /// <summary>
        /// 任务描述
        /// </summary>
        public string AGV_TaskDesc { get; set; }

        /// <summary>
        /// 任务描述-查询关系运算符 
        /// </summary>
        public string AGV_TaskDesc_FilterMode { get; set; }

        /// <summary>
        /// 执行车辆
        /// </summary>
        public string ProcessingVehicle { get; set; }

        /// <summary>
        /// 执行车辆-查询关系运算符 
        /// </summary>
        public string ProcessingVehicle_FilterMode { get; set; }

        /// <summary>
        /// 车辆描述
        /// </summary>
        public string AgvNameDesc { get; set; }

        /// <summary>
        /// 车辆描述-查询关系运算符 
        /// </summary>
        public string AgvNameDesc_FilterMode { get; set; }

        /// <summary>
        /// 配料任务号
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 配料任务号-查询关系运算符 
        /// </summary>
        public string BurdenWorkNo_FilterMode { get; set; }

        /// <summary>
        /// 站点ID
        /// </summary>
        public int? StationId { get; set; }

        /// <summary>
        /// 站点ID-查询关系运算符 
        /// </summary>
        public string StationId_FilterMode { get; set; }

        /// <summary>
        /// 站点编号
        /// </summary>
        public string StationCode { get; set; }

        /// <summary>
        /// 站点编号-查询关系运算符 
        /// </summary>
        public string StationCode_FilterMode { get; set; }

        /// <summary>
        /// 站点名称
        /// </summary>
        public string StationName { get; set; }

        /// <summary>
        /// 站点名称-查询关系运算符 
        /// </summary>
        public string StationName_FilterMode { get; set; }

        /// <summary>
        /// 物料号
        /// </summary>
        public string MaterialCode { get; set; }

        /// <summary>
        /// 物料号-查询关系运算符 
        /// </summary>
        public string MaterialCode_FilterMode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料名称-查询关系运算符 
        /// </summary>
        public string MaterialName_FilterMode { get; set; }

        /// <summary>
        /// 操作说明
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// 操作说明-查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// 创建者ID-查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者-查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


 
    } 
} 
