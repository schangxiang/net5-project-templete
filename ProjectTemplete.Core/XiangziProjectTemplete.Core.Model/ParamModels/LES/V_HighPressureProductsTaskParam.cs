﻿using XiangziProjectTemplete.Core.Common;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ParamModels
{
    /// <summary> 
    /// 查询高压绕线线圈 查询视图类 
    /// </summary> 
    public class V_HighPressureProductsTaskParam : PageParam
    {
        [NoAutoQuery]
        public List<int> LesTaskTypes { get; set; }


        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ManualHandlingTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string ModifyBy { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ModifyBy_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PlaceTypeName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string PlaceTypeName_FilterMode { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string LesTaskMode { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string LesTaskMode_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? StationId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string StationId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AgvTask2 { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AgvTask2_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IsManualHandling { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string IsManualHandling_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string MaterialName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CreateBy_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AllowProcedureName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AllowProcedureName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MVSId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string MVSId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LesTaskNo { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string LesTaskNo_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ToStationCode { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ToStationCode_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ManualHandlingUser { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ManualHandlingUser_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> CreateTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string BurdenWorkNo_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskIssueTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string StoreArea { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string StoreArea_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OperationRemark { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string OperationRemark_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> ModifyTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public int? IsFull { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string IsFull_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MaterialCodeItemId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string MaterialCodeItemId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? LesTaskStatus { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string LesTaskStatus_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //表示是 高级查询范围查询特性
        [HighSearchRangeAttribute]
        public List<string> TaskFinishTime { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string MaterialCode { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string MaterialCode_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MaterialId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string MaterialId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AllowLineName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AllowLineName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StationCode { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string StationCode_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ManualHandlingRemark { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ManualHandlingRemark_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ToStationName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ToStationName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? MaterialType { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string MaterialType_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ToStationId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ToStationId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StoreAreaName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string StoreAreaName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string Remark_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CreateId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CreateId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CargoTypeName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CargoTypeName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LesTaskStatusName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string LesTaskStatusName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IsLock { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string IsLock_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CargoType { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string CargoType_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ModifyId { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string ModifyId_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PlaceTypeCode { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string PlaceTypeCode_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AllowProcedure { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AllowProcedure_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StationName { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string StationName_FilterMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AgvTask1 { get; set; }

        /// <summary>
        /// -查询关系运算符 
        /// </summary>
        public string AgvTask1_FilterMode { get; set; }


    }
}
