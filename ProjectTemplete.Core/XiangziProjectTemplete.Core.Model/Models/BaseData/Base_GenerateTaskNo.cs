﻿using SqlSugar;

//注意：要想DBFirst，命名空间必须是 XiangziProjectTemplete.Core.Model.Models
namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 生成任务号表
    /// </summary>
    [SugarTable(tableName: "Base_GenerateTaskNo", tableDescription: "任务号表")]
    public class Base_GenerateTaskNo : GeneralBusinessRoot<int>
    {
        /// <summary>
        /// 生成的任务号类型
        /// </summary>
        [SugarColumn(ColumnDescription = "生成的任务号类型")]
        public int GenerateTaskType { get; set; }

        /// <summary>
        /// 生成的任务号前缀
        /// </summary>
        [SugarColumn(ColumnDescription = "生成的任务号前缀")]
        public string GenerateTaskNoPrefix { get; set; }

        /// <summary>
        /// 生成的任务号数值
        /// </summary>
        [SugarColumn(ColumnDescription = "生成的任务号数值")]
        public int GenerateTaskId { get; set; }

        /// <summary>
        /// 生成的任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "生成的任务号")]
        public string GenerateTaskNo { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [SugarColumn(ColumnDescription = "描述")]
        public string GenerateDescription { get; set; }

    }
}
