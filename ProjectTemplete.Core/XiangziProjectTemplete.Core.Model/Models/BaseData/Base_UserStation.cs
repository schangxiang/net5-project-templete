﻿using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 用户站点绑定关系
    /// </summary>
    public class Base_UserStation : GeneralBusinessRoot<int>
    {

        /// <summary>
        /// 用户ID
        /// </summary>
        [SugarColumn(ColumnDescription = "用户ID")]
        public int uID { get; set; }


        /// <summary>
        /// 登录账号
        /// </summary>
        [SugarColumn(ColumnDataType = "nvarchar", Length = 200, IsNullable = true)]
        public string uLoginName { get; set; }



        /// <summary>
        /// 站点ID的合集，用逗号分隔
        /// </summary>
        [SugarColumn(ColumnDescription = "绑定的站点", IsNullable = true)]
        public string BindStation { get; set; }


        /// <summary>
        ///  站点ID的完整合集，包括上级工序，用逗号分隔，是 BindStationList List<int[]>的序列化结果
        /// </summary>
        [SugarColumn(ColumnDescription = "绑定的工序下的站点", IsNullable = true)]
        public string BindStationListStr { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "备注")]
        public string Remark { get; set; }


    }
}
