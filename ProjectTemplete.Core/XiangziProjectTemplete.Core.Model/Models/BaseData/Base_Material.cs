﻿using SqlSugar;
using System;

//注意：要想DBFirst，命名空间必须是 XiangziProjectTemplete.Core.Model.Models
namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 物料信息
    /// </summary>
    [SugarTable(tableName: "Base_Material", tableDescription: "物料信息表")]
    public class Base_Material : GeneralBusinessRoot<string>
    {
        /// <summary>
        /// 物料类型
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型")]
        public int CargoType { get; set; }

        /// <summary>
        /// 物料类型
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型", IsNullable = true)]
        public string CargoTypeName { get; set; }


        /// <summary>
        /// 物料分类,枚举 MaterialClassifyEnum
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料分类")]
        public int MaterialClassify { get; set; }

        /// <summary>
        /// 物料分类,枚举 MaterialClassifyEnum
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料分类")]
        public string MaterialClassifyName { get; set; }



        /// <summary>
        /// 配料任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "配料任务号", IsNullable = true)]
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 所属工序
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工序", IsNullable = true)]
        public string AllowProcedure { get; set; }


        /// <summary>
        /// 物料大类的码表项ID,在码表中配置
        /// </summary>
        [SugarColumn(ColumnDescription = "物料大类的码表项ID", IsNullable = true)]
        public string MaterialCodeItemId { get; set; }

        /// <summary>
        /// 物料大类,在码表中配置
        /// </summary>
        [SugarColumn(ColumnDescription = "物料大类", IsNullable = true)]
        public string MaterialCode { get; set; }

        /// <summary>
        /// 货物名称
        /// </summary>
        [SugarColumn(ColumnDescription = "货物名称", IsNullable = true)]
        public string MaterialName { get; set; }



        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "备注")]
        public string Remark { get; set; }


        /// <summary>
        /// 货物数量
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "货物数量")]
        public decimal? CargoNum { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "单位")]
        public string Unit { get; set; }


        /// <summary>
        /// 入库时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "入库时间")]
        public DateTime? InStoreTime { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "重量")]
        public decimal? CargoWeight { get; set; }

        /// <summary>
        /// 物料来源站点
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料来源站点")]
        public int? SourceStationId { get; set; }


    }
}
