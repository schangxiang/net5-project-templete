using SqlSugar;
using System;

namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 系统异常信息表
    /// </summary>
    public class Base_SysExceptionInfo : RootEntityTkey<string>
    {

        [SugarColumn(Length = 500, ColumnDescription = "模块", IsNullable = true)]
        public string module { get; set; }

        [SugarColumn(Length = 500, ColumnDescription = "级别", IsNullable = true)]
        public string exceptionLevel { get; set; }

        [SugarColumn(Length = 5000, ColumnDescription = "错误来源", IsNullable = true)]
        public string exceptionSource { get; set; }

        [SugarColumn(Length = 5000, ColumnDescription = "错误方法", IsNullable = true)]
        public string exceptionFun { get; set; }

        [SugarColumn(Length = 5000, ColumnDescription = "参数", IsNullable = true)]
        public string sourceData { get; set; }

        [SugarColumn(Length = 5000, ColumnDescription = "异常信息", IsNullable = true)]
        public string exceptionMsg { get; set; }
        [SugarColumn(Length = 5000, ColumnDescription = "异常堆栈", IsNullable = true)]
        public string exceptionData { get; set; }

        [SugarColumn(Length = 500, ColumnDescription = "主机名", IsNullable = true)]
        public string host { get; set; }

        [SugarColumn(Length = 500, ColumnDescription = "关键字1", IsNullable = true)]
        public string key1 { get; set; }

        [SugarColumn(Length = 500, ColumnDescription = "关键字2", IsNullable = true)]
        public string key2 { get; set; }

        [SugarColumn(Length = 500, ColumnDescription = "关键字3", IsNullable = true)]
        public string key3 { get; set; }

        [SugarColumn(Length = 500, ColumnDescription = "创建人", IsNullable = true)]
        public string creator { get; set; }

        [SugarColumn(ColumnDescription = "创建时间", IsNullable = true)]
        public DateTime? createTime { get; set; }
    }
}
