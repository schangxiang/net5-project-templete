﻿using SqlSugar;
using System;

//注意：要想DBFirst，命名空间必须是 XiangziProjectTemplete.Core.Model.Models
namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 站点管理
    /// </summary>
    [SugarTable(tableName: "Base_Station", tableDescription: "站点管理表")]
    public class Base_Station : RootEntityTkey<int>
    {
        /// <summary>
        /// 库存区域，在码表中配置,码表Base_CodeItems的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "库存区域")]
        public string StoreArea { get; set; }

        /// <summary>
        /// 所属工序，在码表中配置,码表Base_CodeItems的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工序")]
        public string AllowProcedure { get; set; }

        /// <summary>
        /// 工位类型，，在码表中配置，枚举 StationPlaceTypeEnum ,码表Base_CodeItems的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "工位类型", IsNullable = true)]
        public string PlaceType { get; set; }

        /// <summary>
        /// 所属产线，在码表中配置
        /// </summary>
        [SugarColumn(ColumnDescription = "所属产线", IsNullable = true)]
        public string AllowLine { get; set; }

        /// <summary>
        /// 站点编号,唯一
        /// </summary>
        [SugarColumn(ColumnDescription = "站点编号")]
        public string StationCode { get; set; }

        /// <summary>
        /// 站点名称
        /// </summary>
        [SugarColumn(ColumnDescription = "站点名称")]
        public string StationName { get; set; }


        /// <summary>
        /// Agv站点
        /// </summary>
        [SugarColumn(ColumnDescription = "Agv站点", IsNullable = true)]
        public string AgvStation { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "备注")]
        public string Remark { get; set; }


        /// <summary>
        /// 是否锁定
        /// </summary>
        [SugarColumn(ColumnDescription = "是否锁定", IsNullable = true)]
        public int? IsLock { get; set; }


        /// <summary>
        /// 是否有货
        /// </summary>
        [SugarColumn(ColumnDescription = "是否有货", IsNullable = true)]
        public int? IsFull { get; set; }


        #region 创建者等公共用的

        /// <summary>
        /// 是否禁用
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "是否禁用")]
        public bool IsDeleted { get; set; } = false;

        /// <summary>
        /// 操作说明
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "操作说明")]
        public string OperationRemark { get; set; }

        /// <summary>
        /// 创建ID
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "创建者ID")]
        public int? CreateId { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        [SugarColumn(ColumnDataType = "nvarchar", Length = 50, IsNullable = true, ColumnDescription = "创建者")]
        public string CreateBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "创建时间")]
        public DateTime? CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 修改ID
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "修改ID")]
        public int? ModifyId { get; set; }
        /// <summary>
        /// 修改者
        /// </summary>
        [SugarColumn(ColumnDataType = "nvarchar", Length = 50, IsNullable = true, ColumnDescription = "修改者")]
        public string ModifyBy { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "修改时间")]
        public DateTime? ModifyTime { get; set; } = DateTime.Now;

        #endregion
    }
}
