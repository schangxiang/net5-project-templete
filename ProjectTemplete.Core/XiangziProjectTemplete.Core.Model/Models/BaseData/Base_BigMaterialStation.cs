﻿using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 大类物料和站点绑定信息
    /// </summary>
    public class Base_BigMaterialStation : SimpleGeneralBusinessRoot<string>
    {
        /// <summary>
        /// 工序的Base_CodeItems表的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "工序的代码项表ID")]
        public int ProcedureCodeItemsId { get; set; }

        /// <summary>
        /// 大类物料的Base_CodeItems表的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "大类物料的代码项表ID")]
        public int BigMaterialCodeItemsId { get; set; }

        /// <summary>
        /// 站点表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "站点表ID")]
        public int StationId { get; set; }

    }
}
