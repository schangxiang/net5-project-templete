﻿using SqlSugar;
using System;

//注意：要想DBFirst，命名空间必须是 XiangziProjectTemplete.Core.Model.Models
namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 系统基础配置表
    /// </summary>
    [SugarTable(tableName: "Base_BasicDataSet", tableDescription: "系统基础配置表")]
    public class Base_BasicDataSet : GeneralBusinessRoot<int>
    {
        /// <summary>
        /// 基础数据类型
        /// </summary>
        [SugarColumn(ColumnDescription = "BasicDataType")]
        public string BasicDataType { get; set; }

        /// <summary>
        /// 基础数据值
        /// </summary>
        [SugarColumn(ColumnDescription = "基础数据值", IsNullable = true)]
        public string BasicDataValue { get; set; }

        /// <summary>
        /// 数据单位
        /// </summary>
        [SugarColumn(ColumnDescription = "数据单位", IsNullable = true)]
        public string BasicDataUnit { get; set; }

        /// <summary>
        /// 机器名
        /// </summary>
        [SugarColumn(ColumnDescription = "机器名", IsNullable = true)]
        public string Machine { get; set; }


        /// <summary>
        /// 上线时间
        /// </summary>
        [SugarColumn(ColumnDescription = "上线时间", IsNullable = true)]
        public DateTime? OnLineTime { get; set; }

        /// <summary>
        /// 最后下线时间
        /// </summary>
        [SugarColumn(ColumnDescription = "最后下线时间", IsNullable = true)]
        public DateTime? LastUnLineTime { get; set; }


        /// <summary>
        /// 最后机器名
        /// </summary>
        [SugarColumn(ColumnDescription = "最后机器名", IsNullable = true)]
        public string LastMachine { get; set; }



    }
}
