﻿using SqlSugar;

namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 异常信息表实体类
    /// </summary>
    public class ExceptionInfo : RootEntityTkey<int>
    {
        public ExceptionInfo()
        {
        }


        /// <summary>
        /// 关键词1（一般存taskNo，loadNumber）
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string key1 { get; set; }


        /// <summary>
        /// 关键词2（一般存流转卡号，line）
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string key2 { get; set; }

        /// <summary>
        /// 异常级别
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string exceptionLevel { get; set; }

        /// <summary>
        /// 异常方向
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string exceptionSource { get; set; }

        /// <summary>
        /// 异常时方法名
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string exceptionFun { get; set; }

        /// <summary>
        /// 源数据的json数据
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string sourceData { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string exceptionMsg { get; set; }

        /// <summary>
        /// 异常的json数据
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string exceptionData { get; set; }


        /// <summary>
        /// 错误发生所在的host
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string host { get; set; }


        /// <summary>
        /// 创建人
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string creator { get; set; }

    }
}