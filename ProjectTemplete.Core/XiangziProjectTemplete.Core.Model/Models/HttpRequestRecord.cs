﻿using SqlSugar;
using System;

namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// Http请求记录表实体类
    /// </summary>
    public class HttpRequestRecord : RootEntityTkey<string>
    {
        /// <summary>
        /// 关键词1（一般存taskNo，loadNumber）
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "关键词1", IsNullable = true)]
        public string key1 { get; set; }


        /// <summary>
        /// 关键词2（一般存流转卡号，line）
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "关键词2", IsNullable = true)]
        public string key2 { get; set; }


        /// <summary>
        /// 方向( 1  接收 2  推送)
        /// </summary>
        [SugarColumn(ColumnDescription = "方向( 1  接收 2  推送)", IsNullable = true)]
        public int direction { get; set; } = 2;

        /// <summary>
        /// 方法名
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "方法名", IsNullable = true)]
        public string fullFun { get; set; }

        /// <summary>
        /// 请求host
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "请求host", IsNullable = true)]
        public string host { get; set; }

        /// <summary>
        /// 请求地址
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "请求地址", IsNullable = true)]
        public string url { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        [SugarColumn(Length = 5000, ColumnDescription = "请求参数", IsNullable = true)]
        public string param { get; set; }

        /// <summary>
        /// 请求返回结果
        /// </summary>
        [SugarColumn(Length = 5000, ColumnDescription = "请求返回结果", IsNullable = true)]
        public string retResult { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(Length = 5000, ColumnDescription = "备注", IsNullable = true)]
        public string remark { get; set; }

        /// <summary>
        /// 发生的主机host
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "发生的主机host", IsNullable = true)]
        public string happenHost { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "创建时间")]
        public DateTime? CreateTime { get; set; } = DateTime.Now;
    }
}