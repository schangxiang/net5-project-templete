﻿using System;
using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 呼叫任务
    /// </summary>
    public class Les_CallOrder : GeneralBusinessRoot<string>
    {

        /// <summary>
        /// 呼叫任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫任务号")]
        public string CallOrderTaskNo { get; set; }

        /// <summary>
        /// 呼叫模式
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫模式", IsNullable = true)]
        public int LesTaskMode { get; set; }

        /// <summary>
        /// 呼叫模式
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫模式", IsNullable = true)]
        public string LesTaskModeName { get; set; }


        /// <summary>
        /// 配料任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "配料任务号", IsNullable = true)]
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 实际配料任务号，是分拣托盘上的任务号！
        /// </summary>
        [SugarColumn(ColumnDescription = "实际配料任务号", IsNullable = true)]
        public string BurdenWorkNoActual { get; set; }


        /// <summary>
        /// AGV任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "AGV任务号", IsNullable = true)]
        public string AgvTaskNo { get; set; }

        /// <summary>
        /// AGV任务号2
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "AGV任务号2", IsNullable = true)]
        public string AgvTaskNo2 { get; set; }


        /// <summary>
        /// 所属工序，在码表中配置,码表项的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工序")]
        public string AllowProcedure { get; set; }

        /// <summary>
        /// 所属产线，在码表中配置,码表项的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "所属产线", IsNullable = true)]
        public string AllowLine { get; set; }

        #region  站点相关


        /// <summary>
        /// 呼叫站点ID,Base_Station的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫站点ID")]
        public int StationId { get; set; }


        /// <summary>
        /// 送货站点ID,Base_Station的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "送货站点ID")]
        public int DeliveryStationId { get; set; }

        #endregion


        #region 物料相关

        /// <summary>
        /// 物料大类的码表项ID,在码表中配置
        /// </summary>
        [SugarColumn(ColumnDescription = "物料大类的码表项ID", IsNullable = true)]
        public string MaterialCodeItemId { get; set; }


        /// <summary>
        /// 关联的Base_Material表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "关联的物料信息表ID", IsNullable = true)]
        public string MaterialId { get; set; }



        #endregion

        /// <summary>
        /// 叫料说明
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "叫料说明", IsNullable = true)]
        public string CallOrderRemark { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "项目名称", IsNullable = true)]
        public string ProjectName { get; set; }

        /// <summary>
        /// 工单号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "工单号", IsNullable = true)]
        public string OrderNo { get; set; }

        /// <summary>
        /// 状态，枚举 LesTaskStatusEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public int Status { get; set; }

        /// <summary>
        /// 状态，枚举 LesTaskStatusEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public string StatusName { get; set; }

        /// <summary>
        /// 下发时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "下发时间")]
        public DateTime? TaskIssueTime { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "完成时间")]
        public DateTime? TaskFinishTime { get; set; }


        #region 异常处理

        /// <summary>
        /// 是否人工处理
        /// </summary>
        [SugarColumn(ColumnDescription = "是否人工处理", IsNullable = true)]
        public bool? IsManualHandling { get; set; }


        /// <summary>
        /// 人工处理人
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理人", IsNullable = true)]
        public string ManualHandlingUser { get; set; }

        /// <summary>
        /// 人工处理说明
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理说明", IsNullable = true)]
        public string ManualHandlingRemark { get; set; }

        /// <summary>
        /// 人工处理时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "人工处理时间")]
        public DateTime? ManualHandlingTime { get; set; }

        #endregion


        #region 物料冗余字段


        /// <summary>
        /// 物料大类编号,在码表中配置
        /// </summary>
        [SugarColumn(ColumnDescription = "物料大类编号", IsNullable = true)]
        public string MaterialCode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [SugarColumn(ColumnDescription = "物料名称", IsNullable = true)]
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料分类,枚举 MaterialClassifyEnum
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料分类")]
        public int MaterialClassify { get; set; }

        /// <summary>
        /// 物料分类,枚举 MaterialClassifyEnum
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料分类")]
        public string MaterialClassifyName { get; set; }

        /// <summary>
        /// 物料类型，枚举 CargoTypeEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型", IsNullable = true)]
        public int CargoType { get; set; }

        /// <summary>
        /// 物料类型，枚举 CargoTypeEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型", IsNullable = true)]
        public string CargoTypeName { get; set; }

        #endregion


        /// <summary>
        /// 数据来源，枚举 DataSourceEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型", IsNullable = true)]
        public int? DataSource { get; set; }

        /// <summary>
        /// 数据来源，枚举 DataSourceEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型", IsNullable = true)]
        public string DataSourceName { get; set; }

    }
}
