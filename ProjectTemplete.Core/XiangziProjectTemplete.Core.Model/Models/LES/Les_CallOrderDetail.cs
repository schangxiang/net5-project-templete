﻿using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 呼叫任务明细
    /// </summary>
    public class Les_CallOrderDetail : GeneralBusinessRoot<string>
    {

        /// <summary>
        /// 呼叫订单ID
        /// </summary>
        [SugarColumn( ColumnDescription = "呼叫订单ID")]
        public string CallOrderId { get; set; }

        /// <summary>
        /// 物料号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "物料号")]
        public string MaterielNo { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "物料名称")]
        public string MaterielName { get; set; }

        /// <summary>
        /// 型号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "型号")]
        public string MaterielType { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        [SugarColumn(ColumnDescription = "重量")]
        public decimal MaterielWeight { get; set; }

        /// <summary>
        /// 原始数量
        /// </summary>
        [SugarColumn(ColumnDescription = "原始数量")]
        public int OriginalNum { get; set; }

        /// <summary>
        /// 剩余数量
        /// </summary>
        [SugarColumn(ColumnDescription = "剩余数量")]
        public int SurplusNum { get; set; }


        /// <summary>
        /// 已叫料数量
        /// </summary>
        [SugarColumn(ColumnDescription = "已叫料数量")]
        public int HasCallNum { get; set; }

        /// <summary>
        /// 已叫料次数
        /// </summary>
        [SugarColumn(ColumnDescription = "已叫料次数")]
        public int HasCallCount { get; set; }

    }
}
