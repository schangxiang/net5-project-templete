﻿using System;
using SqlSugar;

//注意：要想DBFirst，命名空间必须是 XiangziProjectTemplete.Core.Model.Models
namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 高压绕线线圈 流转任务
    /// </summary>
    public class Les_HighPressureProductsTask : SimpleGeneralBusinessRoot<string>
    {
        /// <summary>
        /// XiangziProjectTemplete任务类型,枚举 LesTaskTypeEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务类型")]
        public int LesTaskType { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务类型,枚举 LesTaskTypeEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务类型")]
        public string LesTaskTypeName { get; set; }


        /// <summary>
        /// 低压内膜任务标记  叫料/回托盘
        /// </summary>
        [SugarColumn(ColumnDescription = "低压内膜任务标记", IsNullable = true)]
        public string LowPressureMoldTaskFlag { get; set; }


        /// <summary>
        /// XiangziProjectTemplete任务号,有系统自己生成的随机任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务号")]
        public string LesTaskNo { get; set; }

        /// <summary>
        /// 生产任务号，操作工输入的生产使用的任务号！
        /// </summary>
        [SugarColumn(ColumnDescription = "生产任务号", IsNullable = true)]
        public string BurdenWorkNo { get; set; }

        #region AGV任务有关

        /// <summary>
        /// AGV任务1
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV任务1", IsNullable = true)]
        public string AgvTask1 { get; set; }

        /// <summary>
        /// AGV任务2
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV任务2", IsNullable = true)]
        public string AgvTask2 { get; set; }


        #endregion

        #region 任务状态

        /// <summary>
        /// 任务状态,枚举 LesTaskStatusEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public int LesTaskStatus { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public string LesTaskStatusName { get; set; }

        #endregion 


        /// <summary>
        /// 关联的库位绑定表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "关联的库位绑定表ID", IsNullable = true)]
        public string MVSId { get; set; }


        /// <summary>
        /// 关联的Base_Material表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "关联的物料信息表ID", IsNullable = true)]
        public string MaterialId { get; set; }

        /// <summary>
        /// 所属工序
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工序")]
        public string AllowProcedure { get; set; }

        /// <summary>
        /// 起始站点ID
        /// </summary>
        [SugarColumn(ColumnDescription = "起始站点ID")]
        public int StationId { get; set; }


        /// <summary>
        /// 目标站点ID
        /// </summary>
        [SugarColumn(ColumnDescription = "目标站点ID")]
        public int ToStationId { get; set; } = 0;


        /// <summary>
        /// 物料大类,在码表中配置,是一个值
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "物料大类")]
        public string MaterialCodeItemId { get; set; }


        /// <summary>
        /// 物料类型,枚举 MaterialTypeEnum
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料类型")]
        public int MaterialType { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "备注")]
        public string Remark { get; set; }

        /// <summary>
        /// 下发时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "下发时间")]
        public DateTime? TaskIssueTime { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "完成时间")]
        public DateTime? TaskFinishTime { get; set; }

        #region 异常处理

        /// <summary>
        /// 是否人工处理
        /// </summary>
        [SugarColumn(ColumnDescription = "是否人工处理", IsNullable = true)]
        public bool? IsManualHandling { get; set; }


        /// <summary>
        /// 人工处理人
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理人", IsNullable = true)]
        public string ManualHandlingUser { get; set; }

        /// <summary>
        /// 人工处理说明
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理说明", IsNullable = true)]
        public string ManualHandlingRemark { get; set; }

        /// <summary>
        /// 人工处理时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "人工处理时间")]
        public DateTime? ManualHandlingTime { get; set; }

        #endregion


        #region 物料冗余字段


        /// <summary>
        /// 物料大类编号,在码表中配置
        /// </summary>
        [SugarColumn(ColumnDescription = "物料大类编号", IsNullable = true)]
        public string MaterialCode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [SugarColumn(ColumnDescription = "物料名称", IsNullable = true)]
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料分类,枚举 MaterialClassifyEnum
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料分类")]
        public int MaterialClassify { get; set; }

        /// <summary>
        /// 物料分类,枚举 MaterialClassifyEnum
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料分类")]
        public string MaterialClassifyName { get; set; }

        /// <summary>
        /// 物料类型，枚举 CargoTypeEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型", IsNullable = true)]
        public int CargoType { get; set; }

        /// <summary>
        /// 物料类型，枚举 CargoTypeEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型", IsNullable = true)]
        public string CargoTypeName { get; set; }

        #endregion


        #region 呼叫模式，目前仅用作 高压内膜物流

        /// <summary>
        /// 呼叫模式
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫模式", IsNullable = true)]
        public int? LesTaskMode { get; set; }

        /// <summary>
        /// 呼叫模式
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫模式", IsNullable = true)]
        public string LesTaskModeName { get; set; }

        #endregion


    }
}
