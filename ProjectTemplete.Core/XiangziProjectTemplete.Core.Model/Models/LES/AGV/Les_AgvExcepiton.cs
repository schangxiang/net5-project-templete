﻿using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// Agv异常信息
    /// </summary>
    public class Les_AgvExcepiton : Les_AgvTask
    {
        /// <summary>
        /// 处理措施
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "处理措施")]
        public int? DoType { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "异常信息")]
        public string ExcepitonMsg { get; set; }

        
    }
}
