﻿using SqlSugar;
using System;

namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// AGV报警
    /// </summary>
    public class Les_AgvWarning : SimpleGeneralBusinessRoot<int>
    {
        /// <summary>
        /// 报警时间
        /// </summary>
        [SugarColumn(ColumnDescription = "报警时间")]
        public DateTime WarningTime { get; set; }


        /// <summary>
        /// 车辆名称
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "车辆名称")]
        public string AgvName { get; set; }

        /// <summary>
        /// 车辆描述
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "车辆描述", IsNullable = true)]
        public string AgvNameDesc { get; set; }


        /// <summary>
        /// 报警位置
        /// </summary>
        [SugarColumn(ColumnDescription = "报警位置")]
        public string WarningLocation { get; set; }


        /// <summary>
        /// 报警代码
        /// </summary>
        [SugarColumn(ColumnDescription = "报警代码")]
        public int WarningCode { get; set; }

        /// <summary>
        /// 报警内容
        /// </summary>
        [SugarColumn(ColumnDescription = "报警内容")]
        public string WarningContent { get; set; }

        /// <summary>
        /// 状态（0：新建 1：已处理）
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public int Status { get; set; } = 0;


        /// <summary>
        /// 状态（0：新建 1：已处理）
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public string StatusName { get; set; } = "状态";


        /// <summary>
        /// 是否工人已知道（0：未知 1：工人已知道）
        /// </summary>
        [SugarColumn(ColumnDescription = "是否工人已知道", IsNullable = true)]
        public int IsKnow { get; set; }


        /// <summary>
        /// 关闭报警说明
        /// </summary>
        [SugarColumn(ColumnDescription = "关闭报警说明")]
        public string CloseContent { get; set; } = " ";

        /// <summary>
        /// 完成时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "完成时间")]
        public DateTime? FinishTime { get; set; }


        /// <summary>
        /// 持续时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "持续时间")]
        public string DurationTime { get; set; }

        /// <summary>
        /// 持续时间(分钟)
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "持续时间(分钟)")]
        public decimal? DurationMinuteTime { get; set; }
    }
}
