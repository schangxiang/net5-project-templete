using SqlSugar;

namespace XiangziProjectTemplete.Core.Model.Models
{
    public class Les_AgvTaskRequestLog : GeneralBusinessRoot<string>
    {
        /// <summary>
        /// 任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "任务号")]
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型")]
        public int TaskType { get; set; }


        /// <summary>
        /// 请求状态
        /// </summary>
        [SugarColumn(ColumnDescription = "请求状态")]
        public int RequestStatus { get; set; }

        /// <summary>
        /// 起始点
        /// </summary>
        [SugarColumn(ColumnDescription = "起始点", IsNullable = true)]
        public int SourcePlace { get; set; }

        /// <summary>
        /// 起始点名称
        /// </summary>
        [SugarColumn(ColumnDescription = "起始点名称", IsNullable = true)]
        public string SourcePlaceName { get; set; }

        /// <summary>
        /// 目标点
        /// </summary>
        [SugarColumn(ColumnDescription = "目标点", IsNullable = true)]
        public int ToPlace { get; set; }

        /// <summary>
        /// 目标点名称
        /// </summary>
        [SugarColumn(ColumnDescription = "目标点名称", IsNullable = true)]
        public string ToPlaceName { get; set; }

        /// <summary>
        /// AGV返回结果
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV返回结果", IsNullable = true)]
        public string AgvResult { get; set; }

        /// <summary>
        /// AGV返回信息
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV返回信息", IsNullable = true)]
        public string AgvMsg { get; set; }

    }
}
