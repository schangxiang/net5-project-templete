﻿using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// Agv车辆
    /// </summary>
    public class Les_Agv : SimpleGeneralBusinessRoot<int>
    {

        /// <summary>
        /// 车辆名称
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "车辆名称")]
        public string AgvName { get; set; }

        /// <summary>
        /// 车辆描述
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "车辆描述",IsNullable =true)]
        public string AgvNameDesc { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "备注", IsNullable = true)]
        public string Remark { get; set; }

        /// <summary>
        /// 电量
        /// </summary>
        [SugarColumn(ColumnDescription = "电量", IsNullable = true)]
        public int? EnergyLevel { get; set; }

        /// <summary>
        /// 当前位置
        /// </summary>
        [SugarColumn(ColumnDescription = "当前位置", IsNullable = true)]
        public string CurrentPosition { get; set; }

        /// <summary>
        /// X坐标
        /// </summary>
        [SugarColumn(ColumnDescription = "X坐标", IsNullable = true)]
        public string XCoordinate { get; set; }

        /// <summary>
        /// Y坐标
        /// </summary>
        [SugarColumn(ColumnDescription = "Y坐标", IsNullable = true)]
        public string YCoordinate { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [SugarColumn(ColumnDescription = "状态", IsNullable = true)]
        public string State { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [SugarColumn(ColumnDescription = "状态", IsNullable = true)]
        public string StateName { get; set; }

        /// <summary>
        /// 当前执行的任务
        /// </summary>
        [SugarColumn(ColumnDescription = "当前执行的任务", IsNullable = true)]
        public string TransportOrder { get; set; }

    }
}
