﻿using System;
using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// Agv任务
    /// </summary>
    public class Les_AgvTask : GeneralBusinessRoot<string>
    {

        /// <summary>
        /// 任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "任务号")]
        public string TaskNo { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务ID
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务ID")]
        public string LesTaskId { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务号")]
        public string LesTaskNo { get; set; }


        /// <summary>
        /// 配料任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "配料任务号", IsNullable = true)]
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型")]
        public int TaskType { get; set; }

        /// <summary>
        /// 任务类型名称
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型名称")]
        public string TaskTypeName { get; set; }

        /// <summary>
        /// 任务描述
        /// </summary>
        [SugarColumn(ColumnDescription = "任务描述", IsNullable = true)]
        public string TaskDesc { get; set; }


        /// <summary>
        /// 叫料单号
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "叫料单号")]
        public string CallOrderNo { get; set; }

        #region 起始点


        /// <summary>
        /// 起始点
        /// </summary>
        [SugarColumn(ColumnDescription = "起始点")]
        public int SourcePlace { get; set; }

        /// <summary>
        /// 起始点名称
        /// </summary>
        [SugarColumn(ColumnDescription = "起始点名称")]
        public string SourcePlaceName { get; set; }


        /// <summary>
        /// 起始AGV站点
        /// </summary>
        [SugarColumn(ColumnDescription = "起始AGV站点")]
        public string SourceAgvStation { get; set; }

        /// <summary>
        /// 目标点
        /// </summary>
        [SugarColumn(ColumnDescription = "目标点")]
        public int ToPlace { get; set; }

        /// <summary>
        /// 目标点名称
        /// </summary>
        [SugarColumn(ColumnDescription = "目标点名称")]
        public string ToPlaceName { get; set; }

        /// <summary>
        /// 目标AGV站点
        /// </summary>
        [SugarColumn(ColumnDescription = "目标AGV站点")]
        public string ToPlaceAgvStation { get; set; }

        #endregion

        #region 物料相关

        /// <summary>
        /// 物料表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "物料表ID")]
        public string MaterialId { get; set; }


        /// <summary>
        /// 物料号
        /// </summary>
        [SugarColumn(ColumnDescription = "物料号", IsNullable = true)]
        public string MaterialCode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [SugarColumn(ColumnDescription = "物料名称", IsNullable = true)]
        public string MaterialName { get; set; }


        /// <summary>
        /// 货物数量
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "货物数量")]
        public decimal? CargoNum { get; set; }


        #endregion

        /// <summary>
        /// 表Les_Mater_V_Station的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "表Les_Mater_V_Station的ID", IsNullable = true)]
        public string MVSId { get; set; }

        /// <summary>
        /// 托盘号
        /// </summary>
        [SugarColumn(ColumnDescription = "托盘号", IsNullable = true)]
        public string SalverCode { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public int TaskStatus { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public string TaskStatusName { get; set; }

        /// <summary>
        /// 任务创建时间
        /// </summary>
        [SugarColumn(ColumnDescription = "任务创建时间")]
        public DateTime TaskCreateTime { get; set; }

        /// <summary>
        /// 下发时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "下发时间")]
        public DateTime? TaskIssueTime { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "完成时间")]
        public DateTime? TaskFinishTime { get; set; }


        #region AGV相关

        /// <summary>
        /// 分配车辆
        /// </summary>
        [SugarColumn(ColumnDescription = "分配车辆", IsNullable = true)]
        public string IntendedVehicle { get; set; }

        /// <summary>
        /// 执行车辆
        /// </summary>
        [SugarColumn(ColumnDescription = "执行车辆", IsNullable = true)]
        public string ProcessingVehicle { get; set; }

        /// <summary>
        /// 车辆描述
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "车辆描述", IsNullable = true)]
        public string AgvNameDesc { get; set; }

        /// <summary>
        /// AGV状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV状态", IsNullable = true)]
        public string AgvState { get; set; }

        /// <summary>
        /// AGV状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV状态", IsNullable = true)]
        public string AgvStateName { get; set; }



        /// <summary>
        /// AGV取货状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV取货状态", IsNullable = true)]
        public string AgvTakeCargoState { get; set; }

        /// <summary>
        /// AGV取货状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV取货状态", IsNullable = true)]
        public string AgvTakeCargoStateName { get; set; }

        #endregion

        #region 所属工序

        /// <summary>
        /// 所属工序，在码表中配置,码表项的ID
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工序", IsNullable = true)]
        public string AllowProcedure { get; set; }

        /// <summary>
        /// 所属工序，在码表中配置,码表项的编号
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工序编号", IsNullable = true)]
        public string AllowProcedure_CodeItem_Code { get; set; }

        /// <summary>
        /// 所属工序，在码表中配置,码表项的名称
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工序名称", IsNullable = true)]
        public string AllowProcedure_CodeItem_Name { get; set; }

        #endregion

        #region 异常处理

        /// <summary>
        /// 是否人工处理
        /// </summary>
        [SugarColumn(ColumnDescription = "是否人工处理", IsNullable = true)]
        public bool? IsManualHandling { get; set; }


        /// <summary>
        /// 人工处理人
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理人", IsNullable = true)]
        public string ManualHandlingUser { get; set; }

        /// <summary>
        /// 人工处理说明
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理说明", IsNullable = true)]
        public string ManualHandlingRemark { get; set; }

        /// <summary>
        /// 人工处理时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "人工处理时间")]
        public DateTime? ManualHandlingTime { get; set; }

        #endregion


        /// <summary>
        /// 执行顺序
        /// </summary>
        [SugarColumn(ColumnDescription = "执行顺序")]
        public int ExecuteSequence { get; set; } = 1;


        /// <summary>
        /// AGV任务需要的双循环标记
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV任务需要的双循环标记", IsNullable = true)]
        public string Agv_SeqName { get; set; }
    }
}
