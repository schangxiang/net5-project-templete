﻿using SqlSugar;
using System;

namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// Agv任务阶段
    /// </summary>
    public class Les_AgvTaskPhase : MoreSimpleGeneralBusinessRoot<string>
    {

        /// <summary>
        /// AGV任务表的ID
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "AGV任务表的ID")]
        public string AgvTaskId { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "任务号")]
        public string TaskNo { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务ID
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务ID")]
        public string LesTaskId { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务号")]
        public string LesTaskNo { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型")]
        public int TaskType { get; set; }

        /// <summary>
        /// 任务类型名称
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型名称")]
        public string TaskTypeName { get; set; }


        /// <summary>
        /// 任务状态
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public int TaskStatus { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public string TaskStatusName { get; set; }

        #region 起始点


        /// <summary>
        /// 起始点
        /// </summary>
        [SugarColumn(ColumnDescription = "起始点")]
        public int SourcePlace { get; set; }

        /// <summary>
        /// 起始点名称
        /// </summary>
        [SugarColumn(ColumnDescription = "起始点名称")]
        public string SourcePlaceName { get; set; }


        /// <summary>
        /// 起始AGV站点
        /// </summary>
        [SugarColumn(ColumnDescription = "起始AGV站点")]
        public string SourceAgvStation { get; set; }

        /// <summary>
        /// 目标点
        /// </summary>
        [SugarColumn(ColumnDescription = "目标点")]
        public int ToPlace { get; set; }

        /// <summary>
        /// 目标点名称
        /// </summary>
        [SugarColumn(ColumnDescription = "目标点名称")]
        public string ToPlaceName { get; set; }

        /// <summary>
        /// 目标AGV站点
        /// </summary>
        [SugarColumn(ColumnDescription = "目标AGV站点")]
        public string ToPlaceAgvStation { get; set; }

        #endregion

        #region AGV相关

        /// <summary>
        /// 分配车辆
        /// </summary>
        [SugarColumn(ColumnDescription = "分配车辆", IsNullable = true)]
        public string IntendedVehicle { get; set; }

        /// <summary>
        /// 执行车辆
        /// </summary>
        [SugarColumn(ColumnDescription = "执行车辆", IsNullable = true)]
        public string ProcessingVehicle { get; set; }

        /// <summary>
        /// AGV状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV状态", IsNullable = true)]
        public string AgvState { get; set; }

        /// <summary>
        /// AGV状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV状态", IsNullable = true)]
        public string AgvStateName { get; set; }

        /// <summary>
        /// AGV取货状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV取货状态", IsNullable = true)]
        public string AgvTakeCargoState { get; set; }

        /// <summary>
        /// AGV取货状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV取货状态", IsNullable = true)]
        public string AgvTakeCargoStateName { get; set; }

        #endregion

        #region 异常处理

        /// <summary>
        /// 是否人工处理
        /// </summary>
        [SugarColumn(ColumnDescription = "是否人工处理", IsNullable = true)]
        public bool? IsManualHandling { get; set; }


        /// <summary>
        /// 人工处理人
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理人", IsNullable = true)]
        public string ManualHandlingUser { get; set; }

        /// <summary>
        /// 人工处理说明
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理说明", IsNullable = true)]
        public string ManualHandlingRemark { get; set; }

        /// <summary>
        /// 人工处理时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "人工处理时间")]
        public DateTime? ManualHandlingTime { get; set; }

        #endregion
    }
}
