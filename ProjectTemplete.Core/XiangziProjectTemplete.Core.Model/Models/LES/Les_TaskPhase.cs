﻿using SqlSugar;
using System;

namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// XiangziProjectTemplete任务阶段
    /// </summary>
    public class Les_TaskPhase : MoreSimpleGeneralBusinessRoot<string>
    {
        /// <summary>
        /// XiangziProjectTemplete任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务号")]
        public string LesTaskNo { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务ID
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务ID")]
        public string LesTaskId { get; set; }

        #region 任务状态

        /// <summary>
        /// 任务状态,枚举 LesTaskStatusEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public int LesTaskStatus { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public string LesTaskStatusName { get; set; }

        #endregion 

        /// <summary>
        /// 任务类型
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型")]
        public int TaskType { get; set; }

        /// <summary>
        /// 任务类型名称
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型名称")]
        public string TaskTypeName { get; set; }

        #region AGV相关

        /// <summary>
        /// 分配车辆
        /// </summary>
        [SugarColumn(ColumnDescription = "分配车辆", IsNullable = true)]
        public string IntendedVehicle { get; set; }

        /// <summary>
        /// 执行车辆
        /// </summary>
        [SugarColumn(ColumnDescription = "执行车辆", IsNullable = true)]
        public string ProcessingVehicle { get; set; }

        /// <summary>
        /// AGV状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV状态", IsNullable = true)]
        public string AgvState { get; set; }

        /// <summary>
        /// AGV状态
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV状态", IsNullable = true)]
        public string AgvStateName { get; set; }

        #endregion


        #region 异常处理

        /// <summary>
        /// 是否人工处理
        /// </summary>
        [SugarColumn(ColumnDescription = "是否人工处理", IsNullable = true)]
        public bool? IsManualHandling { get; set; }


        /// <summary>
        /// 人工处理人
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理人", IsNullable = true)]
        public string ManualHandlingUser { get; set; }

        /// <summary>
        /// 人工处理说明
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理说明", IsNullable = true)]
        public string ManualHandlingRemark { get; set; }

        /// <summary>
        /// 人工处理时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "人工处理时间")]
        public DateTime? ManualHandlingTime { get; set; }

        #endregion

    }
}
