﻿using System;
using SqlSugar;

//注意：要想DBFirst，命名空间必须是 XiangziProjectTemplete.Core.Model.Models
namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 出库记录
    /// </summary>
    public class Les_OutStockRecord : Les_StockInfo
    {
        /// <summary>
        /// 出库时间
        /// </summary>
        [SugarColumn(ColumnDescription = "出库时间")]
        public DateTime OutStockDate { get; set; }
        
    }
}
