﻿using System;
using SqlSugar;

//注意：要想DBFirst，命名空间必须是 XiangziProjectTemplete.Core.Model.Models
namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 分拣入库任务
    /// </summary>
    public class Les_PickInStockTask : SimpleGeneralBusinessRoot<string>
    {
        /// <summary>
        /// 是否人工分拣入库
        /// </summary>
        [SugarColumn(ColumnDescription = "是否人工分拣入库")]
        public bool IsPersonPickInStore { get; set; }

        /// <summary>
        /// 分拣入库任务号,有系统自己生成的随机任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "分拣入库任务号")]
        public string PickInStockTaskNo { get; set; }

        /// <summary>
        /// 配料任务号，操作工输入的分拣使用的任务号！
        /// </summary>
        [SugarColumn(ColumnDescription = "配料任务号")]
        public string BurdenWorkNo { get; set; }


        #region 任务状态

        /// <summary>
        /// 任务状态,枚举 LesTaskStatusEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public int LesTaskStatus { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        [SugarColumn(ColumnDescription = "任务状态")]
        public string LesTaskStatusName { get; set; }

        #endregion 


        /// <summary>
        /// 是否分拣完成
        /// </summary>
        [SugarColumn(ColumnDescription = "是否分拣完成")]
        public bool IsPickFinish { get; set; }


        /// <summary>
        /// 关联的库位绑定表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "关联的库位绑定表ID", IsNullable = true)]
        public string MVSId { get; set; }



        /// <summary>
        /// 关联的Base_Material表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "关联的物料信息表ID", IsNullable = true)]
        public string MaterialId { get; set; }

        /// <summary>
        /// 所属工序
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工序")]
        public string AllowProcedure { get; set; }

        /// <summary>
        /// 起始站点ID
        /// </summary>
        [SugarColumn(ColumnDescription = "起始站点ID")]
        public int StationId { get; set; }


        /// <summary>
        /// 目标站点ID
        /// </summary>
        [SugarColumn(ColumnDescription = "目标站点ID")]
        public int ToStationId { get; set; } = 0;



        /// <summary>
        /// 项目名称
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "项目名称")]
        public string ProjectName { get; set; }

        /// <summary>
        /// 跟踪号
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "跟踪号")]
        public string TrackingNo { get; set; }

        /// <summary>
        /// 物料大类,在码表中配置,是一个值
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "物料大类")]
        public string MaterialCodeItemId { get; set; }





        #region 物料冗余字段


        /// <summary>
        /// 物料大类编号,在码表中配置
        /// </summary>
        [SugarColumn(ColumnDescription = "物料大类编号", IsNullable = true)]
        public string MaterialCode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [SugarColumn(ColumnDescription = "物料名称", IsNullable = true)]
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料分类,枚举 MaterialClassifyEnum
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料分类")]
        public int MaterialClassify { get; set; }

        /// <summary>
        /// 物料分类,枚举 MaterialClassifyEnum
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "物料分类")]
        public string MaterialClassifyName { get; set; }

        /// <summary>
        /// 物料类型，枚举 CargoTypeEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型", IsNullable = true)]
        public int CargoType { get; set; }

        /// <summary>
        /// 物料类型，枚举 CargoTypeEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "物料类型", IsNullable = true)]
        public string CargoTypeName { get; set; }

        #endregion


        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "备注")]
        public string Remark { get; set; }

        /// <summary>
        /// 货物数量
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "货物数量")]
        public int? CargoNum { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "单位")]
        public string Unit { get; set; }


        /// <summary>
        /// 重量
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "重量")]
        public decimal? CargoWeight { get; set; }


        /// <summary>
        /// 入缓存库时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "入缓存库时间")]
        public DateTime? InStockDate { get; set; }


        /// <summary>
        /// 下发时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "下发时间")]
        public DateTime? TaskIssueTime { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "完成时间")]
        public DateTime? TaskFinishTime { get; set; }

        #region 异常处理

        /// <summary>
        /// 是否人工处理
        /// </summary>
        [SugarColumn(ColumnDescription = "是否人工处理", IsNullable = true)]
        public bool? IsManualHandling { get; set; }


        /// <summary>
        /// 人工处理人
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理人", IsNullable = true)]
        public string ManualHandlingUser { get; set; }

        /// <summary>
        /// 人工处理说明
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理说明", IsNullable = true)]
        public string ManualHandlingRemark { get; set; }

        /// <summary>
        /// 人工处理时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "人工处理时间")]
        public DateTime? ManualHandlingTime { get; set; }

        #endregion


        #region AGV任务有关

        /// <summary>
        /// AGV任务
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV任务", IsNullable = true)]
        public string AgvTaskNo { get; set; }



        #endregion


        #region 备料通知

        /// <summary>
        /// 备料通知结果
        /// </summary>
        [SugarColumn(ColumnDescription = "备料通知结果", IsNullable = true)]
        public string PreMatResult { get; set; }

        /// <summary>
        /// 备料通知时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "备料通知时间")]
        public DateTime? PreMatResultTime { get; set; }

        #endregion

    }
}
