﻿using SqlSugar;

//注意：要想DBFirst，命名空间必须是 XiangziProjectTemplete.Core.Model.Models
namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 出入库记录
    /// </summary>
    public class Les_InOutStockRecord : MoreSimpleGeneralBusinessRoot<string>
    {
        /// <summary>
        /// 出入库类型,1 入库 2 出库
        /// </summary>
        [SugarColumn(ColumnDescription = "出入库类型")]
        public int InOutStockType { get; set; }

        /// <summary>
        /// 出入库类型,1 入库 2 出库
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "出入库类型")]
        public string InOutStockTypeName { get; set; }



        #region AGV


        /// <summary>
        /// 任务类型
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型", IsNullable = true)]
        public int AGV_TaskType { get; set; }

        /// <summary>
        /// 任务类型名称
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型名称", IsNullable = true)]
        public string AGV_TaskTypeName { get; set; }

        /// <summary>
        /// 任务描述
        /// </summary>
        [SugarColumn(ColumnDescription = "任务描述", IsNullable = true)]
        public string AGV_TaskDesc { get; set; }

        /// <summary>
        /// 执行车辆
        /// </summary>
        [SugarColumn(ColumnDescription = "执行车辆", IsNullable = true)]
        public string ProcessingVehicle { get; set; }

        /// <summary>
        /// 车辆描述
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "车辆描述", IsNullable = true)]
        public string AgvNameDesc { get; set; }

        #endregion

        #region 业务

        /// <summary>
        /// 配料任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "配料任务号", IsNullable = true)]
        public string BurdenWorkNo { get; set; }

        #endregion

        #region 站点

        /// <summary>
        /// 站点ID
        /// </summary>
        [SugarColumn(ColumnDescription = "站点ID")]
        public int StationId { get; set; }

        /// <summary>
        /// 站点编号
        /// </summary>
        [SugarColumn(ColumnDescription = "站点编号")]
        public string StationCode { get; set; }

        /// <summary>
        /// 站点名称
        /// </summary>
        [SugarColumn(ColumnDescription = "站点名称")]
        public string StationName { get; set; }


        #endregion

        #region 物料

        /// <summary>
        /// 物料号
        /// </summary>
        [SugarColumn(ColumnDescription = "物料号", IsNullable = true)]
        public string MaterialCode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [SugarColumn(ColumnDescription = "物料名称", IsNullable = true)]
        public string MaterialName { get; set; }

        #endregion


    }
}
