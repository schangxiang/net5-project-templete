﻿using System;
using SqlSugar;

//注意：要想DBFirst，命名空间必须是 XiangziProjectTemplete.Core.Model.Models
namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 库存表
    /// </summary>
    public class Les_StockInfo : GeneralBusinessRoot<int>
    {

        /// <summary>
        /// 站点ID
        /// </summary>
        [SugarColumn(ColumnDescription = "站点ID")]
        public int StationId { get; set; }

        /// <summary>
        /// 货物类型
        /// </summary>
        [SugarColumn(ColumnDescription = "货物类型")]
        public int CargoType { get; set; }

        /// <summary>
        /// 货物类型名称，不是数据库字段
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string CargoTypeName { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "项目名称")]
        public string ProjectName { get; set; }

        /// <summary>
        /// 跟踪号
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "跟踪号")]
        public string TrackingNo { get; set; }

        /// <summary>
        /// 托盘号
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "托盘号")]
        public string SalverNo { get; set; }

        /// <summary>
        /// 物料号
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "物料号")]
        public string CargoCode { get; set; }

        /// <summary>
        /// 货物名称
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "货物名称")]
        public string CargoName { get; set; }

        /// <summary>
        /// 货物数量
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "货物数量")]
        public int CargoNum { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "重量")]
        public decimal CargoWeight { get; set; }

        /// <summary>
        /// 型号
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 50, ColumnDescription = "型号")]
        public string CargoModel { get; set; }

        /// <summary>
        /// 图纸号
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 50, ColumnDescription = "图纸号")]
        public string DrawingNo { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "有效期")]
        public DateTime? ValidityDate { get; set; }

        /// <summary>
        /// 入库时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "入库时间")]
        public DateTime InStockDate { get; set; }

    }
}
