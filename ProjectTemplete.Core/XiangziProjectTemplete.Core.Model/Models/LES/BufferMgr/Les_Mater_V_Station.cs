﻿using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 物料和站点绑定信息
    /// </summary>
    public class Les_Mater_V_Station : GeneralBusinessRoot<string>
    {
        /// <summary>
        /// 物料表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "物料表ID")]
        public string MaterialId { get; set; }

        /// <summary>
        /// 货物名称
        /// </summary>
        [SugarColumn(ColumnDescription = "货物名称", IsNullable = true)]
        public string MaterialName { get; set; }



        /// <summary>
        /// 站点表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "站点表ID")]
        public int StationId { get; set; }

        /// <summary>
        /// 站点表编号
        /// </summary>
        [SugarColumn(ColumnDescription = "站点表编号", IsNullable = true)]
        public string StationCode { get; set; }

        /// <summary>
        /// AGV车辆名称
        /// </summary>
        [SugarColumn(ColumnDescription = "AGV车辆名称", IsNullable = true)]
        public string AgvName { get; set; }

        /// <summary>
        /// 绑定状态，枚举 M_V_S_StateEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "绑定状态")]
        public int State { get; set; }


        /// <summary>
        /// 绑定状态，枚举 M_V_S_StateEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "绑定状态", IsNullable = true)]
        public string StateName { get; set; }


        #region 记录历史内容

        /// <summary>
        /// 上一个站点表ID
        /// </summary>
        [SugarColumn(ColumnDescription = "上一个站点表ID", IsNullable = true)]
        public int Old_StationId { get; set; }

        /// <summary>
        /// 上一个站点表编号
        /// </summary>
        [SugarColumn(ColumnDescription = "上一个站点表编号", IsNullable = true)]
        public string Old_StationCode { get; set; }

        /// <summary>
        /// 上一个AGV车辆名称
        /// </summary>
        [SugarColumn(ColumnDescription = "上一个AGV车辆名称", IsNullable = true)]
        public string Old_AgvName { get; set; }

        /// <summary>
        /// 上一个绑定状态，枚举 M_V_S_StateEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "上一个绑定状态", IsNullable = true)]
        public int Old_State { get; set; }

        /// <summary>
        /// 上一个绑定状态，枚举 M_V_S_StateEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "上一个绑定状态", IsNullable = true)]
        public string Old_StateName { get; set; }

        #endregion

    }
}
