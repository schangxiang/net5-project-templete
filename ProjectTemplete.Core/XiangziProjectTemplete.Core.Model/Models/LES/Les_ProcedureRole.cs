﻿using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 角色工序绑定
    /// </summary>
    public class Les_ProcedureRole : GeneralBusinessRoot<int>
    {

        /// <summary>
        /// 角色ID
        /// </summary>
        [SugarColumn(ColumnDescription = "角色ID")]
        public int RoleId { get; set; }



        /// <summary>
        /// 是否全部工序权限
        /// </summary>
        [SugarColumn(ColumnDescription = "是否全部工序权限")]
        public bool IsHasAllProcedure { get; set; }

        /// <summary>
        /// 工序ID的合集，用逗号分隔
        /// </summary>
        [SugarColumn(ColumnDescription = "工序", IsNullable = true)]
        public string AllowProcedure { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "备注")]
        public string Remark { get; set; }


    }
}
