﻿using System;
using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 回库搬运
    /// </summary>
    public class Les_BackToStore : GeneralBusinessRoot<int>
    {
        /// <summary>
        /// 任务类型
        /// </summary>
        [SugarColumn(ColumnDescription = "任务类型")]
        public int TaskType { get; set; }

        /// <summary>
        /// 工位
        /// </summary>
        [SugarColumn(Length = 100, ColumnDescription = "工位")]
        public string Station { get; set; }

        /// <summary>
        /// 起始位置
        /// </summary>
        [SugarColumn(Length = 100, ColumnDescription = "起始位置")]
        public string SourceNo { get; set; }

        /// <summary>
        /// 目标位置
        /// </summary>
        [SugarColumn(Length = 100, ColumnDescription = "目标位置")]
        public string ToNo { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public int Status { get; set; }


        /// <summary>
        /// 制单时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "制单时间")]
        public DateTime? TaskCrateTime { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "完成时间")]
        public DateTime? TaskFinishTime { get; set; }

    }
}
