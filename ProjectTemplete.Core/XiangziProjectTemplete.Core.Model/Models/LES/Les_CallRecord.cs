﻿using System;
using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 呼叫任务记录
    /// </summary>
    public class Les_CallRecord : GeneralBusinessRoot<string>
    {

        /// <summary>
        /// 呼叫订单ID
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫订单ID")]
        public int CallOrderId { get; set; }

        /// <summary>
        /// 物料号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "物料号")]
        public string MaterielNo { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "物料名称")]
        public string MaterielName { get; set; }

        /// <summary>
        /// 型号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "型号")]
        public string MaterielType { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        [SugarColumn(ColumnDescription = "重量")]
        public decimal MaterielWeight { get; set; }

        /// <summary>
        /// 呼叫数量
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫数量")]
        public int CallNum { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public int Status { get; set; }

        /// <summary>
        /// 下发时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "下发时间")]
        public DateTime? TaskIssueTime { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "完成时间")]
        public DateTime? TaskFinishTime { get; set; }

    }
}
