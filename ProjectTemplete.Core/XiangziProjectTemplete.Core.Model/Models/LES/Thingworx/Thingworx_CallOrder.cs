﻿using System;
using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// Thingworx发送的呼叫任务
    /// </summary>
    public class Thingworx_CallOrder : GeneralBusinessRoot<string>
    {
        #region Thingworx传递的参数

        /// <summary>
        /// 呼叫模式
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫模式", IsNullable = true)]
        public int LesTaskMode { get; set; }


        /// <summary>
        /// 呼叫模式
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫模式", IsNullable = true)]
        public string LesTaskModeName { get; set; }

        /// <summary>
        /// 工序编号
        /// </summary>
        [SugarColumn(ColumnDescription = "工序")]
        public string Procedure { get; set; }


        /// <summary>
        /// 工位号
        /// </summary>
        [SugarColumn(ColumnDescription = "工位号")]
        public string WorkStationNo { get; set; }

        /// <summary>
        /// 点位
        /// </summary>
        [SugarColumn(ColumnDescription = "点位")]
        public string StationCode { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "任务号")]
        public string BurdenWorkNo { get; set; }


        /// <summary>
        /// 物料
        /// </summary>
        [SugarColumn(ColumnDescription = "物料")]
        public string Material { get; set; }

        #endregion

        #region 业务字段

        /// <summary>
        /// 状态，枚举 LesTaskStatusEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public int Status { get; set; }

        /// <summary>
        /// 状态，枚举 LesTaskStatusEnum
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public string StatusName { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务ID
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务ID", IsNullable = true)]
        public string LesTaskId { get; set; }


        /// <summary>
        /// 呼叫任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "呼叫任务号")]
        public string CallOrderTaskNo { get; set; }

        /// <summary>
        /// 下发时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "下发时间")]
        public DateTime? TaskIssueTime { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "完成时间")]
        public DateTime? TaskFinishTime { get; set; }



        /// <summary>
        /// 是否通知成功
        /// </summary>
        [SugarColumn(ColumnDescription = "是否通知成功", IsNullable = true)]
        public bool NoticeResult { get; set; }

        /// <summary>
        /// 通知返回消息
        /// </summary>
        [SugarColumn(ColumnDescription = "通知返回消息", IsNullable = true)]
        public string ThingworxReturn { get; set; }

        /// <summary>
        /// 通知时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "通知时间")]
        public DateTime? NoticeTime { get; set; }


        #endregion


    }
}
