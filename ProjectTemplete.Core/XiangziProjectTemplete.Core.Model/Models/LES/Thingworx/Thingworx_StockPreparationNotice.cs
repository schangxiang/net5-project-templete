﻿using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// Thingworx 备料通知表
    /// </summary>
    public class Thingworx_StockPreparationNotice : MoreSimpleGeneralBusinessRoot<string>
    {
        #region Thingworx传递的参数


        /// <summary>
        /// 工序编号
        /// </summary>
        [SugarColumn(ColumnDescription = "工序编号")]
        public string Procedure { get; set; }

        /// <summary>
        /// 工序
        /// </summary>
        [SugarColumn(ColumnDescription = "工序")]
        public string ProcedureName { get; set; }


        /// <summary>
        /// 任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "任务号")]
        public string BurdenWorkNo { get; set; }


        /// <summary>
        /// 物料编号
        /// </summary>
        [SugarColumn(ColumnDescription = "物料编号")]
        public string Material { get; set; }


        /// <summary>
        /// 物料
        /// </summary>
        [SugarColumn(ColumnDescription = "物料")]
        public string MaterialName { get; set; }

        /// <summary>
        /// 备料状态 1：备料完成 2：备料取消
        /// </summary>
        [SugarColumn(ColumnDescription = "备料状态")]
        public string Status { get; set; }

        /// <summary>
        /// 备料状态 1：备料完成 2：备料取消
        /// </summary>
        [SugarColumn(ColumnDescription = "备料状态")]
        public string StatusName { get; set; }

        #endregion

        #region 业务字段

        /// <summary>
        /// LES_PickInStockTask的任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "LES_PickInStockTask的任务号", IsNullable = true)]
        public string PickInStockTaskID { get; set; }




        #endregion


        /// <summary>
        /// Thingworx返回结果
        /// </summary>
        [SugarColumn(ColumnDescription = "Thingworx返回结果", IsNullable = true)]
        public string ThingworxResult { get; set; }


    }
}
