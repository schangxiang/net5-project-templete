﻿using System;
using SqlSugar;


namespace XiangziProjectTemplete.Core.Model.Models
{
    /// <summary>
    /// 人工处理数据记录表
    /// </summary>
    public class Les_PersonHandlerRecord : GeneralBusinessRoot<string>
    {
        #region 公共字段

        /// <summary>
        /// 关键字1
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "关键字1")]
        public string Key1 { get; set; }

        /// <summary>
        /// 关键字2
        /// </summary>
        [SugarColumn(IsNullable = true, Length = 500, ColumnDescription = "关键字2")]
        public string Key2 { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [SugarColumn(ColumnDescription = "类型", IsNullable = true)]
        public string HandlerType { get; set; }


        /// <summary>
        /// 人工处理说明
        /// </summary>
        [SugarColumn(ColumnDescription = "人工处理说明", Length = 1500, IsNullable = true)]
        public string ManualHandlerRemark { get; set; }

        /// <summary>
        /// 人工处理人员
        /// </summary>
        [SugarColumn(Length = 100, ColumnDescription = "人工处理人员", IsNullable = true)]
        public string ManualHandlerUser { get; set; }

        /// <summary>
        /// 人工处理时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "人工处理时间")]
        public DateTime? ManualHandlerTime { get; set; }

        #endregion

        #region 个性字段

        /// <summary>
        /// 配料任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "配料任务号", IsNullable = true)]
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 所属工序
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工序", IsNullable = true)]
        public string AllowProcedure { get; set; }


        /// <summary>
        /// 物料大类的码表项ID,在码表中配置
        /// </summary>
        [SugarColumn(ColumnDescription = "物料大类的码表项ID", IsNullable = true)]
        public string MaterialCodeItemId { get; set; }

        /// <summary>
        /// 物料大类,在码表中配置
        /// </summary>
        [SugarColumn(ColumnDescription = "物料大类", IsNullable = true)]
        public string MaterialCode { get; set; }

        /// <summary>
        /// 货物名称
        /// </summary>
        [SugarColumn(ColumnDescription = "货物名称", IsNullable = true)]
        public string MaterialName { get; set; }


        /// <summary>
        /// 任务号
        /// </summary>
        [SugarColumn(Length = 500, ColumnDescription = "任务号", IsNullable = true)]
        public string TaskNo { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务ID
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务ID", IsNullable = true)]
        public string LesTaskId { get; set; }

        /// <summary>
        /// XiangziProjectTemplete任务号
        /// </summary>
        [SugarColumn(ColumnDescription = "XiangziProjectTemplete任务号", IsNullable = true)]
        public string LesTaskNo { get; set; }

        /// <summary>
        /// 站点编号
        /// </summary>
        [SugarColumn(ColumnDescription = "站点编号", IsNullable = true)]
        public string StationCode { get; set; }

        /// <summary>
        /// 站点名称
        /// </summary>
        [SugarColumn(ColumnDescription = "站点名称", IsNullable = true)]
        public string StationName { get; set; }

        #endregion
    }
}
