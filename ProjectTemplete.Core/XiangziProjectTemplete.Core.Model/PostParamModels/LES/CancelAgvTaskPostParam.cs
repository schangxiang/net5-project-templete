﻿namespace XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete
{
    /// <summary>
    /// 取消AGV任务的请求参数类
    /// </summary>
    public class CancelAgvTaskPostParam
    {

        /// <summary>
        /// XiangziProjectTemplete的任务号
        /// </summary>
        public string LesTaskId { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 操作说明
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 业务表
        /// </summary>
        public string BussinessTableName { get; set; }

    }
}
