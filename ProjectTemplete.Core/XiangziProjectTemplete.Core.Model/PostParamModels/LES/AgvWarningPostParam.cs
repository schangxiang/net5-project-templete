﻿using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete
{
    /// <summary>
    /// AGV报警信息的请求参数类
    /// </summary>
    public class AgvWarningPostParam
    {

        /// <summary>
        /// 车辆名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 报警代码集合
        /// </summary>
        public List<int> error { get; set; }

        /// <summary>
        /// 报警位置
        /// </summary>
        public string currentPosition { get; set; }

    }
}
