﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete
{
    /// <summary>
    /// 分拣出库和回库搬运的请求参数类
    /// </summary>
    public class LES_SortOutStoreTaskPostParam
    {

        /// <summary>
        /// 叫料单号
        /// </summary>
        public string CallOrderNo { get; set; }

        /// <summary>
        /// 起始点
        /// </summary>
        public int SourcePlace { get; set; }

        /// <summary>
        /// 起始点名称
        /// </summary>
        public string SourcePlaceName { get; set; }

        /// <summary>
        /// 目标点
        /// </summary>
        public int ToPlace { get; set; }

        /// <summary>
        /// 目标点名称
        /// </summary>
        public string ToPlaceName { get; set; }


        /// <summary>
        /// 工件号
        /// </summary>
        public string MaterialCode { get; set; }

        /// <summary>
        /// 工件名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 托盘号
        /// </summary>
        public string SalverCode { get; set; }
    }
}
