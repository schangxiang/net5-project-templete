﻿namespace XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete
{
    /// <summary>
    /// 缺料呼叫请求参数类
    /// </summary>
    public class CallOrderPostParam
    {
        /// <summary>
        /// 站点ID
        /// </summary>
        public int StationId { get; set; }



        /// <summary>
        /// 货物类型
        /// </summary>
        public int CargoType { get; set; }


        /// <summary>
        /// 任务号
        /// </summary>
        public string WorkNo { get; set; }

        /// <summary>
        /// 所属工序
        /// </summary>
        public string AllowProcedure { get; set; }


        /// <summary>
        /// 物料大类,在码表中配置,是一个值
        /// </summary>
        public string MaterialCodeItemId { get; set; }

        ///// <summary>
        ///// 物料大类,在码表中配置,是一个值
        ///// </summary>
        //public string MaterialCode { get; set; }

        ///// <summary>
        ///// 货物名称
        ///// </summary>
        //public string MaterialName { get; set; }

        /// <summary>
        /// 物料类型,枚举 MaterialTypeEnum
        /// </summary>
        public int MaterialType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }


        /// <summary>
        /// 货物数量
        /// </summary>
        public int CargoNum { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        public decimal CargoWeight { get; set; }

    }
}
