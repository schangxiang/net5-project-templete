﻿namespace XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete
{
    /// <summary>
    /// 清理库存的请求参数类
    /// </summary>
    public class ClearStoreByStationPostParam
    {

        /// <summary>
        /// 站点
        /// </summary>
        public int StationId { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 操作说明
        /// </summary>
        public string Remark { get; set; }

    }
}
