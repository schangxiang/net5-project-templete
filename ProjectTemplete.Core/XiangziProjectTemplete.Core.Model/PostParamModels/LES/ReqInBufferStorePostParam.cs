﻿using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete
{
    /// <summary>
    /// 准备入缓存库请求参数类
    /// </summary>
    public class ReqInBufferStorePostParam
    {

        /// <summary>
        /// 工作号
        /// </summary>
        public string WorkNo { get; set; }

        /// <summary>
        /// 工位号
        /// </summary>
        public int StationNo { get; set; }

        /// <summary>
        /// 工序号
        /// </summary>
        public string ProcessNo { get; set; }


        /// <summary>
        /// 班别
        /// </summary>
        public string shift { get; set; }


        public List<InBufferStoreMats> Mats { get; set; }


    }

    public class InBufferStoreMats
    {
        public string MaterialCode { get; set; }

        public string MaterialName { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Qty { get; set; }

    }
}
