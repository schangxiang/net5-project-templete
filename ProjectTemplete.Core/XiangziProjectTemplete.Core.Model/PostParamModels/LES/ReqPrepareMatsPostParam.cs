﻿namespace XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete
{
    /// <summary>
    /// PDSS请求备料请求参数类
    /// </summary>
    public class ReqPrepareMatsPostParam
    {

        /// <summary>
        /// 工作号
        /// </summary>
        public string WorkNo { get; set; }

        /// <summary>
        /// 工位号
        /// </summary>
        public int StationNo { get; set; }

        /// <summary>
        /// 工序号
        /// </summary>
        public string ProcessNo { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Qty { get; set; }

        /// <summary>
        /// 班别
        /// </summary>
        public string shift { get; set; }


    }
}
