﻿namespace XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete
{
    /// <summary>
    /// 增加库存的请求参数类
    /// </summary>
    public class AddStoreByStationPostParam
    {

        /// <summary>
        /// 站点
        /// </summary>
        public int StationId { get; set; }

        /// <summary>
        /// 物料码表中的编号
        /// </summary>
        public string CodeItemsByCode { get; set; }

        /// <summary>
        /// 生产任务号
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 操作说明
        /// </summary>
        public string Remark { get; set; }

    }
}
