﻿using XiangziProjectTemplete.Core.Model.Models;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.PostParamModels.XiangziProjectTemplete
{
    /// <summary>
    /// 入库请求参数类
    /// </summary>
    public class AddEditStationPostParam:Base_Station
    {
        /// <summary>
        /// 能放的物料大类集合
        /// </summary>
        public List<int[]> BigMaterialStations { get; set; }
    }
}
