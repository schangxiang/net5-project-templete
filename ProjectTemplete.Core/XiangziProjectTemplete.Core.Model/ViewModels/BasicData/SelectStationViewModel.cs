﻿using XiangziProjectTemplete.Core.Model.Models;

namespace XiangziProjectTemplete.Core.Model.ViewModels.BasicData
{
    /// <summary>
    /// 下拉框的站点列表，用于下拉框
    /// </summary>
    public class SelectStationViewModel : Base_Station
    {
        /// <summary>
        /// 是否不可用，包括（锁定，有货、禁用）
        /// </summary>
        public bool Disabled { get; set; } = false;

        /// <summary>
        /// 是否不可用的说明
        /// </summary>
        public string DisabledMsg { get; set; } = string.Empty;

    }
}
