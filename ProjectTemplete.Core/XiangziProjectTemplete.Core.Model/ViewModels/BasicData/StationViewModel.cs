﻿using XiangziProjectTemplete.Core.Model.Models;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ViewModels.BasicData
{
    public class StationViewModel : Base_Station
    {
        /// <summary>
        /// 能放的物料大类集合
        /// </summary>
        public List<int[]> BigMaterialStations { get; set; }


        /// <summary>
        /// 能放的物料大类集合
        /// </summary>
        public string Str_BigMaterialStations { get; set; }

        /// <summary>
        /// 所属工序名称
        /// </summary>
        public string AllowProcedureName { get; set; }

        /// <summary>
        /// 所属产线名称
        /// </summary>
        public string AllowLineName { get; set; }

        /// <summary>
        /// 工位类型编号，表Base_CodeItems的Code
        /// </summary>
        public string PlaceTypeCode { get; set; }

        /// <summary>
        /// 工位类型，表Base_CodeItems的Name
        /// </summary>
        public string PlaceTypeName { get; set; }

        /// <summary>
        /// 库存区域名称
        /// </summary>
        public string StoreAreaName { get; set; }

    }
}
