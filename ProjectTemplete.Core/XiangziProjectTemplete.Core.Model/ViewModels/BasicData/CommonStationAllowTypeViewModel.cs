﻿namespace XiangziProjectTemplete.Core.Model.ViewModels.BasicData
{
    /// <summary>
    /// 站点有关的通用的类型查询类
    /// </summary>
    public class CommonStationAllowTypeViewModel
    {
        /// <summary>
        /// 所属工序名称
        /// </summary>
        public string AllowProcedureName { get; set; }

        /// <summary>
        /// 所属产线名称
        /// </summary>
        public string AllowLineName { get; set; }

        /// <summary>
        /// 工位类型编号，表Base_CodeItems的Code
        /// </summary>
        public string PlaceTypeCode { get; set; }

        /// <summary>
        /// 工位类型，表Base_CodeItems的Name
        /// </summary>
        public string PlaceTypeName { get; set; }

        /// <summary>
        /// 库存区域名称
        /// </summary>
        public string StoreAreaName { get; set; }

    }
}
