﻿using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ViewModels
{
    /// <summary>
    /// Select控件显示的组合
    /// </summary>
    public class SelectViewModel: SelectViewModelCommon
    {

        /// <summary>
        /// 值
        /// </summary>
        public int value { get; set; }

    }

    /// <summary>
    /// Select控件显示的组合
    /// </summary>
    public class SelectViewModelCommon
    {
        /// <summary>
        /// 显示文本
        /// </summary>
        public string label { get; set; }


        /// <summary>
        /// 值的名
        /// </summary>
        public string name { get; set; }
    }


    /// <summary>
    /// Select控件显示的组合(Value是Strng类型)
    /// </summary>
    public class SelectViewModel_Extensions: SelectViewModelCommon
    {

        /// <summary>
        /// 值
        /// </summary>
        public string value { get; set; }

    }


    /// <summary>
    /// Select控件显示的组合
    /// </summary>
    public class CascaderSelectViewModel : SelectViewModel
    {
        public List<CascaderSelectViewModel> children { get; set; }
    }
}
