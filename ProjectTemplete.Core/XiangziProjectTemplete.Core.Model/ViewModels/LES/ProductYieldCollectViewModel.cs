﻿using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 产量汇总视图
    /// </summary>
    public class ProductYieldCollectViewModel
    {
        /// <summary>
        /// 昨天产量
        /// </summary>
        public int YesterdayProductYield { get; set; }

        /// <summary>
        /// 今天产量
        /// </summary>
        public int TodayProductYield { get; set; }


    }


    public class WelcomeAccessApiDateView_AGV
    {
        public string[] columns { get; set; }
        public List<WelcomeApiDate> rows { get; set; }
    }


    public class WelcomeApiDate
    {
        public string 日期 { get; set; }
        public int 总量 { get; set; }

        /// <summary>
        /// 1号车
        /// </summary>
        public int 车辆1 { get; set; }

        public int 车辆2 { get; set; }
        public int 车辆3 { get; set; }
        public int 车辆4 { get; set; }
        public int 车辆5 { get; set; }
        public int 车辆6 { get; set; }
        public int 车辆7 { get; set; }
        public int 车辆8 { get; set; }
        public int 车辆9 { get; set; }
        public int 车辆10 { get; set; }

        public int 未分配 { get; set; }

    }
}
