﻿using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 用户站点绑定 请求参数类
    /// </summary>
    public class V_UserStationViewModel : ViewModels.XiangziProjectTemplete.V_UserStation
    {
        /// <summary>
        /// 工序Id列表
        /// </summary>
        public List<int[]> BindStationList { get; set; }

    }
}
