﻿using XiangziProjectTemplete.Core.Model.Models;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 首页数据展示
    /// </summary>
    public class WelcomeDataViewModel
    {

        /// <summary>
        /// 所有未开始的AGV任务
        /// </summary>
        public int AllNoStartTaskCount { get; set; }


        /// <summary>
        /// 所有已下发的AGV任务
        /// </summary>
        public int AllIssueTaskCount { get; set; }

        /// <summary>
        /// 所有未完成的AGV任务
        /// </summary>
        public int AllRuningTaskCount { get; set; }

        /// <summary>
        /// 执行中
        /// </summary>
        public int BEING_PROCESSED_Count { get; set; }


        /// <summary>
        /// 所有 待分配 的AGV任务
        /// </summary>
        public int AllWaitForDistributionTaskCount { get; set; }


        /// <summary>
        /// AGV报警个数
        /// </summary>
        public int AgvWarnCount { get; set; }

        /// <summary>
        /// 报警列表
        /// </summary>
        public List<AgvWarningViewModel> AgvWarningList { get; set; }
    }
}
