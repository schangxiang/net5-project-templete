﻿using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 能看到用户的站点的对象
    /// </summary>
    public class SeeUserStationViewModel
    {
        /// <summary>
        /// 能看到的站点列表
        /// </summary>
        public List<int> StationIdList { get; set; }


    }
}
