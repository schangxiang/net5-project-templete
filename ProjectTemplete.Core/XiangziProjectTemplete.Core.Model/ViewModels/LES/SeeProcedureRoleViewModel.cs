﻿using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 能看到工序的对象
    /// </summary>
    public class SeeProcedureRoleViewModel
    {
        /// <summary>
        /// 工序列表
        /// </summary>
        public List<int> ProcedureList { get; set; }

        /// <summary>
        /// 是否全部工序权限
        /// </summary>
        public bool IsHasAllProcedure { get; set; }

    }
}
