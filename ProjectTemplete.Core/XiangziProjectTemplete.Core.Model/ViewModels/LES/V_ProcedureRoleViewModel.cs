﻿using System;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 工序角色 请求参数类
    /// </summary>
    public class V_ProcedureRoleViewModel : ViewModels.XiangziProjectTemplete.V_ProcedureRole
    {
        /// <summary>
        /// 工序Id列表
        /// </summary>
        public List<String> AllowProcedureNameList { get; set; }

    }
}
