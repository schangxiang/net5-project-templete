﻿using XiangziProjectTemplete.Core.Model.Views;
using System.Collections.Generic;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 展示的可视化库存视图，用Card分组
    /// </summary>
    public class BufferStockCollectByStationGroupCardViewModel
    {
        /// <summary>
        /// 库存列表
        /// </summary>
        public List<V_LES_BufferStockCollectByStation> bufferStockCollectByStationList { get; set; }


        /// <summary>
        /// Card标题
        /// </summary>
        public string CardTitle { get; set; }

    }
}
