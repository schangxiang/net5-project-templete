﻿namespace XiangziProjectTemplete.Core.Model.ViewModels
{
    public class TokenInfoViewModel
    {
        public bool success { get; set; }
        public string token { get; set; }
        public double expires_in { get; set; }
        public string token_type { get; set; }

        /// <summary>
        /// 显示的用户名 【EditBy shaocx,2021-04-23】
        /// </summary>
        public string user_name { get; set; }

        /// <summary>
        /// 显示的用户ID 【EditBy shaocx,2021-04-23】
        /// </summary>
        public int user_id { get; set; }
    }
}
