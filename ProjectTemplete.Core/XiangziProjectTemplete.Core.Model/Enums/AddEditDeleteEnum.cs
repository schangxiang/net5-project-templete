﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model
{
    /// <summary>
    /// 新增、编辑、删除枚举
    /// </summary>
    public enum AddEditDeleteEnum
    {
        /// <summary>
        /// 新增
        /// </summary>
        [Description("新增")]
        Add = 1,
        /// <summary>
        /// 编辑
        /// </summary>
        [Description("编辑")]
        Edit = 2,
        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")]
        Delete = 500
    }
}
