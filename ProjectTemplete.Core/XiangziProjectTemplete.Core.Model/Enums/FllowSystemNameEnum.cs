﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model
{
    /// <summary>
    /// 所属系统枚举
    /// </summary>
    public enum FllowSystemNameEnum
    {
        /// <summary>
        /// 公共
        /// </summary>
        [Description("公共")]
        公共 = 1,
        /// <summary>
        /// 低压线圈控制系统
        /// </summary>
        [Description("低压线圈控制系统")]
        低压线圈控制系统 = 2,
        /// <summary>
        /// 物流XiangziProjectTemplete系统
        /// </summary>
        [Description("物流XiangziProjectTemplete系统")]
        物流XiangziProjectTemplete系统 = 3
    }
}
