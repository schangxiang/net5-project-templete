﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// 数据来源枚举
    /// </summary>
    public enum DataSourceEnum
    {
        [Description("XiangziProjectTemplete")]
        XiangziProjectTemplete = 0,
        [Description("Thingworx")]
        Thingworx = 1
    }
}
