﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// 库存区域枚举
    /// </summary>
    public enum StoreAreaEnum
    {
        /// <summary>
        /// 立库缓存区
        /// </summary>
        [Description("立库缓存区")]
        StoreBuffer = 1,
        /// <summary>
        /// 立库分拣区
        /// </summary>
        [Description("立库分拣区")]
        FJQ = 2,
        /// <summary>
        /// 引线线圈区
        /// </summary>
        [Description("引线线圈区")]
        KCQY_YXXQQ = 3,
        /// <summary>
        /// 引线线圈空车区
        /// </summary>
        [Description("引线线圈空车区")]
        KCQY_YXXQKCQ = 4,
        /// <summary>
        /// 高压绕线线圈区
        /// </summary>
        [Description("高压绕线线圈区")]
        KCQY_GYRXXQQ = 5,

        /// <summary>
        /// 高压内模托盘区
        /// </summary>
        [Description("高压内模托盘区")]
        KCQY_GYNMTPQ = 6,
        /// <summary>
        /// 高压内模准备区
        /// </summary>
        [Description("高压内模准备区")]
        KCQY_GYNMZBQ = 7,
        /// <summary>
        /// 高压绕线机内膜区
        /// </summary>
        [Description("高压绕线机内膜区")]
        KCQY_GYRXJNMQ = 8,



        /// <summary>
        /// 低压浇注线圈区
        /// </summary>
        [Description("低压浇注线圈区")]
        KCQY_DYJZXQQ = 9,
        ///// <summary>
        ///// 低压浇注线圈空车区
        ///// </summary>
        //[Description("低压浇注线圈空车区")]
        //KCQY_DYJZXQKCQ = 10,
        /// <summary>
        /// 低压绕线机线圈区
        /// </summary>
        [Description("低压绕线机线圈区")]
        KCQY_DYRXJXQQ = 11,
        /// <summary>
        /// 低压内模缓存区
        /// </summary>
        [Description("低压内模缓存区")]
        KCQY_DYNMHCQ = 12,

        /// <summary>
        /// 高压装配线圈发货区
        /// </summary>
        [Description("高压装配线圈发货区")]
        KCQY_GYZPXQ = 13,

        /// <summary>
        /// 低压装配线圈发货区
        /// </summary>
        [Description("低压装配线圈发货区")]
        KCQY_DYZPXQ = 14,

        /// <summary>
        /// 高压装配线圈收货区
        /// </summary>
        [Description("高压装配线圈收货区")]
        KCQY_GYZPXQSHQ = 15,
        /// <summary>
        /// 低压装配线圈收货区
        /// </summary>
        [Description("低压装配线圈收货区")]
        KCQY_DYZPXQSHQ = 16,



        /// <summary>
        /// 低压脱模发货区
        /// </summary>
        [Description("低压脱模发货区")]
        KCQY_DYTMFHQ = 17,

        /// <summary>
        /// 高压脱模发货区
        /// </summary>
        [Description("高压脱模发货区")]
        KCQY_GYTMFHQ = 18,

        /// <summary>
        /// 脱模内外模外协存放区
        /// </summary>
        [Description("脱模内外模外协存放区")]
        KCQY_TMNWMWXCFQ = 19,
        /// <summary>
        /// 脱模内外模空托区
        /// </summary>
        [Description("脱模内外模空托区")]
        KCQY_TMNWMKTQ = 20,
        /// <summary>
        /// 脱模压板外协存放区
        /// </summary>
        [Description("脱模压板外协存放区")]
        KCQY_TMYBWXCFQ = 21,
        /// <summary>
        /// 脱模压板空托区
        /// </summary>
        [Description("脱模压板空托区")]
        KCQY_TMYBKTQ = 22,
        /// <summary>
        /// 脱模引线接收区
        /// </summary>
        [Description("脱模引线接收区")]
        KCQY_TMYXJSQ = 23,

    }
}
