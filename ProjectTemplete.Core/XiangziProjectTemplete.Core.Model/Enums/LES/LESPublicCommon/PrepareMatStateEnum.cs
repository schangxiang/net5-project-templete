﻿
namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// 备料状态枚举
    /// </summary>
    public enum PrepareMatStateEnum
    {
        初始 = 1,
        /// <summary>
        /// 这个地方是Quarert调度任务启动，更改成此状态
        /// </summary>
        准备备料 = 2,
        已发送给立库 = 3,
        已入缓存库 = 4

    }
}
