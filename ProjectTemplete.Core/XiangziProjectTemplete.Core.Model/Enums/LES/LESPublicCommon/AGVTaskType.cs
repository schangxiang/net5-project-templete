﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// AGV任务类型
    /// </summary>
    public enum AGVTaskType
    {
        [Description("分拣入库")]
        分拣入库 = 1,
        [Description("缺料呼叫")]
        缺料呼叫 = 2,
        [Description("空托盘回库搬运")]
        空托盘回库搬运 = 3,
        [Description("余料回库搬运")]
        余料回库搬运 = 4,

        [Description("高压绕线线圈送至高压引线线圈区")]
        高压绕线线圈送至高压引线线圈区 = 5,

        [Description("高压线圈空车送至高压绕线线圈区")]
        高压线圈空车送至高压绕线线圈区 = 6,

        [Description("高压内膜托盘送至高压内模托盘区")]
        高压内膜托盘送至高压内模托盘区 = 7,

        [Description("高压内模送至高压绕线机内膜区")]
        高压内模送至高压绕线机内膜区 = 8,



        [Description("低压线圈送至低压浇注线圈区")]
        低压线圈送至低压浇注线圈区 = 9,

        [Description("低压线圈空车送至低压绕线机线圈区")]
        低压线圈空车送至低压绕线机线圈区 = 10,



        [Description("低压内模送至低压绕线内模区")]
        低压内模送至低压绕线内模区 = 11,

        [Description("低压内模空车送至低压内模缓存区")]
        低压内模空车送至低压内模缓存区 = 12,

        [Description("自由搬运")]
        自由搬运 = 13,

        [Description("高压装配线圈送至收货区")]
        高压装配线圈送至收货区 = 14,
        [Description("高压装配线圈空车送至发货区")]
        高压装配线圈空车送至发货区 = 15,


        [Description("低压装配线圈送至收货区")]
        低压装配线圈送至收货区 = 16,
        [Description("低压装配线圈空车送至发货区")]
        低压装配线圈空车送至发货区 = 17,


        [Description("低压脱模物料发货区送往存放区")]
        低压脱模物料发货区送往存放区 = 18,
        [Description("低压脱模托盘区送往发货区")]
        低压脱模托盘区送往发货区 = 19,


        [Description("高压脱模物料发货区送往存放区")]
        高压脱模物料发货区送往存放区 = 20,
        [Description("高压脱模托盘区送往发货区")]
        高压脱模托盘区送往发货区 = 21,

        [Description("高压脱模物料发货区送往脱模引线接收区")]
        高压脱模物料发货区送往脱模引线接收区 = 22,

        [Description("高压脱模引线接收区空车送至发货区")]
        高压脱模引线接收区空车送至发货区 = 23,


    }
}
