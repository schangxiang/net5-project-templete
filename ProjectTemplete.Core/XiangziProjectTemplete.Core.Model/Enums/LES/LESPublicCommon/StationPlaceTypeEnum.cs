﻿namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// 站点工位类型
    /// </summary>
    public enum StationPlaceTypeEnum
    {

        托盘工位 = 190,
        货物工位 = 191,
        余料工位 = 192,

        其他 = 199
    }
}
