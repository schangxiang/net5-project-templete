﻿using System.ComponentModel;

namespace iWareModels
{
    public enum TaskStatusEnum
    {
        [Description("未开始")]
        未开始 = 0,
        [Description("已下发")]
        已下发 = 1,
        [Description("已完成")]
        已完成 = 2,
        [Description("已取消")]
        已取消 = 99
    }
}
