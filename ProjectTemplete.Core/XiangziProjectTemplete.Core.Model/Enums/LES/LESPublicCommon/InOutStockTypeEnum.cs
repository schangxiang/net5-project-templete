﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete.LESPublicCommon
{
    /// <summary>
    /// 物料编号枚举
    /// </summary>
    public enum InOutStockTypeEnum
    {
        /// <summary>
        /// 入库
        /// </summary>
        入库 = 1,

        /// <summary>
        /// 出库
        /// </summary>
        出库 = 2,
    }
}
