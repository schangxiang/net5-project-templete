﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// XiangziProjectTemplete任务类型
    /// </summary>
    public enum LesTaskTypeEnum
    {
        [Description("高压绕线线圈运送任务")]
        高压绕线线圈运送任务 = 1,
        [Description("高压绕线线圈空车运送任务")]
        高压绕线线圈空车运送任务 = 2,
        [Description("高压内膜运送任务")]
        高压内膜运送任务 = 3,
        [Description("低压线圈运送任务")]
        低压线圈运送任务 = 4,
        [Description("低压内膜运送任务")]
        低压内膜运送任务 = 5,
        [Description("低压线圈空车运送任务")]
        低压线圈空车运送任务 = 6,
        [Description("自由搬运")]
        自由搬运 = 7,

        [Description("高压装配线圈运送任务")]
        高压装配线圈运送任务 = 8,
        [Description("高压装配线圈空车运送任务")]
        高压装配线圈空车运送任务 = 9,
        [Description("低压装配线圈运送任务")]
        低压装配线圈运送任务 = 10,
        [Description("低压装配线圈空车运送任务")]
        低压装配线圈空车运送任务 = 11,


        [Description("低压脱模运送任务")]
        低压脱模运送任务 = 12,
        [Description("高压脱模运送任务")]
        高压脱模运送任务 = 13,
        [Description("高压脱模空车任务")]
        高压脱模空车任务 = 14,

    }
}
