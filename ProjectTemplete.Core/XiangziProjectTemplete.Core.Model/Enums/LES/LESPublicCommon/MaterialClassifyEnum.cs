﻿namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete.LESPublicCommon
{
    /// <summary>
    /// 物料分类
    /// </summary>
    public enum MaterialClassifyEnum
    {
        普通物料 = 1,
        线圈物料 = 2
    }
}
