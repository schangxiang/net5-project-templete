﻿
namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    public enum M_V_S_StateEnum
    {
        在库分拣区料 = 1,
        分拣区料往立库缓存区配送中 = 2,
        在库立库缓存区料 = 3,

        站点余料 = 4,
        站点空托盘 = 5,


        站点物料 = 6,


        配送中 = 99,

        配送完成 = 100

    }
}
