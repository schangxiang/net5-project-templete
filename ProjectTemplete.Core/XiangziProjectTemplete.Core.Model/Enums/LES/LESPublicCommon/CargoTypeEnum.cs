﻿namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// 物料类型
    /// </summary>
    public enum CargoTypeEnum
    {
        物料 = 1,
        托盘 = 2
    }
}
