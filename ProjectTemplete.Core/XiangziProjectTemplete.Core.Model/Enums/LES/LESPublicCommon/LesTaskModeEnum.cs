﻿namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete.LESPublicCommon
{
    /// <summary>
    /// XiangziProjectTemplete业务任务的模式枚举
    /// </summary>
    public enum LesTaskModeEnum
    {
        //仅余料回库 = 1,
        //仅空托盘回库 = 2,
        //仅呼叫物料 = 3,
        //余料回库及呼叫物料 = 4,
        //空托盘回库及呼叫物料 = 5

        余料或空托盘回库 = 1,
        呼叫物料 = 2,
        余料或空托盘回库并呼叫物料 = 3
    }
}
