﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// AGV托盘型号
    /// </summary>
    public enum AGVSalverCategoryEnum
    {
        [Description("物料小车")]
        物料小车 = 1,
        [Description("川字托盘")]
        川字托盘 = 2,

        [Description("其他")]
        其他 = 99
    }
}
