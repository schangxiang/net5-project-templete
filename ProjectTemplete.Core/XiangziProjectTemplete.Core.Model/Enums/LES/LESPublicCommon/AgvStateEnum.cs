﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// AGV任务状态
    /// </summary>
    public enum AgvTaskStateEnum
    {
        /// <summary>
        /// 初始化中
        /// </summary>
        [Description("初始化中")]
        RAW = 0,
        /// <summary>
        /// 激活，可分配
        /// </summary>
        [Description("激活，可分配")]
        ACTIVE = 1,
        /// <summary>
        /// 待分配
        /// </summary>
        [Description("待分配")]
        DISPATCHABLE = 2,
        /// <summary>
        /// 执行中
        /// </summary>
        [Description("执行中")]
        BEING_PROCESSED = 3,
        /// <summary>
        /// 任务完成
        /// </summary>
        [Description("任务完成")]
        FINISHED = 4,
        /// <summary>
        /// 任务取消
        /// </summary>
        [Description("任务取消")]
        FAILED = 5,
        /// <summary>
        /// 路径生成错误
        /// </summary>
        [Description("路径生成错误")]
        UNROUTABLE = 6,
        /// <summary>
        /// 取消中
        /// </summary>
        [Description("取消中")]
        WITHDRAWN = 7
    }
}
