﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// 业务表名称
    /// </summary>
    public enum BussinessTableNameEnum
    {
        [Description("呼叫任务表")]
        Les_CallOrder = 1,
        [Description("分拣入库任务表")]
        Les_PickInStockTask = 2,
        [Description("高压绕线线圈任务表")]
        Les_HighPressureProductsTask = 3
    }
}
