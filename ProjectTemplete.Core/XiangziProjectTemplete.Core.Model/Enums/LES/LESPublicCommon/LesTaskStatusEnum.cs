﻿namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete.LESPublicCommon
{
    /// <summary>
    /// XiangziProjectTemplete业务任务的状态枚举
    /// </summary>
    public enum LesTaskStatusEnum
    {
        新建任务 = 0,

        //这些是正常的单个AGV任务
        生成AGV任务 = 1,
        已下发任务给AGV = 2,
        AGV运输中 = 3,

        //这些是由回库和叫料的单个AGV任务
        生成AGV回库任务 = 4,
        已下发回库任务给AGV = 5,
        AGV回库任务运输中 = 6,
        AGV回库任务完成 = 7,
        生成AGV叫料任务 = 8,
        已下发叫料任务给AGV = 9,
        AGV叫料任务运输中 = 10,

        //这些是高压绕线线圈的AGV任务
        已下发送料任务给AGV = 11,
        AGV送料任务运输中 = 12,
        AGV送料任务完成 = 13,
        已下发运送托盘任务给AGV = 14,
        运送托盘任务运输中 = 15,
        运送托盘任务完成 = 16,//注意：这个状态可能用不上！！！


        已完成 = 99,
        任务取消 = 100,
        强制完成 = 101
    }
}
