﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// 呼叫记录状态
    /// </summary>
    public enum CallRecordStatusEnum
    {
        已创建 = 1,
        进行中 = 2,
        已完成 = 3,
        取消 = 4
    }
}
