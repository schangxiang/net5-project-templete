﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model
{
    /// <summary>
    /// AGV报警状态枚举
    /// </summary>
    public enum AgvWarningStatusEnum
    {
        /// <summary>
        /// 新建
        /// </summary>
        [Description("新建")]
        新建 = 0,
        /// <summary>
        /// 已处理
        /// </summary>
        [Description("已处理")]
        已处理 = 1
    }
}
