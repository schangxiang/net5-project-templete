﻿namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// 入库场景
    /// </summary>
    public enum InStoreSceneEnum
    {
        立库分拣完成后入缓存库 = 1,

        高压绕线线圈送货 = 2,

        高压绕线内膜送货 = 3,

        低压线圈送货 = 4,

        低压内模送货 = 5,

        其他 = 99
    }
}
