﻿namespace XiangziProjectTemplete.Core.Model.Enums.XiangziProjectTemplete
{
    /// <summary>
    /// 回库搬运状态
    /// </summary>
    public enum BackToStoreStatusEnum
    {
        已创建 = 0,
        已呼叫Agv = 1,
        Agv搬运中 = 2,
        已入库 = 3,
        取消 = 4
    }
}
