﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model
{
    /// <summary>
    /// 异常级别枚举
    /// </summary>
    public enum ExceptionLevel
    {
        /// <summary>
        /// 警告
        /// </summary>
        [Description("警告")]
        Warning = 0,
        /// <summary>
        /// 异常错误
        /// </summary>
        [Description("异常错误")]
        Error = 1,
        /// <summary>
        /// 业务错误，默认错误
        /// </summary>
        [Description("业务错误")]
        BusinessError = 2,
        /// <summary>
        /// 正常信息
        /// </summary>
        [Description("信息")]
        Info = 3,
        /// <summary>
        /// 严重错误
        /// </summary>
        [Description("严重错误")]
        SeriousError = 4
    }
    /// <summary>
    /// 异常来源方向枚举
    /// </summary>
    public enum ExceptionSource
    {
        /// <summary>
        /// 接收
        /// </summary>
        Receive = 0,
        /// <summary>
        /// 推送
        /// </summary>
        Post = 1
    }
}
