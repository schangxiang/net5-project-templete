﻿using System.ComponentModel;

namespace XiangziProjectTemplete.Core.Model
{
    /// <summary>
    /// 需要生成任务号的类型
    /// </summary>
    public enum GenerateTaskTypeEnum
    {
        /// <summary>
        /// 请求立库备料
        /// </summary>
        [Description("请求立库备料")]
        请求立库备料 = 1,
    }
}
