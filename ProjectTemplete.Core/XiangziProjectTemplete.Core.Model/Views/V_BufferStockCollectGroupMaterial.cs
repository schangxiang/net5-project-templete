﻿using SqlSugar;

namespace XiangziProjectTemplete.Core.Model.Views
{
    [SugarTable(tableName: "V_BufferStockCollectGroupMaterial")]
    /// <summary>
    /// 分组查询立库缓存区的物料 视图
    /// </summary>
    public class V_BufferStockCollectGroupMaterial
    {
        /// <summary>
        /// 分拣入库的备注
        /// </summary>
        public string PickInStockTaskRemark { get; set; }

        /// <summary>
        /// 是否分拣完成
        /// </summary>
        public bool? IsPickFinish { get; set; }

        /// <summary>
        /// 是否分拣完成
        /// </summary>
        public string IsPickFinishName { get; set; }

        /// <summary>
        /// 配料任务号
        /// </summary>
        public string BurdenWorkNo { get; set; }

        /// <summary>
        /// 所属工序
        /// </summary>
        public string AllowProcedure { get; set; }

        /// <summary>
        /// 所属工序名称
        /// </summary>
        public string AllowProcedureName { get; set; }

        /// <summary>
        /// 物料大类的码表项ID,在码表中配置
        /// </summary>
        public string MaterialCodeItemId { get; set; }


        /// <summary>
        /// 货物名称
        /// </summary>
        public string MaterialName { get; set; }


        /// <summary>
        ///  托数
        /// </summary>
        public int Quantity { get; set; }



    }
}
