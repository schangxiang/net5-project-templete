﻿using XiangziProjectTemplete.Core.Model.Models;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 角色工序 查询视图
    /// </summary>
    public class V_ProcedureRole : Les_ProcedureRole
    {
        /// <summary>
        /// 角色名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 工序名称，用,分隔
        /// </summary>
        public string AllowProcedureName { get; set; }

    }
}
