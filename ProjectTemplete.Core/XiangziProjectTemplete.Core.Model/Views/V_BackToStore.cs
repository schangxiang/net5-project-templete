﻿using XiangziProjectTemplete.Core.Model.Models;
using SqlSugar;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 回库搬运视图
    /// </summary>
    [SugarTable(tableName: "V_BackToStore")]
    public class V_BackToStore : Les_BackToStore
    {
        /// <summary>
        /// 工位名称
        /// </summary>
        public string StationName { get; set; }

        /// <summary>
        /// 状态名称
        /// </summary>
        public string StatusName { get; set; }
    }
}
