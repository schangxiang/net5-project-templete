﻿using XiangziProjectTemplete.Core.Model.Models;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// 用户站点绑定 查询视图
    /// </summary>
    public class V_UserStation : Base_UserStation
    {
        /// <summary>
        /// 用户真实名
        /// </summary>
        public string uRealName { get; set; }

        /// <summary>
        /// 站点名称，用,分隔
        /// </summary>
        public string BindStationName { get; set; }

    }
}
