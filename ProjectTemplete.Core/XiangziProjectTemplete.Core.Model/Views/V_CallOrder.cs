﻿using XiangziProjectTemplete.Core.Model.Models;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    public class V_CallOrder : Les_CallOrder
    {


        /// <summary>
        /// 工位类型编号，表Base_CodeItems的Code
        /// </summary>
        public string PlaceTypeCode { get; set; }

        /// <summary>
        /// 工位类型，表Base_CodeItems的Name
        /// </summary>
        public string PlaceTypeName { get; set; }

        /// <summary>
        /// 所属工序名称
        /// </summary>
        public string AllowProcedureName { get; set; }

        /// <summary>
        /// 所属产线名称
        /// </summary>
        public string AllowLineName { get; set; }


        /// <summary>
        /// 库存区域名称
        /// </summary>
        public string StoreAreaName { get; set; }

        /// <summary>
        /// 叫料站点编号,唯一
        /// </summary>
        public string StationCode { get; set; }

        /// <summary>
        /// 叫料站点名称
        /// </summary>
        public string StationName { get; set; }



        /// <summary>
        /// 送货站点编号
        /// </summary>
        public string DeliveryStationCode { get; set; }


        /// <summary>
        /// 送货站点名称
        /// </summary>
        public string DeliveryStationName { get; set; }



    }
}
