﻿using XiangziProjectTemplete.Core.Model.Models;
using SqlSugar;
using System;

namespace XiangziProjectTemplete.Core.Model.Views
{
    [SugarTable(tableName: "V_LES_BufferStockCollectByStation")]
    /// <summary>
    /// 根据站点查询的库存查询视图
    /// </summary>
    public class V_LES_BufferStockCollectByStation : Base_Material
    {
        /// <summary>
        /// 表Les_Mater_V_Station的ID
        /// </summary>
        public string MVSId { get; set; }

        /// <summary>
        /// 工位类型，，在码表中配置，枚举 StationPlaceTypeEnum ,码表Base_CodeItems的ID
        /// </summary>
        public string PlaceType { get; set; }

        /// <summary>
        /// 工位类型编码，在码表中配置，枚举 StationPlaceTypeEnum ,码表Base_CodeItems的ID
        /// </summary>
        public string PlaceTypeCode { get; set; }

        /// <summary>
        /// 库存区域的编号
        /// </summary>
        public string StoreAreaCode { get; set; }


        /// <summary>
        /// 库存区域，在码表中配置
        /// </summary>
        public string StoreArea { get; set; }



        /// <summary>
        /// 站点表ID
        /// </summary>
        public int StationId { get; set; }

        /// <summary>
        /// 站点编号,唯一
        /// </summary>
        public string StationCode { get; set; }

        /// <summary>
        /// 站点名称
        /// </summary>
        public string StationName { get; set; }


        /// <summary>
        /// 所属工序名称
        /// </summary>
        public string AllowProcedureName { get; set; }

        /// <summary>
        /// 所属产线名称
        /// </summary>
        public string AllowLineName { get; set; }


        /// <summary>
        /// 工位类型，表Base_CodeItems的Name
        /// </summary>
        public string PlaceTypeName { get; set; }

        /// <summary>
        /// 库存区域名称
        /// </summary>
        public string StoreAreaName { get; set; }


        /// <summary>
        /// 是否锁定
        /// </summary>
        public int? IsLock { get; set; }


        /// <summary>
        /// 是否有货
        /// </summary>
        public int? IsFull { get; set; }


        /// <summary>
        /// 站点是否禁用
        /// </summary>
        public bool? Station_IsDeleted { get; set; }


        /// <summary>
        /// 小车名称
        /// </summary>
        public string AgvName { get; set; }


        /// <summary>
        /// 最后更新时间
        /// </summary>
        public DateTime? Station_ModifyTime { get; set; }

        /// <summary>
        /// 最后更新时间
        /// </summary>
        [SugarColumn( IsIgnore =true)]
        public String Station_ModifyTimeStr { get; set; }



        /// <summary>
        /// 站点的操作说明
        /// </summary>
        public string Station_OperationRemark { get; set; }


    }
}
