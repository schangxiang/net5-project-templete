﻿using XiangziProjectTemplete.Core.Model.Models;
using SqlSugar;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    /// <summary>
    /// AGV查询视图
    /// </summary>
    [SugarTable(tableName: "V_Agv")]
    public class V_Agv : Les_Agv
    {
        /// <summary>
        /// 物料号
        /// </summary>
        public string MaterialCode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }
    }
}
