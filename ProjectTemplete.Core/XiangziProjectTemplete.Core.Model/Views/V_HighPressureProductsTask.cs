﻿using XiangziProjectTemplete.Core.Model.Models;

namespace XiangziProjectTemplete.Core.Model.ViewModels.XiangziProjectTemplete
{
    public class V_HighPressureProductsTask : Les_HighPressureProductsTask
    {
        /// <summary>
        /// 货物类型名称，不是数据库字段
        /// </summary>
        public string CargoTypeName { get; set; }

        /// <summary>
        /// 工位类型编号，表Base_CodeItems的Code
        /// </summary>
        public string PlaceTypeCode { get; set; }

        /// <summary>
        /// 工位类型，表Base_CodeItems的Name
        /// </summary>
        public string PlaceTypeName { get; set; }

        /// <summary>
        /// 所属工序名称
        /// </summary>
        public string AllowProcedureName { get; set; }

        /// <summary>
        /// 所属产线名称
        /// </summary>
        public string AllowLineName { get; set; }


        /// <summary>
        /// 库存区域名称
        /// </summary>
        public string StoreAreaName { get; set; }



        public string MaterialCode { get; set; }



        public string MaterialName { get; set; }


        /// <summary>
        /// 站点编号,唯一
        /// </summary>
        public string StationCode { get; set; }

        /// <summary>
        /// 站点名称
        /// </summary>
        public string StationName { get; set; }


        #region 目标站点

        /// <summary>
        /// 目标站点站点编号,唯一
        /// </summary>
        public string ToStationCode { get; set; }

        /// <summary>
        /// 目标站点站点名称
        /// </summary>
        public string ToStationName { get; set; }

        #endregion



        /// <summary>
        /// 是否锁定
        /// </summary>
        public int? IsLock { get; set; }


        /// <summary>
        /// 是否有货
        /// </summary>
        public int? IsFull { get; set; }

    }
}
