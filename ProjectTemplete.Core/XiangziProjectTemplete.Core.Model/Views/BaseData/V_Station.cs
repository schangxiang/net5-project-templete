﻿using XiangziProjectTemplete.Core.Model.Models;
using SqlSugar;

namespace XiangziProjectTemplete.Core.Model.ViewModels.BasicData
{
    [SugarTable(tableName: "V_Station")]
    public class V_Station : Base_Station
    {
        /// <summary>
        /// 工位类型编号，表Base_CodeItems的Code
        /// </summary>
        public string PlaceTypeCode { get; set; }

        /// <summary>
        /// 工位类型，表Base_CodeItems的Name
        /// </summary>
        public string PlaceTypeName { get; set; }

    }
}
