﻿using XiangziProjectTemplete.Core.Model.Models;
using SqlSugar;

namespace XiangziProjectTemplete.Core.Model.ViewModels.BasicData
{
    [SugarTable(tableName: "V_CodeItems")]
    public class V_CodeItems : Base_CodeItems
    {
        public string setCodeName { get; set; }

        public string setCodeCode { get; set; }
    }
}
